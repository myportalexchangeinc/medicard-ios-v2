//
//  PostRouter.swift
//  Medicard
//
//  Created by Al John Albuera on 10/24/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


enum PostRouter: URLRequestConvertible {
    //static let baseURLString = "http://104.199.181.64:8080/"
    //public static let baseURLString = "http://10.10.24.195:8080/"
    static var OAuthToken: String?
    
    case postRegistration([String: AnyObject])
    case getVerifyMember([String: AnyObject])
    case postLogin([String: AnyObject])
    case getMemberInformation(String)
    case getProfilePhoto(String)
    case getDeletePhoto(String)
    case postAddDependents([String: AnyObject])
    case getMemberInfo(String)
    case postRequestNewPass([String: AnyObject])
    case postChangePassword([String: AnyObject])
    case getHospitalList
    case getDoctorsToHospital([String: AnyObject])
    case postRequestConsultation([String: AnyObject])
    case postRequestMaternity([String: AnyObject])
    case getInPatienExclusion([String: AnyObject])
    case getOutPatienExclusion([String: AnyObject])
    case getCityList
    case getProvinceList
    case getSpecialization
    case postAddPin([String: AnyObject])
    case postChangePin([String: AnyObject])
    case getLoaByMemberCode([String: AnyObject])
    case getDoctorDetails([String: AnyObject])
    case postCancelRequest([String: AnyObject])
    case postApprovedLoaRequest([String: AnyObject])

    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .postRegistration:
            return .post
        case .getVerifyMember:
            return .get
        case .postLogin:
            return .post
        case .getMemberInformation:
            return .get
        case .getProfilePhoto:
            return .get
        case .getDeletePhoto:
            return .delete
        case .postAddDependents:
            return .post
        case .getMemberInfo:
            return .get
        case .postRequestNewPass:
            return .post
        case .postChangePassword:
            return .post
        case .getHospitalList:
            return .get
        case .getDoctorsToHospital:
            return .get
        case .postRequestConsultation:
            return .post
        case .postRequestMaternity:
            return .post
        case .getInPatienExclusion:
            return .get
        case .getOutPatienExclusion:
            return .get
        case .getCityList:
            return .get
        case .getProvinceList:
            return .get
        case .getSpecialization:
            return .get
        case .postAddPin:
            return .post
        case .postChangePin:
            return .post
        case .getLoaByMemberCode:
            return .get
        case .getDoctorDetails:
            return .get
        case .postCancelRequest:
            return .post
        case .postApprovedLoaRequest:
            return .post
        
        }
    }
    
    var path: String {
        switch self {
        case .postRegistration:
            return "v2/registerAccount/"
        case .getVerifyMember:
            return "v2/verifyMember/"
        case .postLogin:
            return "v2/loginMember/"
        case .getMemberInformation(let memberID):
            return "v2/viewAccountInfo/\(memberID)"
        case .getProfilePhoto(let memberID):
            return "v2/downloadPicture/\(memberID)"
        case .getDeletePhoto(let memberID):
            return "v2/deletePicture/\(memberID)"
        case .postAddDependents:
            return "v2/addOtherAccount/"
        case .getMemberInfo(let memberID):
            return "v2/getMemberInfo/\(memberID)"
        case .postRequestNewPass:
            return "v2/requestChangePassword/"
        case .postChangePassword:
            return "v2/changePassword/"
        case .getHospitalList:
            return "listing/getHospitals/"
        case .getDoctorsToHospital:
            return "listing/getDoctorsToHospital/"
        case .postRequestConsultation:
            return "memberloa/requestLOAConsult/"
        case .postRequestMaternity:
            return "memberloa/requestLOAMaternity/"
        case .getInPatienExclusion:
            return "listing/getInpatientHospitalExclusionList/"
        case .getOutPatienExclusion:
            return "listing/getOutpatientHospitalExclusionList/"
        case .getCityList:
            return "listing/getCities/"
        case .getProvinceList:
            return "listing/getProvinces/"
        case .getSpecialization:
            return "listing/getSpecializations/"
        case .postAddPin:
            return "v2/registerPin/"
        case .postChangePin:
            return "v2/updatePin/"
        case .getLoaByMemberCode:
            return "v2/getLoaByMemberCode/"
        case .getDoctorDetails:
            return "listing/getDoctorByCode/"
        case .postCancelRequest:
            return "memberloa/cancelLOA/"
        case .postApprovedLoaRequest:
            return "coordinator/v2/approveLOA/"
            
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        
        let defaults = UserDefaults.standard
        
        let url = URL(string: defaults.value(forKey: "link1") as! String)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        
        if let token = PostRouter.OAuthToken {
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            
        }
        
        
        switch self {
            
        case .postRegistration(let register):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: register)
        case .getVerifyMember (let verify):
            return try URLEncoding.queryString.encode(urlRequest, with: verify)
        case .getDoctorsToHospital (let hospitalCode):
            return try URLEncoding.queryString.encode(urlRequest, with: hospitalCode)
        case .getInPatienExclusion (let memberId):
            return try URLEncoding.queryString.encode(urlRequest, with: memberId)
        case .getOutPatienExclusion (let memberId):
            return try URLEncoding.queryString.encode(urlRequest, with: memberId)
        case .postLogin(let login):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: login)
        case .postAddDependents(let dependents):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: dependents)
        case .postRequestNewPass(let newPassword):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: newPassword)
        case .postChangePassword(let changePassword):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: changePassword)
        case .postRequestMaternity(let requestMaternity):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: requestMaternity)
        case .postRequestConsultation(let requestConsultation):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: requestConsultation)
        case .postAddPin(let addPin):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: addPin)
        case .postChangePin(let updatePin):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: updatePin)
        case .getLoaByMemberCode (let memberCode):
            return try URLEncoding.queryString.encode(urlRequest, with: memberCode)
        case .getDoctorDetails (let doctorCode):
            return try URLEncoding.queryString.encode(urlRequest, with: doctorCode)
        case .postCancelRequest(let approvalCode):
            return try URLEncoding.queryString.encode(urlRequest, with: approvalCode)
        case .postApprovedLoaRequest(let batchCode):
            return try URLEncoding.queryString.encode(urlRequest, with: batchCode)
        default:
            return urlRequest
            
        }
    }
    
    
    
}


