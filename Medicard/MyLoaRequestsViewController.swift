//
//  MyLoaRequestsViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 2/6/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON
import SCLAlertView
import ReachabilitySwift


class MyLoaRequestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var revealButton: UIBarButtonItem!
    @IBOutlet var tblLoaHistory: UITableView!
    @IBOutlet var btnSortAndfilter: UIButton!
    let defaults = UserDefaults.standard
    let reachability = Reachability()
    var subjectInformation = [NSManagedObject]()
    var subjectInformations = [NSManagedObject]()
    var doctorCodes: [String] = []
    var hospitalCodes: [String] = []
    var docRoom: [String] = []
    var docName: [String] = []
    var docSpec: [String] = []
    var hospitalName: [String] = []
    var loaStatus: [String] = []
    var loaType: [String] = []
    var loaReqdate: [String] = []
    var problemCondition: [String] = []
    var approvalNumber: [String] = []
    var consultationHeader: [String] = []
    var requestDate: [Date] = []
    
    var strSearchDoctor: String!
    var strSortBy: String!
    var strServiceType: String!
    var strStatus: String!
    var loadStatus: Int!
    var dataCount: Int!
    
    
    @IBOutlet var lblLoadingOrNoLoa: UILabel!
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "My LOA Requests"
        
        if revealViewController() != nil {
            
            //revealViewController().rightViewRevealWidth = 150
            revealButton.target = revealViewController()
            revealButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        tblLoaHistory.rowHeight = UITableViewAutomaticDimension
        tblLoaHistory.estimatedRowHeight = 205;

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        Getter_Setter.finalChosenDoctorName = Getter_Setter.finalChosenDoctorName
        Getter_Setter.chosenDoctorIndex = Getter_Setter.finalChosenDoctorIndexes
        
        Getter_Setter.finalChosenHospitalName = Getter_Setter.finalChosenHospitalName
        Getter_Setter.chosenHospitalIndex = Getter_Setter.finalChosenHospitalIndexes
        
        Getter_Setter.finalChosenSortBy = Getter_Setter.finalChosenSortBy
        Getter_Setter.finalChosenStatus = Getter_Setter.finalChosenStatus
        Getter_Setter.finalChosenServiceType = Getter_Setter.finalChosenServiceType
        Getter_Setter.finalChosenSearch = Getter_Setter.finalChosenSearch
        
        Getter_Setter.finalChosenRequestDateTo = Getter_Setter.finalChosenRequestDateTo
        Getter_Setter.finalChosenRequestDateFrom = Getter_Setter.finalChosenRequestDateFrom
        
        
        Getter_Setter.chooseHospitalClininc = "All Hospital"
        Getter_Setter.chooseDoctor = "All Doctors"
        
        if Getter_Setter.finalChosenSortBy == "Hospital/Clinic Name"{
            strSortBy = "hospitalName"
        }else if Getter_Setter.finalChosenSortBy == "Status"{
            strSortBy = "loaStatus"
        }else if Getter_Setter.finalChosenSortBy == "Request Date"{
            strSortBy = "reqDate"
        }else if Getter_Setter.finalChosenSortBy == "Doctor"{
            strSortBy = "doctorName"
        }else if Getter_Setter.finalChosenSortBy == "Service Type"{
            strSortBy = "loaType"
        }else if Getter_Setter.finalChosenSortBy == "Hospital/Clinic Name "{
            strSortBy = "loaStatus"
        }else{
            strSortBy = ""
        }
        
        
        if Getter_Setter.finalChosenStatus == "Approved"{
            strStatus = "Approved"
        }else if Getter_Setter.finalChosenStatus == "Pending"{
            strStatus = "Pending"
        }else if Getter_Setter.finalChosenStatus == "Expired"{
            strStatus = "Expired"
        }else if Getter_Setter.finalChosenStatus == "Availed"{
            strStatus = "Availed"
        }else if Getter_Setter.finalChosenStatus == "Cancelled"{
            strStatus = "Cancelled"
        }else if Getter_Setter.finalChosenStatus == "Disapproved"{
            strStatus = "Disapproved"
        }else if Getter_Setter.finalChosenStatus == "Outstanding"{
            strStatus = "Outstanding"
        }else if Getter_Setter.finalChosenStatus == "Choose Status..."{
            strStatus = "Choose Status..."
        }
        
        if Getter_Setter.finalChosenServiceType == "Consultation"{
            strServiceType = "Consultation"
        }else if Getter_Setter.finalChosenServiceType == "Maternity Consultation"{
            strServiceType = "Maternity Consultation"
        }else if Getter_Setter.finalChosenServiceType == "Basic Tests"{
            strServiceType = "Basic Tests"
        }else if Getter_Setter.finalChosenServiceType == "Other Test/Procedures"{
            strServiceType = "Other Test/Procedures"
        }else if Getter_Setter.finalChosenServiceType == "Choose Service Type..."{
            strServiceType = "Choose Service Type..."
        }
        
        
        print("a1: \(Getter_Setter.finalChosenSearch)")
        print("a2: \(Getter_Setter.finalChosenStatus)")
        print("a3: \(Getter_Setter.finalChosenServiceType)")
        print("a4: \(Getter_Setter.finalChosenDoctorName)")
        print("a5: \(Getter_Setter.finalChosenHospitalName)")
        print("a6: \(Getter_Setter.finalChosenRequestDateFrom)")
        print("a6: \(Getter_Setter.finalChosenRequestDateTo)")
        print("a7: \(Getter_Setter.finalChosenSortBy)")
        
        print("a8: \(Getter_Setter.finalDateTo)")
        print("a9: \(Getter_Setter.finalDateFrom)")
        
        if reachability?.isReachable == true{
            
            //fetch dependentInformation
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading..."
            loadingNotification.isUserInteractionEnabled = false;
            tblLoaHistory.isUserInteractionEnabled = false
            self.tblLoaHistory.isScrollEnabled = false
            
            
            if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == "" && Getter_Setter.finalChosenSearch == "" && strSortBy == "reqDate"{
                
                self.deleteDataEntity(entityDescription: "LoaHistoryEntity")
                
                print("HEEEREEEE")
            
                
            }else{
                
                if Getter_Setter.cancelRequestStatus == 1{
                    
                     print("HEEEREEEEssss")
                    
                    tblLoaHistory.isHidden = true
                    self.deleteDataEntity(entityDescription: "LoaHistoryEntity")
                    
                }else{
                    
                    print("HEEEREEEEssssasaasa")
                    
                    fetchAllLoaHistoryMain()
                    
                }
                
                
            }
            
            
            
            
            
        }else{
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectInformation.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MyLoaRequestTableViewCell! = tblLoaHistory.dequeueReusableCell(withIdentifier: "loaHistoryCell", for: indexPath) as! MyLoaRequestTableViewCell
        
        
        let loaHistory = subjectInformation[(indexPath as NSIndexPath).row]
        
        
        let statusSelected = loaHistory.value(forKey: "loaStatus") as! String!
        let serviceTypeSelected = loaHistory.value(forKey: "loaType") as! String!
            
            
        print("STATUS: \(serviceTypeSelected)")
        
        
         cell.lblLoaType.text = loaHistory.value(forKey: "loaType") as! String!
        
        if serviceTypeSelected == "MATERNITY CONSULTATION" || serviceTypeSelected == "CONSULTATION" || serviceTypeSelected == "Choose Status..." {
            
            if statusSelected == "APPROVED" || statusSelected == "APPROVED "{
                
                cell.lblStatus.text = "OUTSTANDING"
                
            }else{
                
                cell.lblStatus.text = loaHistory.value(forKey: "loaStatus") as! String!
                
            }

            
        }else{
            
             cell.lblStatus.text = "APPROVED"
            
        }
       
        
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        formatter.date(from: loaHistory.value(forKey: "reqDate") as! String!)
        let dateFormatted = formatter.date(from: loaHistory.value(forKey: "reqDate") as! String!)
        formatter.dateFormat = "MM/dd/yyyy"
        let result = formatter.string(from: dateFormatted!)
        
        print("dateeeeee : \(loaHistory.value(forKey: "reqDate") as! String!)")
        print("reqDateeee: \(loaHistory.value(forKey: "requestDate"))")
        
        
        cell.lblRequestDate.text = "Request Date: \(result)"
        cell.lblHospitalName.text = loaHistory.value(forKey: "hospitalName") as! String!
        cell.lblDoctorName.text = loaHistory.value(forKey: "doctorName") as! String!
        cell.lblDoctorSpec.text = loaHistory.value(forKey: "docSpec") as! String!
        
        
        
        cell.selectionStyle = .none

        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let loaHistory = subjectInformation[(indexPath as NSIndexPath).row]
        
        let goToLoaHistoryDetails = self.storyboard!.instantiateViewController(withIdentifier: "LoaHostoryDetailsViewController") as! LoaHostoryDetailsViewController
        goToLoaHistoryDetails.strRequestingDoctor = loaHistory.value(forKey: "doctorName") as! String!
        goToLoaHistoryDetails.strHospitalName = loaHistory.value(forKey: "hospitalName") as! String!
        goToLoaHistoryDetails.strProblemCondition = loaHistory.value(forKey: "problemCondition") as! String!
        goToLoaHistoryDetails.strMemberName = defaults.value(forKey: "memFullName") as! String!
        goToLoaHistoryDetails.strCompany = defaults.value(forKey: "memCompanyName") as! String!
        goToLoaHistoryDetails.strMemberCode = defaults.value(forKey: "memCode") as! String!
        goToLoaHistoryDetails.strAge = defaults.value(forKey: "memAge") as! String!
        goToLoaHistoryDetails.strGender = defaults.value(forKey: "memGender") as! String
        goToLoaHistoryDetails.strApprovalCode = loaHistory.value(forKey: "approvalCode") as! String!
        goToLoaHistoryDetails.strDateApproved = loaHistory.value(forKey: "reqDate") as! String!
        goToLoaHistoryDetails.strPageHeader = loaHistory.value(forKey: "loaType") as! String!
        goToLoaHistoryDetails.strRequestStatus = loaHistory.value(forKey: "loaStatus") as! String!
        let navController = UINavigationController(rootViewController: goToLoaHistoryDetails)
        self.present(navController, animated:true, completion: nil)

    }
    
    func fetchLoaHistory(memberCode: String){
        
        let params = ["memberCode":memberCode]  as [String : Any]
        
        let request = Alamofire.request(PostRouter.getLoaByMemberCode(params as [String : AnyObject]))
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblLoaHistory.isUserInteractionEnabled = true
                    self.tblLoaHistory.isScrollEnabled = true
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    
                    
                    if post["loaList"].count == 0{
                        
                        self.tblLoaHistory.isHidden = true
                        self.tblLoaHistory.isUserInteractionEnabled = true
                        self.tblLoaHistory.isScrollEnabled = true
                        
                    }else{
                        
                        self.tblLoaHistory.isHidden = false
                        
                        self.dataCount = post["loaList"].count
                        print("DATA COUNT: \(self.dataCount)")
                        
                        for item in post["loaList"].arrayValue {
                            
                            self.hospitalCodes.append(item["hospitalCode"].stringValue)
                            self.doctorCodes.append(item["doctorCode"].stringValue)
                            self.loaType.append(item["remarks"].stringValue)
                            self.loaReqdate.append(item["approvalDate"].stringValue)
                            self.loaStatus.append(item["status"].stringValue)
                            self.problemCondition.append(item["primaryComplaint"].stringValue)
                            self.approvalNumber.append(item["approvalNo"].stringValue)
                            
                            
                            print("COMPLAINT: \(item["primaryComplaint"].stringValue)")
                            
                            self.fetchDoctorDetails(doctorCode: item["doctorCode"].stringValue, hospitalCode: item["hospitalCode"].stringValue, itemLoaType: item["remarks"].stringValue, itemLoaStatus: item["status"].stringValue, itemReqDate: item["approvalDate"].stringValue, primaryComplaint: item["primaryComplaint"].stringValue, approvalNumber: item["approvalNo"].stringValue)
                            
                            
                        }
                        
                    }
                    
                }
        }
        debugPrint(request)
        
    }
    

    func fetchDoctorDetails(doctorCode: String, hospitalCode: String, itemLoaType: String, itemLoaStatus: String, itemReqDate: String, primaryComplaint: String, approvalNumber: String){
        
        let params = ["doctorCode" : doctorCode]  as [String : Any]
        
        
        let request = Alamofire.request(PostRouter.getDoctorDetails(params as [String : AnyObject]))
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblLoaHistory.isUserInteractionEnabled = true
                    self.tblLoaHistory.isScrollEnabled = true
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    let doctorDetails = post["doctor"]
                    
                    
                    
                    let fName = doctorDetails["docFname"].stringValue
                    let lName = doctorDetails["docLname"].stringValue
                   
                    
                    if post["responseCode"].stringValue == "200"{
                        
                        
                        self.docName.append(fName + " " + lName)
                        self.docSpec.append(doctorDetails["specDesc"].stringValue)
                        
                            
                        self.fetchAllHospital(hospitalCode: hospitalCode, itemLoaType: itemLoaType, itemLoaStatus: itemLoaStatus, itemReqDate: itemReqDate, primaryComplaint: primaryComplaint, approvalNumber: approvalNumber)
                        
                        
                    }else if post["responseCode"].stringValue == "210"{

                        
                        let docSpec = "Spec Not Specified."
                        
                        self.docSpec.append(docSpec)
                        self.docName.append(doctorCode)
                        

                        self.fetchAllHospital(hospitalCode: hospitalCode, itemLoaType: itemLoaType, itemLoaStatus: itemLoaStatus, itemReqDate: itemReqDate, primaryComplaint: primaryComplaint, approvalNumber: approvalNumber)

                        
                    }
                    
                }
        }
        debugPrint(request)
        
    }


    
    
    
    func fetchAllHospital(hospitalCode: String, itemLoaType: String, itemLoaStatus: String, itemReqDate: String, primaryComplaint: String, approvalNumber: String){

       
       let appDelegate =
           UIApplication.shared.delegate as! AppDelegate
       
       let managedContext = appDelegate.managedObjectContext
       
       let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "HospitalsEntity")
       //FILTERING
       let predicate1 = NSPredicate(format: "hospitalCode == [cd] %@", hospitalCode)
       let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
       fetchRequest.predicate = motherPredicate
       
       
       do {
           let results = try managedContext.fetch(fetchRequest)
           subjectInformations = results
           
           for itemHospitalName in subjectInformations{
               
               self.insertLoaHistory(hospitalName: itemHospitalName.value(forKey: "hospitalName") as! String, itemLoaType: itemLoaType, itemLoaStatus: itemLoaStatus, itemReqDate: itemReqDate, primaryComplaint: primaryComplaint, approvalNumber: approvalNumber)
               
           }
        
        
           
       } catch let error as NSError {
           print("Could not fetch. \(error), \(error.userInfo)")
       }

        
    }
    
    
    
    func insertLoaHistory(hospitalName: String, itemLoaType: String, itemLoaStatus: String, itemReqDate: String, primaryComplaint: String, approvalNumber: String){
            
       let appDelegate =
           UIApplication.shared.delegate as! AppDelegate
       
       let managedContext = appDelegate.managedObjectContext
       
       let entity = NSEntityDescription.entity(forEntityName: "LoaHistoryEntity",
                                               in: managedContext)!
       
       let loaHistory = NSManagedObject(entity: entity,
                                        insertInto: managedContext)

       loaHistory.setValue(hospitalName, forKey: "hospitalName")
       
       for itemDoctorCodes in doctorCodes{
           loaHistory.setValue(itemDoctorCodes, forKey: "docCode")
       }
       
       for itemDoctorName in docName{
           loaHistory.setValue(itemDoctorName, forKey: "doctorName")
       }
       
       for itemDoctorSpec in docSpec{
           loaHistory.setValue(itemDoctorSpec, forKey: "docSpec")
       }
       
       loaHistory.setValue(itemLoaType, forKey: "loaType")
       
       loaHistory.setValue(itemReqDate, forKey: "reqDate")
       loaHistory.setValue(primaryComplaint, forKey: "problemCondition")
       loaHistory.setValue(approvalNumber, forKey: "approvalCode")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.date(from: itemReqDate)
        let dateFormatted = dateFormatter.date(from: itemReqDate)
        
       
        
        
        //ADDED 3DAYS FROM CURRENT DATE
        let tomorrow = Calendar.current.date(byAdding: .day, value: 4, to: dateFormatted!)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let result3 = dateFormatter.string(from: tomorrow!)
        let myDates = dateFormatter.date(from: result3)

        let currentDate = Date()
        
        if currentDate > myDates!{
            
            loaHistory.setValue("EXPIRED", forKey: "loaStatus")
            
        }else if itemLoaType == "MATERNITY CONSULTATION" || itemLoaType == "CONSULTATION"{
            
            if itemLoaStatus == "APPROVED" || itemLoaStatus == "APPROVED "{
                loaHistory.setValue("OUTSTANDING", forKey: "loaStatus")
                
                print("HEEEEEEEEEEE")
                
            }else{
                
                loaHistory.setValue(itemLoaStatus, forKey: "loaStatus")
                
            }
            
        }else{
            
            loaHistory.setValue(itemLoaStatus, forKey: "loaStatus")
            
        }
        
        
        loaHistory.setValue(dateFormatted, forKey: "requestDate")
        
        print("CURRENT DATE: \(currentDate)")
        print("DATE APPROVED: \(myDates)")
        
        

       do {
           try managedContext.save()
        
            fetchAllLoaHistory()
        
       } catch let error as NSError {
           print("Could not save. \(error), \(error.userInfo)")
       }

    }
    
    
    func fetchAllLoaHistoryMain(){
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "LoaHistoryEntity")
        
        var motherPredicate = NSCompoundPredicate()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        formatter.date(from: Getter_Setter.finalChosenRequestDateFrom)
        formatter.date(from: Getter_Setter.finalChosenRequestDateTo)
        let dateFormattedFrom = formatter.date(from: Getter_Setter.finalChosenRequestDateFrom)
        let dateFormattedTo = formatter.date(from: Getter_Setter.finalChosenRequestDateTo)
        
        
        if Getter_Setter.finalChosenSearch == "" {
            

            if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("1a:")
                
                let predicate3 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("3a:")
                
                let predicate3 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("4a")
                
                let predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [ predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{

                print("5a")

                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                print("6a")
                
                var predicate2 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("DATE: 1 \(Getter_Setter.finalDateFrom)")
                    print("DATE: 2 \(Getter_Setter.finalDateTo)")
                
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate2 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate2 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("10a")
                
                let predicate2 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate3 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate3, predicate2])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
            
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{

                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                
                let predicate2 = NSPredicate(format: "doctorName IN %@", Getter_Setter.finalChosenDoctorName)
                let predicate3 = NSPredicate(format: "hospitalName IN %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                
                let predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "doctorName IN %@", Getter_Setter.finalChosenDoctorName)
                let predicate4 = NSPredicate(format: "hospitalName IN %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3, predicate4])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                
                let predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "doctorName IN %@", Getter_Setter.finalChosenDoctorName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                
                let predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate2 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate2 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                var predicate4 = NSPredicate()
                
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3, predicate4])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                var predicate4 = NSPredicate()
                var predicate5 = NSPredicate()
                
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                        predicate5 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                        predicate5 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3, predicate4, predicate5])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate2 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strStatus)
                        predicate2 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate2 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                       predicate2 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate3])
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                fetchRequest.predicate = motherPredicate
                
            }

            
        }else{
            
            
            if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate3 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate3 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "doctorName IN %@", Getter_Setter.finalChosenHospitalName)
                let predicate3 = NSPredicate(format: "hospitalName IN %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                let predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "doctorName IN %@", Getter_Setter.finalChosenDoctorName)
                let predicate4 = NSPredicate(format: "hospitalName IN %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate0, predicate1, predicate2, predicate3, predicate4])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                
                let predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "doctorName IN %@", Getter_Setter.finalChosenDoctorName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate0, predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                print("HELLo: \(Getter_Setter.finalChosenHospitalName)")
                print("Hospital Status: \(strStatus)")
                let predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate0, predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                let predicate3 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate3, predicate2])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(loaStatus BEGINSWITH %@)", strStatus)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(loaStatus BEGINSWITH %@)", strStatus)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(loaType == %@)", strServiceType)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(loaType == %@)", strServiceType)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(doctorName IN %@)", Getter_Setter.finalChosenDoctorName)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(doctorName IN %@)", Getter_Setter.finalChosenDoctorName)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(hospitalName IN %@)", Getter_Setter.finalChosenHospitalName)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(hospitalName IN %@)", Getter_Setter.finalChosenHospitalName)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate0 = NSPredicate()
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate0, predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate0 = NSPredicate()
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                var predicate4 = NSPredicate()
                
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate0, predicate1, predicate2, predicate3, predicate4])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus != "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate0 = NSPredicate()
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                var predicate4 = NSPredicate()
                var predicate5 = NSPredicate()
                
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                        predicate5 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate0 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate1 = NSPredicate(format: "loaType == [cd] %@", strServiceType)
                        predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                        predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate4 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                        predicate5 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate0, predicate1, predicate2, predicate3, predicate4, predicate5])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "loaType == [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                fetchRequest.predicate = motherPredicate
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count != 0 && Getter_Setter.finalChosenHospitalName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                var predicate3 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "doctorName IN [cd] %@", Getter_Setter.finalChosenDoctorName)
                        predicate3 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                         predicate2 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate3 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                fetchRequest.predicate = motherPredicate
                
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType == "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenHospitalName.count != 0 && Getter_Setter.finalChosenRequestDateFrom == "" && Getter_Setter.finalChosenRequestDateTo == ""{
                
                let predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                let predicate2 = NSPredicate(format: "loaStatus BEGINSWITH [cd] %@", strStatus)
                let predicate3 = NSPredicate(format: "hospitalName IN [cd] %@", Getter_Setter.finalChosenHospitalName)
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                
            }else if Getter_Setter.finalChosenStatus == "Choose Status..." && Getter_Setter.finalChosenServiceType != "Choose Service Type..." && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenDoctorName.count == 0 && Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
                
                var predicate1 = NSPredicate()
                var predicate2 = NSPredicate()
                
                if dateFormattedFrom != nil || dateFormattedTo != nil{
                    
                    
                    let dateFormatterCompleteDate = DateFormatter()
                    dateFormatterCompleteDate.dateFormat = "YYYY-MM-dd"
                    dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    let dates =  dateFormatterCompleteDate.string(from: Getter_Setter.finalDateTo as Date)
                    print("HAHAHAHAHA: \(dates)")
                    
                    
                    print("CHOSEN SEARCH: \(Getter_Setter.finalChosenSearch)")
                    
                    if Getter_Setter.finalChosenRequestDateFrom == Getter_Setter.finalChosenRequestDateTo{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(reqDate CONTAINS %@)", dates)
                    }else{
                        predicate1 = NSPredicate(format: "doctorName CONTAINS [cd] %@ OR hospitalName CONTAINS [cd] %@", Getter_Setter.finalChosenSearch, Getter_Setter.finalChosenSearch)
                        predicate2 = NSPredicate(format: "(requestDate >= %@) AND (requestDate <= %@)", Getter_Setter.finalDateTo, Getter_Setter.finalDateFrom)
                    }
                    
                }
                
                motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                fetchRequest.predicate = motherPredicate
                
            }

        }
        
        if strSortBy == "reqDate" ||  Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
            
            let sortDescriptor = NSSortDescriptor(key: "reqDate", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            
        }else{
            
            let sortDescriptor = NSSortDescriptor(key: strSortBy, ascending: true)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            
        }
        
        
        do {

            let results = try managedContext.fetch(fetchRequest)
            subjectInformation = results
            
            print("ITEMS: \(subjectInformation.count)")
            
            if subjectInformation.count == 0{
                
                tblLoaHistory.isHidden = true
                //self.btnSortAndfilter.isHidden = true
                MBProgressHUD.hide(for: self.view, animated: true)
                
            }else{
                
                tblLoaHistory.reloadData()
                self.tblLoaHistory.isUserInteractionEnabled = true
                self.tblLoaHistory.isScrollEnabled = true
                tblLoaHistory.isHidden = false
                //self.btnSortAndfilter.isHidden = false
                
            }
            
           

            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        
    }
    
    func fetchAllLoaHistory(){
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "LoaHistoryEntity")
        
        if strSortBy == "reqDate" ||  Getter_Setter.finalChosenRequestDateFrom != "" && Getter_Setter.finalChosenRequestDateTo != ""{
            
            let sortDescriptor = NSSortDescriptor(key: "reqDate", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            
        }else{
            
            let sortDescriptor = NSSortDescriptor(key: strSortBy, ascending: true)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            
        }

    
        
        do {
         
            let results = try managedContext.fetch(fetchRequest)
            
            let sections = tblLoaHistory.numberOfSections
            
            var rows = 1;
            
            for items in 0..<sections {
                rows += tblLoaHistory.numberOfRows(inSection: items)
            }
        
            if dataCount == results.count {
                

              subjectInformation = results
              
              tblLoaHistory.isHidden = false
              tblLoaHistory.reloadData()
              
              MBProgressHUD.hide(for: self.view, animated: true)
              self.tblLoaHistory.isUserInteractionEnabled = true
              self.tblLoaHistory.isScrollEnabled = true
              //self.btnSortAndfilter.isHidden = false

                
            }else if subjectInformation.count == 0{
                
                self.tblLoaHistory.isUserInteractionEnabled = false
                self.tblLoaHistory.isScrollEnabled = false
                //self.btnSortAndfilter.isHidden = true
                
            }

            
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        
    }
    
    
    func deleteDataEntity(entityDescription: String){
    
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
       let managedContext = appDelegate.managedObjectContext
       
       let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityDescription)
       let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
       
       do {
           try managedContext.execute(batchDeleteRequest)
           
           let memberCode = defaults.string(forKey: "memberCode")! as String
           
           fetchLoaHistory(memberCode: memberCode)

           
       } catch {
           // Error Handling
       }
        
    }
    

    @IBAction func sortFilterAction(_ sender: Any) {
        
        let goToLoaHistorySorting = self.storyboard!.instantiateViewController(withIdentifier: "LoaHistorySortingFilteringViewController") as! LoaHistorySortingFilteringViewController
        let navController = UINavigationController(rootViewController: goToLoaHistorySorting)
        self.present(navController, animated:true, completion: nil)
        
        
    }

}
