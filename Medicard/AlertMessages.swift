//
//  AlertMessages.swift
//  Medicard
//
//  Created by Al John Albuera on 11/27/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit

class AlertMessages: NSObject {
    
    open static let errorTitle = ""
    open static let successTitle = ""
    open static let uploadPhotoTitle = ""
    open static let noInternetConnectionMessage = "Please check your internet connection."
    open static let successUploadPhoto = "You have successfully uploaded your photo."
    open static let errorUploadPhoto = "Failed to upload photo. Please try again."
    open static let errorAccountIsAlreadyCreated = "Account is already created or not existing."
    open static let successAccountCreated = "You have successfully created an account. Please login to begin."
    open static let errorEmptyFields = "Please fill up the required fields."
    open static let errorPleaseInputDependetID = "Please input Dependent Member ID." 
    open static let errorAlreadAdded = "Something went wrong. Member Account was not found."
    open static let errorNotADependent = "Something went wrong. Member ID Number is not your dependent."
    open static let successAddDependent = "You have successfully added a dependent."
    open static let warningLogout = "Are you sure you want to logout?"
    open static let warningUploadPhoto = "Updating of Photo will request a validation from MediCard. Do you want to continue?"
    open static let errorCredentials = "It seems like you've got the wrong username/password. Please try again."
    open static let errorAccountLocked = "Account is locked due to incorrect input of your username or password 3 times."
    open static let errorNoUserAccount = "No User Account for entered username."
    open static let errorRequestPassword = "MACE cannot connect to the server. Please try again."
    open static let successRequestPassword = "You have successfully requested a new password."
    open static let succesCheckEmailPassword = "Password has been sent to your email address."
    open static let warningPasswordValidation = "Password must have at least one (1) capitalized letter, one (1) number and a minimum of eight (8) characters."
    open static let sucessChangePassword = "You have successfully changed your password."
    open static let successAddedPin = "You have successfully registered a PIN."
    open static let successChangedPin = "You have successfully changed your PIN."
    open static let errorChangePassword = "Something went wrong. Unable to change your password. Please try again."
    open static let warningVerifyMember = "Please make sure that you typed a valid information."
    open static let warningPasswordMinimum = "Password must contain a minimum of 8 characters."
    open static let warningCharacterMinimum = "Username must contain a minimum of 3 characters."
    open static let warningInvalidUsername = "Username invalid. It must not contain blank spaces."
    open static let warningInvalidEmail = "Invalid email address. Please try again."
    open static let warningPasswordDidntMatch = "Password did not match. Please try again."
    open static let warningPasswordRetypePassDidntMatch = "New Password and Re-Type Password did not match. Please try again."
    open static let warningPinRetypePinDidntMatch = "PIN and Re-type PIN fields do not match"
    open static let warningSomethingWrong = "Something went wrong. Please try again."
    open static let warningInvalidNumber = "Invalid mobile number. Please try again."
    open static let warningInvalidMemberCodeAndUsername = "Something went wrong. Incorrect Email Address or MemberCode."
    open static let warningInvalidUsernamePassword = "Something went wrong. Incorrect username or password."
    
    open static let warningMemberAccountDoesntMatch = "Date of Birth does not match with MediCard ID number."
    open static let warningMemberAccountAlreadyExist = "Account is already existing."
    open static let warningInputMemberID = "Please input Dependent member ID."
    
    
    //Account Status Dialog
    open static let warningAccountDisapproved = "Account is Disapproved. Please Call 841-8080 for details."
    open static let warningAccountResigned = "Account is no longer active."
    open static let warningAccountCancelled = "Account is no longer active."
    open static let warningAccountLapse = "Account is no longer active."
    open static let warningAccountForReactivation = "Account is subject to reactivation."
    
    //V2 Messages
    open static let warningMaleUser = "Maternity Consultation is not available for male."
    open static let warningSubmitReq = "Are you sure you want to submit this request?"
    open static let warningSumbitReqHeader = "Confirm Request"
    open static let warningCancelHeader = "Cancel Request"
    open static let warningPleaseInputProblem = "Please enter a Problem/Condition."
    open static let successDownloadedForm = "LoA request has been saved to MediCard folder in your photo gallery."
    open static let warningPinIncorrect = "Old Pin is incorrect. Please Try Again."
    open static let warningCancelLoaRequest = "Are you sure you sure you want to disregard all changes made?"
    open static let warningCancelLoa = "Are you sure you want to cancel your LoA Request?"
    open static let warningPickStartDate = "Please pick start date first."
    open static let warningNoDiagnosisList = "No List for Diagnosis."
    open static let warningDateMustNotBeNull = "Date From or Date To must not be empty."
    open static let warningNoTestList = "No List for Test."
    open static let warningNoPinLoa = "A valid PIN is required to Request for LOA. Please register PIN through Account Settings."
    open static let warningNotValidForReg = "Member account is not valid for registration."
    open static let warningPinCount = "PIN must be 4-digits."
    open static let warningStartIsGreaterThan = "Date from is greater than Date To. Please try again."
    open static let warningDoubleCheck = "There is an existing request with the same details. Submitting this may cause duplicate requests and may affect your existing utilization limits. Are you sure you want to submit this request?"

    
}
