//
//  SelectHospitalViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 12/28/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON
import SCLAlertView



class SelectHospitalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate {

    @IBOutlet var tblSelectHospital: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    
    let defaults = UserDefaults.standard
    
    var hospitalsCode = [NSManagedObject] ()
    var hospitalsName = [NSManagedObject] ()
    var keyword = [NSManagedObject] ()
    var alias = [NSManagedObject] ()
    var category = [NSManagedObject] ()
    var coordinator = [NSManagedObject] ()
    var streetAddress = [NSManagedObject] ()
    var subjectInformation = [NSManagedObject]()
    var specialization: [String] = []
    
    var specializationCode: [String] = []
    var specializationIndex: [String] = []
    
    var searchActive : Bool = false
    var filtered:[String] = []
    var data: [String] = []
    
    var exclusionIDs: [String] = []
    var nullArray: [String] = []
    
    var loadingNotification = MBProgressHUD()
    var loadHospitalIndicator: Int!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
        
        let header = defaults.string(forKey: "consultationHeader")! as String;
        
        if header == "consultation" {
            self.title = "Consultation"
        }else{
            self.title = "Maternity Consultation"
        }
        

        tblSelectHospital.rowHeight = UITableViewAutomaticDimension
        tblSelectHospital.estimatedRowHeight = 1000;
        
        addToolBarMedicarNum()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("Sort By: \(Getter_Setter.finalChosenSortBy)")
        print("Filter Province: \(Getter_Setter.finalChosenProvinceName)")
        print("Filter Cities: \(Getter_Setter.finalChosenCityName)")
        print("Province Code: \(Getter_Setter.finalChosenProvinceCode)")
        print("clinicName: \(Getter_Setter.finalChosenSortBy)")
        
        print("CHOSEN PROVINCE NAME: \(Getter_Setter.finalChosenProvinceNameArray)")
        print("CHOSEN PROVINCE CODE: \(Getter_Setter.finalChosenProvinceCodeArray)")
        
        searchBar.text = Getter_Setter.finalChosenSearch
            
        loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading Hospitals..."
        loadingNotification.isUserInteractionEnabled = false;
        self.view.isUserInteractionEnabled = false
        
        self.getInPatienExclusion()
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    fileprivate func searchBarDidBeginEditing(_ searchBar: UISearchBar) {
        
        if searchBar == searchBar {
            
            ShowKeyboard.animateViewMoving(up: true, moveValue: -100, view: self.view)
            addToolBarMedicarNum()
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectInformation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell:SelectHospitalTableViewCell! = tblSelectHospital.dequeueReusableCell(withIdentifier: "SelectHospitalCell", for: indexPath) as! SelectHospitalTableViewCell
        
        let hospName = subjectInformation[(indexPath as NSIndexPath).row]
        
       

        let strStreet = hospName.value(forKey: "streetAddress") as! String
        let strCity = hospName.value(forKey: "city") as! String
        let strProvince = hospName.value(forKey: "province") as! String
        let strRegion = hospName.value(forKey: "region") as! String
        
        let trimStrStreet = strStreet.trimmingCharacters(in:.whitespaces)
        let trimStrCity = strCity.trimmingCharacters(in:.whitespaces)
        let trimStrProvince = strProvince.trimmingCharacters(in:.whitespaces)
        let trimStrRegion = strRegion.trimmingCharacters(in:.whitespaces)
        
        
        cell.lblHospitalName.text = hospName.value(forKey: "hospitalName") as? String
        cell.lblHospitalAddress.text = trimStrStreet + ", " + trimStrCity + ", " + trimStrProvince + ", " + trimStrRegion
        
        
        if hospName.value(forKey: "phoneNo") as? String == "" {
            
            cell.lblPhoneNumbers.text = "Tel. No. not specified."
            
        }else{
            
            cell.lblPhoneNumbers.text = "Tel. No. : \(hospName.value(forKey: "phoneNo")!)"
            
        }
        
        
        
        if hospName.value(forKey: "contactPerson") as? String == ""{
            
             cell.lblContactPerson.text = "No Contact Person Available."
            
        }else{
            
             cell.lblContactPerson.text = "Contact Person: \(hospName.value(forKey: "contactPerson")!)"
            
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let hospName = subjectInformation[(indexPath as NSIndexPath).row]
        let selectIndicator = defaults.string(forKey: "selectIndicator")! as String;
        
        let strCity = hospName.value(forKey: "city") as! String
        let strProvince = hospName.value(forKey: "province") as! String
        let strRegion = hospName.value(forKey: "region") as! String
        
        let trimStrCity = strCity.trimmingCharacters(in:.whitespaces)
        let trimStrProvince = strProvince.trimmingCharacters(in:.whitespaces)
        let trimStrRegion = strRegion.trimmingCharacters(in:.whitespaces)

        
        
        if selectIndicator == "fromHome"{
            
            let goToSelectDoctor = self.storyboard!.instantiateViewController(withIdentifier: "SelectDoctorViewController") as! SelectDoctorViewController
           
            
            Getter_Setter.selectedHospitalCode = hospName.value(forKey: "hospitalCode") as? String
            Getter_Setter.selectedHospitalName = hospName.value(forKey: "hospitalName") as? String
            Getter_Setter.selectedDeviceID = (GUIDString() as String!)
            Getter_Setter.selectedAddress = hospName.value(forKey: "streetAddress") as? String
            Getter_Setter.selectedphoneNo = hospName.value(forKey: "phoneNo") as? String
            Getter_Setter.selectedCity = trimStrCity
            Getter_Setter.selectedProvince = trimStrProvince
            Getter_Setter.selectedRegion = trimStrRegion
            
            if hospName.value(forKey: "contactPerson") as? String == ""{
                
                Getter_Setter.seletedContactPerson = "No Contact Person Available."

            }else{
                
                Getter_Setter.seletedContactPerson = hospName.value(forKey: "contactPerson") as! String
                
                
            }

            navigationController?.pushViewController(goToSelectDoctor, animated: true)
            
        }else{
            
             let goToSelectDoctor = self.storyboard!.instantiateViewController(withIdentifier: "SelectDoctorViewController") as! SelectDoctorViewController
            
            Getter_Setter.selectedHospitalCode = hospName.value(forKey: "hospitalCode") as? String
            Getter_Setter.selectedHospitalName = hospName.value(forKey: "hospitalName") as? String
            Getter_Setter.selectedDeviceID = (GUIDString() as String!)
            Getter_Setter.selectedAddress = hospName.value(forKey: "streetAddress") as? String
            Getter_Setter.selectedphoneNo = hospName.value(forKey: "phoneNo") as? String
            Getter_Setter.selectedCity = trimStrCity
            Getter_Setter.selectedProvince = trimStrProvince
            Getter_Setter.selectedRegion = trimStrRegion
            
            if hospName.value(forKey: "contactPerson") as? String == ""{
                
                Getter_Setter.seletedContactPerson = "No Contact Person Available."
                
            }else{
                
                Getter_Setter.seletedContactPerson = hospName.value(forKey: "contactPerson") as! String
                
                
            }
            
            navigationController?.pushViewController(goToSelectDoctor, animated: true)

            defaults.set("", forKey: "doctorName")
            defaults.set("", forKey: "doctorSpec")
            defaults.set("", forKey: "doctorCode")
            defaults.set("", forKey: "room")
            defaults.set("", forKey: "schedule")
            
        }
        
        //SPECIALIZATION
        defaults.set(specialization, forKey: "chosenSpecializationName")
        defaults.set(specializationCode, forKey: "chosenSpecializationCode")
        defaults.set(specializationIndex, forKey: "chosenSpecializationIndexes")
        defaults.set("Doctor's Family Name", forKey: "sortByIndicator")
        defaults.set("docLnam", forKey: "sortingData")
        defaults.set("", forKey: "searchDoctorsName")
        defaults.set("", forKey: "doctorsRoom")
        defaults.set("", forKey: "room")
        defaults.set("docLname", forKey: "sortingData")
        defaults.set("", forKey: "filterSpecialization")
        defaults.set("", forKey: "doctorsName")
        defaults.set("", forKey: "hospitalsName")
        
        searchBar.text = ""
        
        
    }
    
    
    func GUIDString() -> NSString {
        let newUniqueID = CFUUIDCreate(kCFAllocatorDefault)
        let newUniqueIDString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueID);
        let guid = newUniqueIDString as! NSString
        
        return guid.lowercased as NSString
    }
    
  
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "HospitalsEntity")
        let managedContext2 = appDelegate.managedObjectContext
        let fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "HospitalsEntity")
        let provinceName = Getter_Setter.finalChosenProvinceName.trimmingCharacters(in: .whitespaces)
        let sortBy = Getter_Setter.finalChosenSortBy
        let cityName = Getter_Setter.finalChosenCityName
        
        print("PROVINCE: \(provinceName)")
        
        if sortBy == "hospitalName" || sortBy == "category" ||  sortBy == "city" ||  sortBy == "province" && provinceName == "All Province" && cityName.count == 0{
            
            print("DITO KAYA")
            
            let predicate1 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", searchBar.text!)
            let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
            fetchRequest.predicate = motherPredicate
            
            let predicate2 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", " " + searchBar.text!)
            let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
            fetchRequest2.predicate = motherPredicate2
            
        }else if sortBy == "hospitalName" || sortBy == "category" ||  sortBy == "city" ||  sortBy == "province" && provinceName != "All Province" && cityName.count == 0{
            
            print("DITO")
            
            let predicate1 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@ AND province MATCHES [cd] %@", searchBar.text!, provinceName)
            let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
            fetchRequest.predicate = motherPredicate
            
            let predicate2 = NSPredicate(format: "hospitalName CONTAINS [cd] %@ AND province MATCHES [cd] %@", " " + searchBar.text!, provinceName)
            let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
            fetchRequest2.predicate = motherPredicate2
            
            
        }else if sortBy == "hospitalName" || sortBy == "category" ||  sortBy == "city" ||  sortBy == "province" && provinceName == "All Province" && cityName.count != 0{
            
            print("OR DITO KAYA")
            
            let predicate1 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@ AND province MATCHES [cd] %@ AND city IN %@",  searchBar.text!, provinceName, cityName)
            let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
            fetchRequest.predicate = motherPredicate
            
            let predicate2 = NSPredicate(format: "hospitalName CONTAINS [cd] %@ AND province MATCHES [cd] %@ AND city IN %@", " " + searchBar.text!, provinceName, cityName)
            let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
            fetchRequest2.predicate = motherPredicate2
            
        }
        
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest2.returnsObjectsAsFaults = false //needed to make data accessible
        
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            let results2 = try managedContext2.fetch(fetchRequest2)
            
            print("RESULTSSSSS: \(results)")
            print("RESULTSSSSS22222: \(results2)")
            
            if searchBar.text == ""{
                
                self.fetchAllHospital()
                
                tblSelectHospital.isHidden = false
                
            }else if results.count == 0{
                
                tblSelectHospital.isHidden = true
                
            }else{
                subjectInformation = results + results2
                tblSelectHospital.isHidden = false
            }
            
            
        } catch {
            
            print("uh oh")
            
        }
        
        tblSelectHospital.reloadData()
        
    }
    

    
    func addToolBarMedicarNum(){
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        searchBar.inputAccessoryView = toolBar
        
        
    }
    
    func pickerDoneClicked(){
        
        searchBar.resignFirstResponder()
        
    }
    
    
    func getInPatienExclusion(){
        
        
        let memberCode = defaults.value(forKey: "memberCode") as! String
        
        let inPatientExclusionParams = ["memberCode": memberCode]
        
        let request = Alamofire.request(PostRouter.getInPatienExclusion(inPatientExclusionParams as [String: AnyObject]))
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.loadingNotification.isUserInteractionEnabled = true;
                    self.view.isUserInteractionEnabled = true
                    
                     _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    let exclutionID = post["exclusions"]
                
                    
                    for ids in exclutionID.arrayValue{
                    
                        self.exclusionIDs.append(ids.stringValue)
                        
                    }
                    
                    
                    if Getter_Setter.finalChosenSearch == "" && Getter_Setter.finalChosenSortBy == "" && Getter_Setter.finalMedicardClinicFirst == false && Getter_Setter.finalChosenProvinceName == "All Province" && Getter_Setter.finalChosenCityName == self.nullArray{
                        
                        self.loadHospitalIndicator = 1
                        self.fetchAllHospital()
                        
                    }else if Getter_Setter.finalChosenSearch == "" && Getter_Setter.finalChosenSortBy == "hospitalName" && Getter_Setter.finalMedicardClinicFirst == false && Getter_Setter.finalChosenProvinceName == "All Province" && Getter_Setter.finalChosenCityName == self.nullArray{
                        
                        self.loadHospitalIndicator = 0
                        self.fetchAllHospital()
                        
                    }else if Getter_Setter.finalChosenSearch == "" && Getter_Setter.finalChosenSortBy == "category" && Getter_Setter.finalMedicardClinicFirst == false && Getter_Setter.finalChosenProvinceName == "All Province" && Getter_Setter.finalChosenCityName == self.nullArray{
                        
                        self.loadHospitalIndicator = 1
                        self.fetchAllHospital()
                        
                    }else{
                        self.fetchFilterHospitas()
                    }
    
                    
                    

                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.loadingNotification.isUserInteractionEnabled = true;
                    self.view.isUserInteractionEnabled = true


                    
                }
        }
        debugPrint(request)
    }
    
    
    
    func fetchFilterHospitas(){
        
        self.deleteExclusions()
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "HospitalsEntity")
        
        if Getter_Setter.finalChosenSearch == "" {
            
            if Getter_Setter.finalMedicardClinicFirst == true{
                
                if Getter_Setter.finalChosenProvinceName == "All Province"{
                    
                    if Getter_Setter.finalChosenCityName.count == 0{
                        
                        //CORRECT
                        
                        let predicate2 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2])
                        fetchRequest.predicate = motherPredicate
                        
                    }else{
                        
                        //CORRECT
                        
                        let predicate1 = NSPredicate(format: "city IN [cd] %@", Getter_Setter.finalChosenCityName)
                        let predicate2 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                        fetchRequest.predicate = motherPredicate
                        
                    }
                    
                    
                    
                }else{
                    
                    if Getter_Setter.finalChosenCityName.count == 0{
                        
                        //CORRECT
                        
                        let predicate1 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                        let predicate2 = NSPredicate(format: "province IN [cd] %@", Getter_Setter.finalChosenProvinceNameArray)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate2, predicate1])
                        fetchRequest.predicate = motherPredicate
                        
                    }else{
                        
                        //CORRECT
                        
                        let predicate1 = NSPredicate(format: "city IN [cd] %@", Getter_Setter.finalChosenCityName)
                        let predicate2 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                        let predicate3 = NSPredicate(format: "province MATCHES [cd] %@", Getter_Setter.finalChosenProvinceName)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                        fetchRequest.predicate = motherPredicate
                        
                    }


                    
                }
                
                
            }else{
                
                if Getter_Setter.finalChosenProvinceName == "All Province"{
                    
                    if Getter_Setter.finalChosenCityName.count == 0{

                        
                        self.fetchAllHospital()
                        
                        
                    }else{
                        
                        let predicate1 = NSPredicate(format: "city IN [cd] %@", Getter_Setter.finalChosenCityName)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                        fetchRequest.predicate = motherPredicate
                        

                    }
                    
                    
                }else{
                    
                    if Getter_Setter.finalChosenCityName.count == 0{
                        
                        let predicate1 = NSPredicate(format: "province MATCHES [cd] %@", Getter_Setter.finalChosenProvinceName)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                        fetchRequest.predicate = motherPredicate
                        
                    }else{
                        
                        
                        let predicate1 = NSPredicate(format: "province IN [cd] %@", Getter_Setter.finalChosenProvinceNameArray)
                        let predicate2 = NSPredicate(format: "city IN [cd] %@", Getter_Setter.finalChosenCityName)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2])
                        fetchRequest.predicate = motherPredicate
                        
                    }

                    
                }
                
            }
            
        }else{
            
            if Getter_Setter.finalMedicardClinicFirst == true{
                
                if Getter_Setter.finalChosenProvinceName == "All Province"{
                    
                    if Getter_Setter.finalChosenCityName.count == 0{
                        
                        let predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", self.searchBar.text!)
                        let predicate2 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2])
                        fetchRequest.predicate = motherPredicate

                        
                    }else{
                        
                        let predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", self.searchBar.text!)
                        let predicate2 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                        let predicate3 = NSPredicate(format: "province IN [cd] %@", Getter_Setter.finalChosenProvinceNameArray)
                        let predicate4 = NSPredicate(format: "city IN [cd] %@", Getter_Setter.finalChosenCityName)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2, predicate3, predicate4])
                        fetchRequest.predicate = motherPredicate
                        
                    }

                    
                }else{
                    
                    let predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", self.searchBar.text!)
                    let predicate2 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                    let predicate3 = NSPredicate(format: "province IN [cd] %@", Getter_Setter.finalChosenProvinceNameArray)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2, predicate3])
                    fetchRequest.predicate = motherPredicate

                    
                }
                
                
            }else{
                
                if Getter_Setter.finalChosenProvinceName == "All Province"{
                    
                    if Getter_Setter.finalChosenCityName.count == 0{
                        
                        let predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", self.searchBar.text!)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                        fetchRequest.predicate = motherPredicate
                        
                        
                    }else{
                        
                        let predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", self.searchBar.text!)
                        let predicate2 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
                        let predicate3 = NSPredicate(format: "province IN [cd] %@", Getter_Setter.finalChosenProvinceNameArray)
                        let predicate4 = NSPredicate(format: "city IN [cd] %@", Getter_Setter.finalChosenCityName)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2, predicate3, predicate4])
                        fetchRequest.predicate = motherPredicate
                        
                    }
                    
                    
                }else{
                    
                     if Getter_Setter.finalChosenCityName.count == 0{
                        
                        let predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", self.searchBar.text!)
                        let predicate3 = NSPredicate(format: "province IN [cd] %@", Getter_Setter.finalChosenProvinceNameArray)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate3])
                        fetchRequest.predicate = motherPredicate
                        
                     }else{
                        
                        let predicate1 = NSPredicate(format: "hospitalName CONTAINS [cd] %@", self.searchBar.text!)
                        let predicate2 = NSPredicate(format: "city IN [cd] %@", Getter_Setter.finalChosenCityName)
                        let predicate3 = NSPredicate(format: "province IN [cd] %@", Getter_Setter.finalChosenProvinceNameArray)
                        let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2, predicate3])
                        fetchRequest.predicate = motherPredicate

                        
                    }

                }

                
            }
            
        }
        
        
        
        let sortDescriptor = NSSortDescriptor(key: Getter_Setter.finalChosenSortBy, ascending: true)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            subjectInformation = results
            tblSelectHospital.reloadData()
            
        } catch {
            
            print("uh oh")
            
        }


    }
    
    
    func fetchAllHospital(){
        
        self.deleteExclusions()
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        let managedContext2 = appDelegate.managedObjectContext

        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "HospitalsEntity")
        let fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "HospitalsEntity")
        
        let predicate1 = NSPredicate(format: "hospitalName BEGINSWITH [cd] %@", "MEDICard PHILIPPINES, INC")
        let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
        fetchRequest2.predicate = motherPredicate
        fetchRequest2.returnsObjectsAsFaults = false
        
        let sortDescriptor = NSSortDescriptor(key: "hospitalName", ascending: true)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            let results2 = try managedContext2.fetch(fetchRequest2)
            
            print("HOSPITAL INDICATOR: \(loadHospitalIndicator)")
            
            if loadHospitalIndicator == 0{
                subjectInformation = results
            }else{
                subjectInformation = results2  + results
            }
            
             print("HEREEE!!")
            
            tblSelectHospital.reloadData()
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
    
    
    func deleteExclusions(){
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "HospitalsEntity")
        fetchRequest.predicate = NSPredicate(format: "hospitalCode IN %@", exclusionIDs)
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            
            for entity in results {
                managedContext.delete(entity)
                subjectInformation = [entity]
            }
            
            self.tblSelectHospital.reloadData()
            
            
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        
    }
    
    
    @IBAction func sortAndFilterAction(_ sender: Any) {
        
        let goToSortingAndFiltering = self.storyboard!.instantiateViewController(withIdentifier: "NewSortAndFilterHospitalViewController") as! NewSortAndFilterHospitalViewController
        let navController = UINavigationController(rootViewController: goToSortingAndFiltering)
        self.present(navController, animated:true, completion: nil)

        
        
    }

    
    
    

}

