//
//  MultipleSelectionLoaTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 2/21/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class MultipleSelectionLoaTableViewCell: UITableViewCell {
    
     @IBOutlet var lblMultipleSelection: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

            
        if (selected == true) {
            lblMultipleSelection.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
            lblMultipleSelection.textColor = UIColor.white
            lblMultipleSelection.layer.cornerRadius = 5
            lblMultipleSelection.clipsToBounds = true
            
        } else {
            lblMultipleSelection.backgroundColor = UIColor.white
            lblMultipleSelection.textColor = UIColor.black
            lblMultipleSelection.layer.cornerRadius = 5
            lblMultipleSelection.clipsToBounds = true
        }
  
    }

}
