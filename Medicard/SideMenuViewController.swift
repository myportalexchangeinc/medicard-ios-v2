
//
//  SideMenuViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 10/11/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView
import Alamofire
import CoreData

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var tblSideMenu: UITableView!
    @IBOutlet var imgProfilePic: UIImageView!
    @IBOutlet var btnLogout: UIButton!
    
    var nullStringArray: [String] = []
    var nullIntArray: [Int] = []
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblSideMenu.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tblSideMenu.sizeToFit()
        tblSideMenu.contentInset = UIEdgeInsetsMake(-30, 0, 0, 0);

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var CellIdentifier = "Cell"
        
        switch indexPath.row {
        case 0:
            CellIdentifier = "myAccount"
        case 1:
            CellIdentifier = "loaRequestCell"
        case 2:
            CellIdentifier = "accountSettings"
        case 3:
            CellIdentifier = "logout"
       
            
        default: break
            
        }
        
         let cell = tblSideMenu.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1{
            
            Getter_Setter.chosenSortBy = "Request Date"
            Getter_Setter.chosenStatus = "Choose Status..."
            Getter_Setter.chosenServiceType = "Choose Service Type..."
            Getter_Setter.chosenSearch = ""
            Getter_Setter.chosenRequestDateTo = ""
            Getter_Setter.chosenRequestDateFrom = ""
            
            Getter_Setter.finalChosenSortBy = Getter_Setter.chosenSortBy
            Getter_Setter.finalChosenStatus = Getter_Setter.chosenStatus
            Getter_Setter.finalChosenServiceType = Getter_Setter.chosenServiceType
            Getter_Setter.finalChosenSearch = Getter_Setter.chosenSearch
            Getter_Setter.finalChosenRequestDateFrom = Getter_Setter.chosenRequestDateFrom
            Getter_Setter.finalChosenRequestDateTo = Getter_Setter.chosenRequestDateTo
            Getter_Setter.finalChosenDoctorName = nullStringArray
            Getter_Setter.finalChosenHospitalName = nullStringArray
            Getter_Setter.finalChosenDoctorIndexes = nullIntArray
            Getter_Setter.finalChosenHospitalIndexes = nullIntArray
            
            Getter_Setter.sortingAndFilterIndicator = 0

            
        }else if indexPath.row == 3{
            
            let alert = SCLAlertView()
            _ = alert.addButton("YES"){
                
                let defaults = UserDefaults.standard
                defaults.set("0", forKey: "loginStatus")
                defaults.set("", forKey: "username")
                defaults.set("", forKey: "password")
                defaults.set("", forKey: "storedUsername")
                defaults.set("", forKey: "storedPassword")
                
                self.deleteDataEntity(entityDescription: "HospitalsEntity")
                self.deleteDataEntity(entityDescription: "DoctorsEntity")
                self.deleteDataEntity(entityDescription: "CityEntity")
                self.deleteDataEntity(entityDescription: "ProvinceEntity")
                self.deleteDataEntity(entityDescription: "SpecializationEntity")
                self.deleteDataEntity(entityDescription: "LoaHistoryEntity")
                
                let goToMainView = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                goToMainView.modalTransitionStyle = .crossDissolve
                self.present(goToMainView, animated:true, completion: nil)
                
            }
            
            let icon = UIImage(named:"ic_white_logout.png")
            //#231F20
            let color = UIColor(red:35/255.0, green:31/255.0, blue:32/255.0, alpha: 1.0)
            
            _ = alert.showCustom("", subTitle: "Are you sure you want to logout?", color: color, icon: icon!)
            
            
        }
        
    }
    
    
    func deleteDataEntity(entityDescription: String){

       let appDelegate = UIApplication.shared.delegate as! AppDelegate
       let managedContext = appDelegate.managedObjectContext
       
       let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityDescription)
       let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
       
       do {
           try managedContext.execute(batchDeleteRequest)
           
       } catch {
           // Error Handling
       }
    }
}
