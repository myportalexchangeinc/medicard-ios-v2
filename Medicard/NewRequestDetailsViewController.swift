//
//  NewRequestDetailsViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 1/25/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView
import ReachabilitySwift
import Alamofire
import SwiftyJSON


class NewRequestDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    
    let defaults = UserDefaults.standard
    var btnTag    : Int = 0
    var doctorIndicator: String!
    
    let validateForm = FormValidation()
    let reachability = Reachability()
    
    var doctorName: String!
    var doctorSpecs: String!
    var doctorRoom: String!
    var doctorSched: String!
    var hospitalsName: String!
    var streetAddress: String!
    var phoneNumbers: String!
    var contactPerson: String!
    var clinicHours: String!
    var province: String!
    var city: String!
    var region: String!
    
    var specialization: [String] = []
    var specializationCode: [String] = []
    var specializationIndex: [String] = []
    
    var strApprovalNumber: String!
    var strRemarks: String!
    var strWithProvider: String!
    
    
    
    @IBOutlet var tblRequestDetails: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        let header = defaults.string(forKey: "consultationHeader")! as String;
        
        if header == "consultation" {
            self.title = "Consultation"
        }else{
            self.title = "Maternity Consultation"
        }

        
        tblRequestDetails.rowHeight = UITableViewAutomaticDimension
        tblRequestDetails.estimatedRowHeight = 1000;
        

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    
        defaults.set("fromHome", forKey: "selectIndicator")
        defaults.set("", forKey: "chosenProvinceCode")
        defaults.set("Hospital/Clinic Name", forKey: "filterSortBy")
        defaults.set("All Provinces", forKey: "filterProvince")
        defaults.set("All Cities", forKey: "filterCity")
        
        //SPECIALIZATION
        defaults.set(specialization, forKey: "chosenSpecializationName")
        defaults.set(specializationCode, forKey: "chosenSpecializationCode")
        defaults.set(specializationIndex, forKey: "chosenSpecializationIndexes")
        defaults.set("Doctor's Family Name", forKey: "sortByIndicator")
        defaults.set("docLnam", forKey: "sortingData")
        defaults.set("", forKey: "searchDoctorsName")
        defaults.set("", forKey: "doctorsRoom")
        defaults.set("", forKey: "roomNumber")
        defaults.set("docLname", forKey: "sortingData")
        defaults.set("", forKey: "filterSpecialization")
        defaults.set("", forKey: "doctorsName")
        defaults.set("", forKey: "hospitalsName")

        self.navigationItem.backBarButtonItem?.isEnabled = false
        self.navigationItem.backBarButtonItem?.image = nil
        navigationItem.backBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        
        
        
        
        doctorName = defaults.value(forKey: "doctorName") as! String!
        doctorSpecs = defaults.value(forKey: "doctorSpec") as! String!
        hospitalsName = Getter_Setter.selectedHospitalName
        streetAddress = Getter_Setter.selectedAddress
        phoneNumbers = Getter_Setter.selectedphoneNo
        contactPerson = Getter_Setter.seletedContactPerson
        clinicHours = Getter_Setter.selectedDoctorSched
        city = Getter_Setter.selectedCity
        province = Getter_Setter.selectedProvince
        region = Getter_Setter.selectedRegion
        doctorRoom = defaults.value(forKey: "room") as! String!
        doctorSched = defaults.value(forKey: "schedule") as! String!
        
        
        tblRequestDetails.reloadData()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let indexPath = IndexPath(row: 0, section: 4)
        let cell = (tblRequestDetails.cellForRow(at: indexPath)! as! DoneRequestDetailsTableViewCell)
        
        if textView == cell.tvConditions {
            
            tblRequestDetails.contentInset = UIEdgeInsetsMake(0, 0, 250, 0)
            tblRequestDetails.scrollToRow(at: indexPath, at: .top, animated: true)

            
        }
        
    }
    

    func textViewDidEndEditing(_ textView: UITextView) {
        
        let indexPath = IndexPath(row: 0, section: 4)
        let cell = (tblRequestDetails.cellForRow(at: indexPath)! as! DoneRequestDetailsTableViewCell)
        
        if textView == cell.tvConditions {
            
            cell.tvConditions.resignFirstResponder()
             tblRequestDetails.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            
        }
        
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let indexPath = IndexPath(row: 0, section: 3)
        
        tblRequestDetails.contentInset = UIEdgeInsetsMake(0, 0, 250, 0)
        tblRequestDetails.scrollToRow(at: indexPath, at: .top, animated: true)

        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        textField.resignFirstResponder()
        tblRequestDetails.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    


    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView.text == "Enter Reason for Consult..."{
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text == ""{
            textView.text = ""
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let indexPath = IndexPath(row: 0, section: 4)
        let cell = (tblRequestDetails.cellForRow(at: indexPath)! as! EnterDoctorTableViewCell)
        
        cell.edtEnterDoctor.resignFirstResponder()
        
        return true
    }
    
    func tableViewHeight() -> CGFloat {
        tblRequestDetails.layoutIfNeeded()
        
        return tblRequestDetails.contentSize.height
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            
            return 1
            
        }else if section == 1{
            
            return 1
            
        }else if section == 2{
            
            return 1
            
        }else if section == 3{
            
            return 1
            
        }else{
            
            return 1
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0{
            
            let cellIdentifier = "Cell"
            let cell: UITableViewCell! = tblRequestDetails.dequeueReusableCell(withIdentifier: cellIdentifier)
            
            
            cell.textLabel?.text = "REQUEST DETAILS"
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            cell.textLabel?.textAlignment = .center
            
            cell.separatorInset = UIEdgeInsetsMake(0, -1000, 0, 0)
            
            return cell
            
        }else if indexPath.section == 1 {
            
            let cell:HospitalRequestDetailsTableViewCell! = tblRequestDetails.dequeueReusableCell(withIdentifier: "HospitalCell", for: indexPath) as! HospitalRequestDetailsTableViewCell
            
            cell.lblHospitalName.text = hospitalsName
            cell.lblHospitalAddress.text = streetAddress + ", " + city + ", " + province + ", " + region
            cell.lblContactPerson.text = contactPerson
            cell.lblPhoneNumbers.text = phoneNumbers
            
            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.section == 2{
            
            let cell:DoctorRequestDetailsTableViewCell! = tblRequestDetails.dequeueReusableCell(withIdentifier: "DoctorCell", for: indexPath) as! DoctorRequestDetailsTableViewCell
            
           
            
            
            if doctorName == "" && doctorSpecs == "" && doctorRoom == "" && doctorSched == ""{
                
                cell.lblDoctorName.isHidden = true
                cell.lblDoctorSkill.isHidden = true
                cell.lblRoom.isHidden = true
                cell.lblClinicHours.isHidden = true
                cell.lblAlertSelectDoctor.isHidden = false
                
            }else{
                
                cell.lblDoctorName.isHidden = false
                cell.lblDoctorSkill.isHidden = false
                cell.lblRoom.isHidden = false
                cell.lblClinicHours.isHidden = false
                cell.lblAlertSelectDoctor.isHidden = true
                
                cell.lblDoctorName.text = doctorName
                cell.lblDoctorSkill.text = doctorSpecs
                cell.lblRoom.text = doctorRoom
                cell.lblClinicHours.text = doctorSched
                
            }
            
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.section == 3{
            
            let cell:EnterDoctorTableViewCell! = tblRequestDetails.dequeueReusableCell(withIdentifier: "enterDoctorCell", for: indexPath) as! EnterDoctorTableViewCell
            
            cell.edtEnterDoctor.layer.borderWidth = 1
            cell.edtEnterDoctor.layer.borderColor = UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0).cgColor
            cell.edtEnterDoctor.layer.cornerRadius = 3
            cell.selectionStyle = .none
            
            addToolBarMedicarNum(textField: cell.edtEnterDoctor)
            
            cell.selectDoctor.addTarget(self, action: #selector(selectDoctorAction(sender:)), for: .touchUpInside)
            
            return cell
            
            
        }else{
            
            
             let cell:DoneRequestDetailsTableViewCell! = tblRequestDetails.dequeueReusableCell(withIdentifier: "doneCell", for: indexPath) as! DoneRequestDetailsTableViewCell
            
            

            cell.btnSubmitTransaction.layer.cornerRadius = 2
            cell.btnCancelTransaction.layer.cornerRadius = 2
            cell.btnCancelTransaction.layer.borderWidth = 1
            cell.btnCancelTransaction.layer.borderColor = UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0).cgColor
            
            cell.tvConditions.layer.borderWidth = 1
            cell.tvConditions.layer.borderColor = UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0).cgColor
            cell.tvConditions.layer.cornerRadius = 2
            cell.tvConditions.text = "Enter Reason for Consult..."
            cell.tvConditions.textColor = UIColor.lightGray
            
            cell.btnCheckBox.isSelected = false
            
            
            //CURRENT DATE MMM dd, YYYY Format
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, YYYY"
            let result = formatter.string(from: date)
            defaults.set(result, forKey: "dateApproved")
            
            //CURRENT DATE MM/dd/YYYY Format
            formatter.dateFormat = "MM/dd/YYYY"
            let result2 = formatter.string(from: date)
            defaults.set(result2, forKey: "dateApprovedNewFormat")
            
            //ADDED 3DAYS FROM CURRENT DATE
            let tomorrow = Calendar.current.date(byAdding: .day, value: 3, to: date)
            formatter.dateFormat = "MM/dd/YYYY"
            let result3 = formatter.string(from: tomorrow!)
            defaults.set(result3, forKey: "dateExpiration")
            
            cell.lblDueDate.text = "This form is valid from " + result2 + " to " + result3 + "."
            
            cell.btnCheckBox.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            cell.btnSubmitTransaction.addTarget(self, action: #selector(submitButton(_:)), for: .touchUpInside)
            cell.btnCancelTransaction.addTarget(self, action: #selector(cancelAction(_:)), for: .touchUpInside)
            cell.btnTermsAndCondition.addTarget(self, action: #selector(showTermsAndConditions(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            
            addToolBarMedicarNum(textView: cell.tvConditions)
            
            return cell
            
        }
        

        
        
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1{
            
            let goToSelectHospital = self.storyboard!.instantiateViewController(withIdentifier: "SelectHospitalViewController") as! SelectHospitalViewController
            defaults.set("fromDetails", forKey: "selectIndicator")
            navigationController?.pushViewController(goToSelectHospital, animated: true)
            
        }else if indexPath.section == 2{
            
            let goToSelectDoctor = self.storyboard!.instantiateViewController(withIdentifier: "SelectDoctorViewController") as! SelectDoctorViewController
            defaults.set("fromDetails", forKey: "selectIndicator")
            navigationController?.pushViewController(goToSelectDoctor, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let indicator = defaults.value(forKey: "tableIndicator") as! String
        
        if indicator == "choose"{
            
            if indexPath.section == 1 {
                return UITableViewAutomaticDimension
            }else if indexPath.section == 2{
                return UITableViewAutomaticDimension
            }else if indexPath.section == 3{
                return 0.0
            }else if indexPath.section == 4{
                
                return 260
            }else{
                
                return 50
            }
            
        }else{
            
            if indexPath.section == 1 {
                return UITableViewAutomaticDimension
            }else if indexPath.section == 2{
                return 0.0
            }else if indexPath.section == 3{
                
                return 100
            }else if indexPath.section == 4{
                
                return 260
            }else{
                
                return 50
            }
            
        }
        
    }

    
    
    func addToolBarMedicarNum(textView: UITextView){
        
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        textView.inputAccessoryView = toolBar
        
        
    }
    
    
    
    func pickerDoneClicked(){
        
        let indexPath = IndexPath(row: 0, section: 4)
        let cell = (tblRequestDetails.cellForRow(at: indexPath)! as! DoneRequestDetailsTableViewCell)
        
        cell.tvConditions.resignFirstResponder();
        
    }
    
    
    @IBAction func checkAction(_ sender: Any) {
        
        let indexPath = IndexPath(row: 0, section: 4)
        let cell = (tblRequestDetails.cellForRow(at: indexPath)! as! DoneRequestDetailsTableViewCell)
        
        if (cell.btnCheckBox.isSelected == true)
        {
            cell.btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
            cell.btnCheckBox.isSelected = false
            cell.btnSubmitTransaction.backgroundColor = UIColor.lightGray
            cell.btnSubmitTransaction.isEnabled = false
            cell.btnSubmitTransaction.isUserInteractionEnabled = false
        }else{
            cell.btnCheckBox.setBackgroundImage(UIImage(named: "ic_selected.png"), for: UIControlState.normal)
            cell.btnCheckBox.isSelected = true
            cell.btnSubmitTransaction.backgroundColor = UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0)
            cell.btnSubmitTransaction.isEnabled = true
            cell.btnSubmitTransaction.isUserInteractionEnabled = true
        }
        
        
    }
    
    @IBAction func selectDoctorAction(sender: UIButton){
        
        let goToSelectDoctor = self.storyboard!.instantiateViewController(withIdentifier: "SelectDoctorViewController") as! SelectDoctorViewController
        defaults.set("fromDetails", forKey: "selectIndicator")
        navigationController?.pushViewController(goToSelectDoctor, animated: true)
        
    }
    
    
    @IBAction func submitButton(_ sender: Any) {
        
        let indexPath = IndexPath(row: 0, section: 3)
        let cell = (tblRequestDetails.cellForRow(at: indexPath)! as! EnterDoctorTableViewCell)
        
        let indexPath2 = IndexPath(row: 0, section: 4)
        let cell2 = (tblRequestDetails.cellForRow(at: indexPath2)! as! DoneRequestDetailsTableViewCell)
        
        
        let indicator = self.defaults.value(forKey: "tableIndicator") as! String
        
        cell2.tvConditions.resignFirstResponder()
        
        
        if indicator == "choose"{
            
            if doctorName == "" || doctorSpecs == ""{
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
                
                cell2.btnCheckBox.isSelected = false
                cell2.btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
                cell2.btnSubmitTransaction.backgroundColor = UIColor.lightGray
                cell2.btnSubmitTransaction.isEnabled = false
                cell2.btnSubmitTransaction.isUserInteractionEnabled = false
                tblRequestDetails.reloadData()
                
            }else{
                
                if cell2.tvConditions.text == "" {
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
                    
                    cell2.btnCheckBox.isSelected = false
                    cell2.btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
                    cell2.btnSubmitTransaction.backgroundColor = UIColor.lightGray
                    cell2.btnSubmitTransaction.isEnabled = false
                    cell2.btnSubmitTransaction.isUserInteractionEnabled = false
                    tblRequestDetails.reloadData()
                    
                }else{
                    
                    requestLoa()
                    
                }
                
            }
            
        }else if indicator == "input"{
            
            if cell.edtEnterDoctor.text == "" {
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
                
                cell2.btnCheckBox.isSelected = false
                cell2.btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
                tblRequestDetails.reloadData()
                
            }else{
                
                if cell2.tvConditions.text == "" {
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
                    
                    cell2.btnCheckBox.isSelected = false
                    cell2.btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
                    tblRequestDetails.reloadData()
                    
                }else{
                    
                    requestLoa()
                    
                }
                
            }
            
        }else{
            
            requestLoa()
            
        }
        
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {

        
        let alertController = UIAlertController(title: "", message: AlertMessages.warningCancelLoaRequest, preferredStyle: UIAlertControllerStyle.alert)
        let cancelButton = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
            
        }
        
        let okButton = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            
            if self.reachability?.isReachable == true{
                
                 self.navigationController?.popToRootViewController(animated: true)
                
            }else{
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
                
            }
            
        }
        alertController.addAction(cancelButton)
        alertController.addAction(okButton)
        self.present(alertController, animated: true, completion: nil)
        
        
       
        
    }
    
    
    func requestLoa (){
        
        let alertController = UIAlertController(title: "", message: AlertMessages.warningSubmitReq, preferredStyle: UIAlertControllerStyle.alert)
        let cancelButton = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
            
        }
        
        let okButton = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            
            let indicator = self.defaults.value(forKey: "tableIndicator") as! String
            
           
            if self.reachability?.isReachable == true{
                
                if indicator == "choose"{
                    
                    let indexPath = IndexPath(row: 0, section: 4)
                    let cell = (self.tblRequestDetails.cellForRow(at: indexPath)! as! DoneRequestDetailsTableViewCell)
                    
                    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Requesting LoA..."
                    loadingNotification.isUserInteractionEnabled = false;
                    
                    self.chooseDoctor(Getter_Setter.selectedDeviceID, doctorCode: self.defaults.value(forKey: "doctorCode") as! String, hospitalCode: Getter_Setter.selectedHospitalCode, memberCode: self.defaults.value(forKey: "memberCode") as! String, username: self.defaults.value(forKey: "storedUsername") as! String, problem: cell.tvConditions.text)
                    
                }else{
                    
                    let indexPath = IndexPath(row: 0, section: 3)
                    let cell = (self.tblRequestDetails.cellForRow(at: indexPath)! as! EnterDoctorTableViewCell)
                    
                    let indexPath2 = IndexPath(row: 0, section: 4)
                    let cell2 = (self.tblRequestDetails.cellForRow(at: indexPath2)! as! DoneRequestDetailsTableViewCell)
                    
                    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Requesting LoA..."
                    loadingNotification.isUserInteractionEnabled = false;
                    
                    self.chooseDoctor(Getter_Setter.selectedDeviceID, doctorCode: cell.edtEnterDoctor.text!, hospitalCode: Getter_Setter.selectedHospitalCode, memberCode: self.defaults.value(forKey: "memberCode") as! String, username: self.defaults.value(forKey: "storedUsername") as! String, problem: cell2.tvConditions.text)
                    
                }
                
            }else{
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
                
            }
            
        }
        alertController.addAction(cancelButton)
        alertController.addAction(okButton)
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    
    func requestCheckDouble(batchCode: String!, requestStatus: Int!, providerStatus: String!){
        
        
        let alertController = UIAlertController(title: "", message: AlertMessages.warningDoubleCheck, preferredStyle: UIAlertControllerStyle.alert)
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
            
        }
        
        let okButton = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            
            if self.reachability?.isReachable == true{
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Requesting LoA..."
                loadingNotification.isUserInteractionEnabled = false;
                
                self.checkDoubleRequest(batchCode: batchCode, requestStatus: requestStatus, providerStatus: providerStatus)

            }else{
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
                
            }
            
        }
        alertController.addAction(cancelButton)
        alertController.addAction(okButton)
        self.present(alertController, animated: true, completion: nil)
        
    }

    
    func chooseDoctor (_ deviceId: String, doctorCode: String, hospitalCode: String, memberCode: String, username: String, problem: String){
        
       
        
        
        let loaIndicator = self.defaults.value(forKey: "loaIndicator") as! String
        let indicator = self.defaults.value(forKey: "tableIndicator") as! String
        
         print("CHOOSE DOCTOR: \(loaIndicator)")
        
        if loaIndicator == "consultationLoA"{
            
            let requestConsultationParams = ["deviceID": deviceId, "diagnosiscode": "", "doctorCode": doctorCode, "hospitalCode": hospitalCode, "locationCode": "", "memberCode": memberCode, "procedureAmount": 0, "procedureCode": "", "procedureDesc": "", "username": username, "primaryComplaint": problem, "diagnosisDesc": ""] as [String : Any]
            
            print("Params: \(requestConsultationParams)")
            
            let request = Alamofire.request(PostRouter.postRequestConsultation(requestConsultationParams as [String: AnyObject]))
                .responseJSON { response in
                    guard response.result.error == nil else {
                        print(response.result.error!)
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                        
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        
                        return
                    }
                    
                    if let value: AnyObject = response.result.value as AnyObject? {
                        let post = JSON(value)
                        print("JSON DATA" + post.description)
                        
                        
                        if post["responseCode"].stringValue == "200"{
                            
                            self.defaults.setValue(post["approvalNo"].stringValue, forKey: "approvalNumber")
                            self.defaults.setValue(problem, forKey: "problemCondition")
                            
                            if indicator == "input"{
                                
                                self.defaults.set(doctorCode, forKey: "doctorName")
                                self.defaults.set("Specialization not specified.", forKey: "doctorSpec")
                                
                            }
                            
                            let goToRequestLoaStatus = self.storyboard!.instantiateViewController(withIdentifier: "RequestLoaStatusViewController") as! RequestLoaStatusViewController
                            goToRequestLoaStatus.requestStatus = 0
                            goToRequestLoaStatus.providerAccountStatus = post["withProvider"].stringValue
                            self.navigationController?.pushViewController(goToRequestLoaStatus, animated: true)
                            
                            
                        }else if post["responseCode"].stringValue == "210"{
                            
                            self.defaults.setValue(post["approvalNo"].stringValue, forKey: "approvalNumber")
                            self.defaults.setValue(problem, forKey: "problemCondition")
                            
                            if indicator == "input"{
                                
                                self.defaults.set(doctorCode, forKey: "doctorName")
                                self.defaults.set("Specialization not specified.", forKey: "doctorSpec")
                                
                            }

                            
                            self.requestCheckDouble(batchCode: post["batchCode"].stringValue, requestStatus: 0, providerStatus: post["withProvider"].stringValue)
                            
                            
                        }else{
                            
                            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                            
                            
                        }
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        
                    }
            }
            debugPrint(request)
            
        }else{
            
            
            let requestMaternityParams = ["deviceID": deviceId, "diagnosiscode": "", "doctorCode": doctorCode, "hospitalCode": hospitalCode, "locationCode": "", "memberCode": memberCode, "procedureAmount": 0, "procedureCode": "", "procedureDesc": problem, "username": username, "primaryComplaint": problem, "diagnosisDesc": ""] as [String : Any]
            
            print("Params: \(requestMaternityParams)")
            
            let request = Alamofire.request(PostRouter.postRequestMaternity(requestMaternityParams as [String: AnyObject]))
                .responseJSON { response in
                    guard response.result.error == nil else {
                        print(response.result.error!)
                        
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                        
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        
                        return
                    }
                    
                    if let value: AnyObject = response.result.value as AnyObject? {
                        let post = JSON(value)
                        print("JSON DATA" + post.description)
                        
                        self.defaults.set("withProvider", forKey: "providerStatus")
                        
                        if post["responseCode"].stringValue == "200"{
                            
                            self.defaults.setValue(post["approvalNo"].stringValue, forKey: "approvalNumber")
                            self.defaults.setValue(problem, forKey: "problemCondition")
                            
                            if indicator == "input"{
                                
                                self.defaults.set(doctorCode, forKey: "doctorName")
                                self.defaults.set("Specialization not specified.", forKey: "doctorSpec")
                                
                            }
                            
                            let goToRequestLoaStatus = self.storyboard!.instantiateViewController(withIdentifier: "RequestLoaStatusViewController") as! RequestLoaStatusViewController
                            goToRequestLoaStatus.requestStatus = 0
                            goToRequestLoaStatus.providerAccountStatus = post["withProvider"].stringValue
                            self.navigationController?.pushViewController(goToRequestLoaStatus, animated: true)
                            
                            
                        }else if post["responseCode"].stringValue == "210"{
                            
                            self.defaults.setValue(post["approvalNo"].stringValue, forKey: "approvalNumber")
                            self.defaults.setValue(problem, forKey: "problemCondition")
                            
                            if indicator == "input"{
                                
                                self.defaults.set(doctorCode, forKey: "doctorName")
                                self.defaults.set("Specialization not specified.", forKey: "doctorSpec")
                                
                            }
                            
                            
                            self.requestCheckDouble(batchCode: post["batchCode"].stringValue, requestStatus: 0, providerStatus: post["withProvider"].stringValue)
                            
                            
                        }else{
                            
                            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                            
                        }
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        
                    }
            }
            debugPrint(request)
            
            
        }
        
        
    }
    
    
    func checkDoubleRequest(batchCode: String!, requestStatus: Int!, providerStatus: String!){
        
        let requestConsultationParams = ["batchCode": batchCode] as [String : Any]
        
        print("Params: \(requestConsultationParams)")
        
        let request = Alamofire.request(PostRouter.postApprovedLoaRequest(requestConsultationParams as [String: AnyObject]))
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    print("JSON DATA" + post.description)
                    
                    
                    if post["responseCode"].stringValue == "200"{
                        
                        let goToRequestLoaStatus = self.storyboard!.instantiateViewController(withIdentifier: "RequestLoaStatusViewController") as! RequestLoaStatusViewController
                        goToRequestLoaStatus.requestStatus = requestStatus
                        goToRequestLoaStatus.providerAccountStatus = providerStatus
                        self.navigationController?.pushViewController(goToRequestLoaStatus, animated: true)
                        
                    }else{
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                        
                    }
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                }
        }
        debugPrint(request)
        
        
    }
    
    
    func addToolBarMedicarNum(textField: UITextField){
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked2), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        
    }
    
    func pickerDoneClicked2(){
        
        let indexPath = IndexPath(row: 0, section: 3)
        let cell = (self.tblRequestDetails.cellForRow(at: indexPath)! as! EnterDoctorTableViewCell)
        
        cell.edtEnterDoctor.resignFirstResponder()
       
    }


    
    @IBAction func showTermsAndConditions(_ sender: Any) {
        
        let goToTermsAndCondition = self.storyboard!.instantiateViewController(withIdentifier: "TermAndConditionViewController") as! TermAndConditionViewController
        self.navigationController?.pushViewController(goToTermsAndCondition, animated: true)
        
        
    }
    
    

    

}
