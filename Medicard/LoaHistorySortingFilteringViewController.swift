//
//  LoaHistorySortingFilteringViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 2/20/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView

class LoaHistorySortingFilteringViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    

    @IBOutlet var tblSortAndFilter: UITableView!
    var finalChosenHospital: String!
    var finalChosenDoctor: String!
    let datePickerViewTo  : UIDatePicker = UIDatePicker()
    let datePickerViewFrom  : UIDatePicker = UIDatePicker()
    var dateIndicator: Int!

    
    var newSortBy: String!
    var newStatus: String!
    var newServiceType: String!
    var newSearch: String!
    
    var nullStringArray: [String] = []
    var nullIntArray: [Int] = []
    
    
    var resetFilterIndicator: Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Sort and Filter"
        
        tblSortAndFilter.separatorStyle = .none
        
        tblSortAndFilter.rowHeight = UITableViewAutomaticDimension
        tblSortAndFilter.estimatedRowHeight = 70;
        
        tblSortAndFilter.contentInset = UIEdgeInsetsMake(25, 0, 0, 0)
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelSortingAndFiltering(_:)))
        
        tblSortAndFilter.reloadData()
        
        resetFilterIndicator = 0
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let chosenHospital = Getter_Setter.chosenHospitalName.joined(separator: ", ")
        finalChosenHospital = chosenHospital
        print("Hosp Name: \(finalChosenHospital)")
        let chosenDoctor = Getter_Setter.chosenDoctorName.joined(separator: ", ")
        finalChosenDoctor = chosenDoctor
        print("Doc Name: \(finalChosenDoctor)")
        
        let indexPath = IndexPath(item: 4, section: 0)
        tblSortAndFilter.reloadRows(at: [indexPath], with: .none)
        
        let indexPath2 = IndexPath(item: 5, section: 0)
        tblSortAndFilter.reloadRows(at: [indexPath2], with: .none)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
         resetFilterIndicator = 0
        
        let fieldsPosition = (textField as AnyObject).convert(CGPoint.zero, to: tblSortAndFilter)
        var indexPath = tblSortAndFilter.indexPathForRow(at: fieldsPosition)!
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        if indexPath.row == 8{
            
            if textField == cell.lblDateTo{
                dateIndicator = 0
                addDatePickerFrom()
                tblSortAndFilter.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(tableViewHeight() - 300))
            }else if textField == cell.lblDateFrom{
                
                if cell.lblDateTo.text == ""{
                     _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPickStartDate, closeButtonTitle:"OK")
                    cell.lblDateTo.resignFirstResponder()
                }else{
                    dateIndicator = 1
                    addDatePickerTo()
                    tblSortAndFilter.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(tableViewHeight() - 300))
                }
                
            }
            
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let fieldsPosition = (textField as AnyObject).convert(CGPoint.zero, to: tblSortAndFilter)
        var indexPath = tblSortAndFilter.indexPathForRow(at: fieldsPosition)!
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        if indexPath.row == 0{
            
            Getter_Setter.chosenSearch = cell.edtInputData.text
            cell.edtInputData.text = Getter_Setter.chosenSearch
            cell.edtInputData.resignFirstResponder()
            
        }
        
        return true
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaFindCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            if Getter_Setter.chosenSearch == nil{
                cell.edtInputData.placeholder = "Find By Hospital/Doctor/Tests..."
            }else{
                cell.edtInputData.text = Getter_Setter.chosenSearch
                cell.edtInputData.textColor = UIColor.black
            }
            

            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.row == 1{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaMultipleSelectionCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            cell.lblHeader.text = "Sort By"
            
            if Getter_Setter.chosenSortBy == ""{
                cell.lblMultipleData.text = Getter_Setter.chosenSortBy
                cell.lblMultipleData.textColor = UIColor.black
            }else{
                cell.lblMultipleData.text = Getter_Setter.chosenSortBy
                cell.lblMultipleData.textColor = UIColor.black
            }
            
            
            
            let tapSort = UITapGestureRecognizer(target: self, action: #selector(LoaHistorySortingFilteringViewController.selectDataToField(recognizer:)))
            cell.lblMultipleData.addGestureRecognizer(tapSort)
            cell.selectionStyle = .none
            
            
            return cell

            
        }else if indexPath.row == 2{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaMultipleSelectionCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            cell.lblHeader.text = "Status"
            
            if Getter_Setter.chosenStatus == "Choose Status..."{
                Getter_Setter.chosenStatus = "Choose Status..."
                cell.lblMultipleData.text = Getter_Setter.chosenStatus
                Getter_Setter.chosenStatus =  cell.lblMultipleData.text
                cell.lblMultipleData.textColor = UIColor.lightGray
                
                
            }else{
                cell.lblMultipleData.text = Getter_Setter.chosenStatus
                cell.lblMultipleData.textColor = UIColor.black
            }
            
            let tapSort = UITapGestureRecognizer(target: self, action: #selector(LoaHistorySortingFilteringViewController.selectDataToField(recognizer:)))
            cell.lblMultipleData.addGestureRecognizer(tapSort)
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.row == 3{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaMultipleSelectionCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            cell.lblHeader.text = "Service Type"
            
            if Getter_Setter.chosenServiceType == "Choose Service Type..."{
                Getter_Setter.chosenServiceType = "Choose Service Type..."
                cell.lblMultipleData.text = Getter_Setter.chosenServiceType
                cell.lblMultipleData.textColor = UIColor.lightGray
            }else{
                cell.lblMultipleData.text = Getter_Setter.chosenServiceType
                cell.lblMultipleData.textColor = UIColor.black
            }
            
            let tapSort = UITapGestureRecognizer(target: self, action: #selector(LoaHistorySortingFilteringViewController.selectDataToField(recognizer:)))
            cell.lblMultipleData.addGestureRecognizer(tapSort)
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.row == 4{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaMultipleSelectionCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            cell.lblHeader.text = "Hospital/Clinic"
            
            cell.selectionStyle = .none
            
            let tapSort = UITapGestureRecognizer(target: self, action: #selector(LoaHistorySortingFilteringViewController.selectDataToField(recognizer:)))
            cell.lblMultipleData.addGestureRecognizer(tapSort)
            
            if Getter_Setter.chosenHospitalName.count == 0{
                
                cell.lblMultipleData.text = Getter_Setter.chooseHospitalClininc
                cell.lblMultipleData.textColor = UIColor.black
                
            }else{
                
                if finalChosenHospital == ""{
                    cell.lblMultipleData.text = ""
                    finalChosenHospital = cell.lblMultipleData.text
                    cell.lblMultipleData.textColor = UIColor.lightGray
                }else{
                    cell.lblMultipleData.text = finalChosenHospital
                    cell.lblMultipleData.textColor = UIColor.black
                }
                
            }
            
            return cell
            
            
        }else if indexPath.row == 5{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaMultipleSelectionCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            cell.lblHeader.text = "Doctor"
            
            if Getter_Setter.chosenDoctorName.count == 0{
                
                cell.lblMultipleData.text = Getter_Setter.chooseDoctor
                cell.lblMultipleData.textColor = UIColor.black
                
            }else{
                
                if finalChosenDoctor == ""{
                    cell.lblMultipleData.text = ""
                    finalChosenHospital = cell.lblMultipleData.text
                    cell.lblMultipleData.textColor = UIColor.lightGray
                }else{
                    cell.lblMultipleData.text = finalChosenDoctor
                    cell.lblMultipleData.textColor = UIColor.black
                }
                
                
                
            }
            
            let tapSort = UITapGestureRecognizer(target: self, action: #selector(LoaHistorySortingFilteringViewController.selectDataToField(recognizer:)))
            cell.lblMultipleData.addGestureRecognizer(tapSort)
            
            cell.selectionStyle = .none
            
            
            return cell
            
            
        }else if indexPath.row == 6{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaMultipleSelectionCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            
            cell.lblHeader.text = "Tests"
            cell.lblMultipleData.text = "Choose Tests..."
            cell.lblMultipleData.textColor = UIColor.lightGray
            
            let tapSort = UITapGestureRecognizer(target: self, action: #selector(LoaHistorySortingFilteringViewController.selectDataToField(recognizer:)))
            cell.lblMultipleData.addGestureRecognizer(tapSort)
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.row == 7{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaMultipleSelectionCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            cell.lblHeader.text = "Diagnosis"
            cell.lblMultipleData.text = "Choose Diagnosis..."
            cell.lblMultipleData.textColor = UIColor.lightGray
            
            let tapSort = UITapGestureRecognizer(target: self, action: #selector(LoaHistorySortingFilteringViewController.selectDataToField(recognizer:)))
            cell.lblMultipleData.addGestureRecognizer(tapSort)
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.row == 8{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "reqDateCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            
            cell.lblHeader.text = "Request Date"
            cell.lblDateTo.text = Getter_Setter.chosenRequestDateTo
            cell.lblDateFrom.text = Getter_Setter.chosenRequestDateFrom
            
            cell.selectionStyle = .none
            
            return cell
            
            
            
        }else{
            
            let cell:LoaFilteringAndSortingTableViewCell! = tblSortAndFilter.dequeueReusableCell(withIdentifier: "loaFinilizedDataCell", for: indexPath) as! LoaFilteringAndSortingTableViewCell
            
            cell.btnResetFilter.addTarget(self, action: #selector(resetFilter(sender:)), for: .touchUpInside)
            cell.btnShowFilter.addTarget(self, action: #selector(showFilter(sender:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            
             return 44
            
        }else if indexPath.row == 9{
            
            return 70
            
        }else{
            
             return UITableViewAutomaticDimension
            
        }
        
       
        
    }
    
    
    @IBAction func selectDataToField(recognizer: UITapGestureRecognizer){
        
        
        resetFilterIndicator = 0
        
        let touch = recognizer.location(in: tblSortAndFilter)
        if let indexPath = tblSortAndFilter.indexPathForRow(at: touch) {
            
            let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
            
            self.view.endEditing(true)
            
            if indexPath.row == 1{
                
                
                let alert = SCLAlertView()
                let icon = UIImage(named:"ic_sort_image.png")
                alert.addButton("Hospital/Clinic Name"){
                    
                    Getter_Setter.chosenSortBy = "Hospital/Clinic Name"
                    cell.lblMultipleData.text = Getter_Setter.chosenSortBy
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Status") {
                    
                    Getter_Setter.chosenSortBy = "Status"
                    cell.lblMultipleData.text = Getter_Setter.chosenSortBy
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Request Date") {
                    
                    Getter_Setter.chosenSortBy = "Request Date"
                    cell.lblMultipleData.text = Getter_Setter.chosenSortBy
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Doctor") {
                    
                    Getter_Setter.chosenSortBy = "Doctor"
                    cell.lblMultipleData.text = Getter_Setter.chosenSortBy
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Service Type") {
                    
                    Getter_Setter.chosenSortBy = "Service Type"
                    cell.lblMultipleData.text = Getter_Setter.chosenSortBy
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }

                
                alert.showCustom("Sort By", subTitle: "", color: UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0), icon: icon!)
                
            }else if indexPath.row == 2{
                
                
                let alert = SCLAlertView()
                let icon = UIImage(named:"ic_sort_image.png")
                
                alert.addButton("Outstanding") {
                    
                    Getter_Setter.chosenStatus = "Outstanding"
                    cell.lblMultipleData.text = Getter_Setter.chosenStatus
                    cell.lblMultipleData.textColor = UIColor.black
                }

                
                alert.addButton("Approved") {
                    
                    Getter_Setter.chosenStatus = "Approved"
                    cell.lblMultipleData.text = Getter_Setter.chosenStatus
                    cell.lblMultipleData.textColor = UIColor.black
                }
                
                alert.addButton("Pending") {
                    
                    Getter_Setter.chosenStatus = "Pending"
                    cell.lblMultipleData.text = Getter_Setter.chosenStatus
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Expired") {
                    
                    Getter_Setter.chosenStatus = "Expired"
                    cell.lblMultipleData.text = Getter_Setter.chosenStatus
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Availed") {
                    
                    Getter_Setter.chosenStatus = "Availed"
                    cell.lblMultipleData.text = Getter_Setter.chosenStatus
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Cancelled") {
                    
                    Getter_Setter.chosenStatus = "Cancelled"
                    cell.lblMultipleData.text = Getter_Setter.chosenStatus
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Disapproved") {
                    
                    Getter_Setter.chosenStatus = "Disapproved"
                    cell.lblMultipleData.text = Getter_Setter.chosenStatus
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                
                alert.showCustom("Status", subTitle: "", color: UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0), icon: icon!)
                
                
            }else if indexPath.row == 3{
                
                
                let alert = SCLAlertView()
                let icon = UIImage(named:"ic_sort_image.png")
                
                alert.addButton("Consultation") {
                    
                    Getter_Setter.chosenServiceType = "Consultation"
                    cell.lblMultipleData.text = Getter_Setter.chosenServiceType
                    cell.lblMultipleData.textColor = UIColor.black
                }
                
                alert.addButton("Maternity Consultation") {
                    
                    Getter_Setter.chosenServiceType = "Maternity Consultation"
                    cell.lblMultipleData.text = Getter_Setter.chosenServiceType
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Basic Tests") {
                    
                    Getter_Setter.chosenServiceType = "Basic Tests"
                    cell.lblMultipleData.text = Getter_Setter.chosenServiceType
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                alert.addButton("Other Tests/Procedures") {
                    
                    Getter_Setter.chosenServiceType = "Other Tests/Procedures"
                    cell.lblMultipleData.text = Getter_Setter.chosenServiceType
                    cell.lblMultipleData.textColor = UIColor.black
                    
                }
                
                
                alert.showCustom("Service Type", subTitle: "", color: UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0), icon: icon!)
                
                
            }else if indexPath.row == 4{
                
                 //cell2.edtInputData.resignFirstResponder()
                //Getter_Setter.chosenSearch = cell2.edtInputData.text
                
                let goToSortFilterLoa = self.storyboard!.instantiateViewController(withIdentifier: "SortFilterLoaHistoryViewController") as! SortFilterLoaHistoryViewController
                goToSortFilterLoa.filterTypes = "hospital"
                self.navigationController?.pushViewController(goToSortFilterLoa, animated: true)
                
                
            }else if indexPath.row == 5{
                
                 //cell2.edtInputData.resignFirstResponder()
                //Getter_Setter.chosenSearch = cell2.edtInputData.text
                
                let goToSortFilterLoa = self.storyboard!.instantiateViewController(withIdentifier: "SortFilterLoaHistoryViewController") as! SortFilterLoaHistoryViewController
                goToSortFilterLoa.filterTypes = "doctor"
                self.navigationController?.pushViewController(goToSortFilterLoa, animated: true)
                
            }else if indexPath.row == 6{
                
                // cell2.edtInputData.resignFirstResponder()
                //Getter_Setter.chosenSearch = cell2.edtInputData.text
                
                 _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningNoTestList, closeButtonTitle:"OK")
                
//                let goToSortFilterLoa = self.storyboard!.instantiateViewController(withIdentifier: "SortFilterLoaHistoryViewController") as! SortFilterLoaHistoryViewController
//                goToSortFilterLoa.filterTypes = "tests"
//                self.navigationController?.pushViewController(goToSortFilterLoa, animated: true)
                
                
            }else if indexPath.row == 7{
                
                 //cell2.edtInputData.resignFirstResponder()
                //Getter_Setter.chosenSearch = cell2.edtInputData.text
                
                 _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningNoDiagnosisList, closeButtonTitle:"OK")
                
//                let goToSortFilterLoa = self.storyboard!.instantiateViewController(withIdentifier: "SortFilterLoaHistoryViewController") as! SortFilterLoaHistoryViewController
//                goToSortFilterLoa.filterTypes = "diagnosis"
//                self.navigationController?.pushViewController(goToSortFilterLoa, animated: true)
                
            }
            
        }
        
        
    }

    
    func cancelSortingAndFiltering(_ sender: AnyObject){
        
        //Doctor
        Getter_Setter.finalChosenDoctorName = Getter_Setter.finalChosenDoctorName
        Getter_Setter.finalChosenDoctorIndexes = Getter_Setter.finalChosenDoctorIndexes
        
        //Hospital
        Getter_Setter.finalChosenHospitalName = Getter_Setter.finalChosenHospitalName
        Getter_Setter.finalChosenHospitalIndexes = Getter_Setter.finalChosenHospitalIndexes
        
        Getter_Setter.finalChosenSortBy = Getter_Setter.finalChosenSortBy
        Getter_Setter.finalChosenStatus = Getter_Setter.finalChosenStatus
        Getter_Setter.finalChosenServiceType = Getter_Setter.finalChosenServiceType
        Getter_Setter.finalChosenSearch = Getter_Setter.finalChosenSearch
        Getter_Setter.finalChosenRequestDateFrom = Getter_Setter.finalChosenRequestDateFrom
        Getter_Setter.finalChosenRequestDateTo = Getter_Setter.finalChosenRequestDateTo
        
        Getter_Setter.finalDateTo = Getter_Setter.finalDateTo
        Getter_Setter.finalDateFrom = Getter_Setter.finalDateFrom
        
        
        Getter_Setter.sortingAndFilterIndicator = 1

        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func resetFilter(sender: UIButton){
        
        resetFilterIndicator = 1
        
        resetFilter()

        
        
        
    }
    
    @IBAction func showFilter(sender: UIButton){
        
        if resetFilterIndicator == 1{
            
          resetFilter()
          Getter_Setter.sortingAndFilterIndicator = 0
            
        }else{
            
            let indexPath = IndexPath(row: 8, section: 0)
            let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)

            
            if cell.lblDateFrom.text == "" && cell.lblDateTo.text != ""{
                
                 _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningDateMustNotBeNull, closeButtonTitle:"OK")
                
            }else{
                
                
                //Doctor
                Getter_Setter.finalChosenDoctorName = Getter_Setter.chosenDoctorName
                Getter_Setter.finalChosenDoctorIndexes = Getter_Setter.chosenDoctorIndex
                
                //Hospital
                Getter_Setter.finalChosenHospitalName = Getter_Setter.chosenHospitalName
                Getter_Setter.finalChosenHospitalIndexes = Getter_Setter.chosenHospitalIndex
                
                Getter_Setter.finalChosenSortBy = Getter_Setter.chosenSortBy
                Getter_Setter.finalChosenStatus = Getter_Setter.chosenStatus
                Getter_Setter.finalChosenServiceType = Getter_Setter.chosenServiceType
                Getter_Setter.finalChosenSearch = Getter_Setter.chosenSearch
                Getter_Setter.finalChosenRequestDateFrom = Getter_Setter.chosenRequestDateFrom
                Getter_Setter.finalChosenRequestDateTo = Getter_Setter.chosenRequestDateTo
                
                Getter_Setter.finalDateTo = Getter_Setter.dateTo
                Getter_Setter.finalDateFrom = Getter_Setter.dateFrom
                
                Getter_Setter.sortingAndFilterIndicator = 1
                
                self.dismiss(animated: true, completion: nil)
                
            }
            
           

        }
        
       
        
    }
    
    
    func addDatePickerTo(){
        
        let indexPath = IndexPath(row: 8, section: 0)
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        datePickerViewTo.datePickerMode = UIDatePickerMode.date
        cell.lblDateFrom.inputView = datePickerViewTo
        datePickerViewTo.backgroundColor = UIColor.white
        datePickerViewTo.maximumDate = Date()
        let strDate = cell.lblDateTo.text!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        
        let s = dateFormatter.date(from: strDate)
        
        datePickerViewTo.minimumDate = s
        
 
        datePickerViewTo.addTarget(self, action: #selector(self.datePickerHandlerTo(_:)), for: UIControlEvents.valueChanged)
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.datePickerDoneClick), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        cell.lblDateFrom.inputAccessoryView = toolBar
    }
    
    func addDatePickerFrom(){
        
        let indexPath = IndexPath(row: 8, section: 0)
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        datePickerViewFrom.datePickerMode = UIDatePickerMode.date
        cell.lblDateTo.inputView = datePickerViewFrom
        datePickerViewFrom.backgroundColor = UIColor.white
        datePickerViewFrom.maximumDate = Date()
        
        datePickerViewFrom.addTarget(self, action: #selector(self.datePickerHandlerFrom(_:)), for: UIControlEvents.valueChanged)
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.datePickerDoneClick2), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        cell.lblDateTo.inputAccessoryView = toolBar
        cell.lblDateFrom.text = ""
    }
    
    func datePickerHandlerTo(_ sender: UIDatePicker) {
        
        let indexPath = IndexPath(row: 8, section: 0)
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        
        let dateFormatterCompleteDate = DateFormatter()
        dateFormatterCompleteDate.dateFormat = "MMMM dd, YYYY"
        cell.lblDateFrom.text = dateFormatterCompleteDate.string(from: datePickerViewTo.date)
        Getter_Setter.chosenRequestDateFrom = dateFormatterCompleteDate.string(from: datePickerViewTo.date)
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: datePickerViewTo.date)
        Getter_Setter.dateFrom = tomorrow as NSDate!
        
        print("DATE SELECTED FROM: \(Getter_Setter.dateFrom)")
        
        
        
    }
    
    func datePickerHandlerFrom(_ sender: UIDatePicker) {
        
        let indexPath = IndexPath(row: 8, section: 0)
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        let dateFormatterCompleteDate = DateFormatter()
        dateFormatterCompleteDate.dateFormat = "MMMM dd, YYYY"
        cell.lblDateTo.text = dateFormatterCompleteDate.string(from: datePickerViewFrom.date)
        Getter_Setter.chosenRequestDateTo = dateFormatterCompleteDate.string(from: datePickerViewFrom.date)
        Getter_Setter.dateTo = datePickerViewFrom.date as NSDate!
        
        print("DATE SELECTED TO: \(Getter_Setter.dateTo)")
        
    }
    
    func datePickerDoneClick(){
        
        let indexPath = IndexPath(row: 8, section: 0)
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        let dateFormatterCompleteDate = DateFormatter()
        dateFormatterCompleteDate.dateFormat = "MMMM dd, YYYY"
        cell.lblDateFrom.text = dateFormatterCompleteDate.string(from: datePickerViewTo.date)
        Getter_Setter.chosenRequestDateFrom = dateFormatterCompleteDate.string(from: datePickerViewTo.date)
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: datePickerViewTo.date)
        Getter_Setter.dateFrom = tomorrow as NSDate!
        
        
        
        cell.lblDateTo.resignFirstResponder()
        cell.lblDateFrom.resignFirstResponder()
        
        tblSortAndFilter.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(200))
        
    }
    
    func datePickerDoneClick2(){
        
        let indexPath = IndexPath(row: 8, section: 0)
        let cell = (tblSortAndFilter.cellForRow(at: indexPath)! as! LoaFilteringAndSortingTableViewCell)
        
        let dateFormatterCompleteDate = DateFormatter()
        dateFormatterCompleteDate.dateFormat = "MMMM dd, YYYY"
        cell.lblDateTo.text = dateFormatterCompleteDate.string(from: datePickerViewFrom.date)
        cell.lblDateFrom.text = ""
        Getter_Setter.chosenRequestDateTo = dateFormatterCompleteDate.string(from: datePickerViewFrom.date)
        Getter_Setter.dateTo = datePickerViewFrom.date as NSDate!

        
        cell.lblDateTo.resignFirstResponder()
        cell.lblDateFrom.resignFirstResponder()
        
        tblSortAndFilter.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(200))
        
    }

    
    func tableViewHeight() -> CGFloat {
        tblSortAndFilter.layoutIfNeeded()
        
        return tblSortAndFilter.contentSize.height
    }
    
    
    func resetFilter(){
        
        Getter_Setter.chosenSortBy = "Request Date"
        Getter_Setter.chosenStatus = "Choose Status..."
        Getter_Setter.chosenServiceType = "Choose Service Type..."
        Getter_Setter.chosenSearch = ""
        Getter_Setter.chosenRequestDateTo = ""
        Getter_Setter.chosenRequestDateFrom = ""
        
        Getter_Setter.finalChosenSortBy = Getter_Setter.chosenSortBy
        Getter_Setter.finalChosenStatus = Getter_Setter.chosenStatus
        Getter_Setter.finalChosenServiceType = Getter_Setter.chosenServiceType
        Getter_Setter.finalChosenSearch = Getter_Setter.chosenSearch
        Getter_Setter.finalChosenRequestDateFrom = Getter_Setter.chosenRequestDateFrom
        Getter_Setter.finalChosenRequestDateTo = Getter_Setter.chosenRequestDateTo
        Getter_Setter.finalChosenDoctorIndexes = nullIntArray
        Getter_Setter.finalChosenHospitalIndexes = nullIntArray
        Getter_Setter.finalChosenDoctorName = nullStringArray
        Getter_Setter.finalChosenHospitalName = nullStringArray
        finalChosenHospital = ""
        finalChosenDoctor = ""
        Getter_Setter.chosenDoctorName = nullStringArray
        Getter_Setter.chosenHospitalName = nullStringArray
        
         tblSortAndFilter.reloadData()
        
        resetFilterIndicator = 0
        
    }

    
}
