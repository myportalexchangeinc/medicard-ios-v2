//
//  ChoosePlaceCollectionViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/25/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit


class ChoosePlaceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblPlaces: UILabel!
    
}
