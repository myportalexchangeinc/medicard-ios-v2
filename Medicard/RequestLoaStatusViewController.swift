//
//  RequestLoaStatusViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 12/29/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView
import Photos

class RequestLoaStatusViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tblRequestResult: UITableView!
    
    var hospName: String!
    var hospAddress: String!
    var phoneNumbers: String!
    var contactPerson: String!
    var clinicHours: String!
    var province: String!
    var city: String!
    var region: String!
    var docName: String!
    var docSpec: String!
    var doctorRoom: String!
    var doctorSched: String!
    var currentDate: String!
    var currentDatePlus3: String!
    var providerStatus: Int!
    var providerAccountStatus: String!
    var requestStatus: Int!
    var doctorIndicator: Int!
    var specialization: [String] = []
    var specializationCode: [String] = []
    var specializationIndex: [String] = []
    var specializationIndicator: Int!
    var tableViewImage: UIImage!
    var hideLastSection: Int!
    
    
    
    
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let header = defaults.string(forKey: "consultationHeader")! as String;
        
        if header == "consultation" {
            self.title = "Consultation"
        }else{
            self.title = "Maternity Consultation"
        }
        
        
        if CustomPhotoAlbum.sharedInstance.fetchAssetCollectionForAlbum() == nil {
            
            CustomPhotoAlbum.sharedInstance.createAlbum()
            
        }
        
    
        tblRequestResult.rowHeight = UITableViewAutomaticDimension
        tblRequestResult.estimatedRowHeight = 470;
        
        currentDate = defaults.value(forKey: "dateApprovedNewFormat") as! String!
        currentDatePlus3 = defaults.value(forKey: "dateExpiration") as! String!
    
        
        let indicator = self.defaults.value(forKey: "tableIndicator") as! String
        
        if indicator == "choose"{
            
            doctorIndicator = 0
            
        }else{
            
            doctorIndicator = 1
        }

        
        if providerAccountStatus == "withProvider"{
            providerStatus = 0
        }else{
            providerStatus = 1
        }

        
        hospName = Getter_Setter.selectedHospitalName
        hospAddress = Getter_Setter.selectedAddress
        phoneNumbers = Getter_Setter.selectedphoneNo
        contactPerson = Getter_Setter.seletedContactPerson
        clinicHours = Getter_Setter.selectedDoctorSched
        city = Getter_Setter.selectedCity
        province = Getter_Setter.selectedProvince
        region = Getter_Setter.selectedRegion
        doctorRoom = defaults.value(forKey: "room") as! String!
        doctorSched = defaults.value(forKey: "schedule") as! String!
        docName = defaults.value(forKey: "doctorName") as! String!
        docSpec = defaults.value(forKey: "doctorSpec") as! String!
        doctorRoom = defaults.value(forKey: "room") as! String!
        doctorSched = defaults.value(forKey: "schedule") as! String!
        
        print("Doc Spec: \(docSpec)")
        
        
        if docSpec == "DERMATOLOGY" || docSpec == "DENTISTRY"{
            specializationIndicator = 0
        }else{
            specializationIndicator = 1
        }
        
        hideLastSection = 0
        
        
         tblRequestResult.reloadData()
        
        
        self.navigationItem.hidesBackButton = true;
        
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            
            return 1
            
        }else if section == 1{
        
            return 1
            
        }else if section == 2{
            
            if specializationIndicator == 0{
                
                return 1
                
            }else{
                
                return 0
                
            }
            
        }else if section == 3{
            
            return 1
            
        }else if section == 4{
            
            return 1
            
        }else if section == 5{
            
            if doctorIndicator == 0{
                
                return 1
                
            }else{
                
                return 0
                
            }
            
        }else if section == 6{
            
            if doctorIndicator == 0{
                
                return 0
                
            }else{
                
                return 1
                
            }
            
            
        }else if section == 7{
            
            if providerStatus == 0{
                
                return 0
                
            }else{
                
                return 1
            }
            
        }else if section == 8{
            
            return 1
            
        }else{
            
            if hideLastSection == 0{
                return 1
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cellIdentifier = "Cell"
            let cell: UITableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: cellIdentifier)
            
            
            cell.textLabel?.text = "REQUEST SUBMITTED"
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            cell.textLabel?.textAlignment = .center
            
            cell.separatorInset = UIEdgeInsetsMake(0, -1000, 0, 0)
            
            return cell
            
        }else if indexPath.section == 1{
            
             let cell:QRCodeTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "qrCell", for: indexPath) as! QRCodeTableViewCell
            
            
            GenerateQRCode.generateQR(qrImageView: cell.imgReferenceQRCode, memberID: (defaults.value(forKey: "approvalNumber") as! String?)!)
            cell.lblDueDate.text = "This form is valid from " + currentDate + " to " + currentDatePlus3 + "."
            cell.lblReferenceNumber.text = defaults.value(forKey: "approvalNumber") as! String?
            
            let header = defaults.string(forKey: "consultationHeader")! as String;
            
            if header == "consultation" {
                cell.lblLoaType.text = "(CONSULTATION)"
            }else{
                cell.lblLoaType.text = "(MATERNITY CONSULTATION)"
            }

            
            cell.selectionStyle = .none

            return cell
            
            
        }else if indexPath.section == 2{
            
            
            let cell:WarningTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "cellWarningSpec", for: indexPath) as! WarningTableViewCell
            
            
            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.section == 3{
            
            let cell:RequestLoaResultFirstTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "firstCell", for: indexPath) as! RequestLoaResultFirstTableViewCell
            
            cell.lblMemberName.text = defaults.value(forKey: "memberName") as! String?
            cell.lblMemberCode.text = defaults.value(forKey: "memberCode") as! String?
            cell.lblAge.text = defaults.value(forKey: "memberAge") as! String?
            
            if defaults.value(forKey: "memberGender") as! String? == "0" {
                
                cell.lblGender.text = "FEMALE"
                
            }else{
                
                cell.lblGender.text = "FEMALE"
                
            }
            
            cell.lblCompanyName.text = defaults.value(forKey: "companyName") as! String?
            cell.lblRemarks.text = defaults.value(forKey: "remarks") as! String?
            cell.lblDateApproved.text = defaults.value(forKey: "dateApproved") as! String?
            cell.lblValidityDate.text = defaults.value(forKey: "valDate") as! String?
            cell.lblEffectivityDate.text = defaults.value(forKey: "effDate") as! String?
            

            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.section == 4{
            
            let cell:HospitalRequestDetailsTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "HospitalCell", for: indexPath) as! HospitalRequestDetailsTableViewCell
            
            
            cell.lblHospitalName.text = hospName
            cell.lblHospitalAddress.text = hospAddress + ", " + city + ", " + province + ", " + region
            cell.lblContactPerson.text = contactPerson
            cell.lblPhoneNumbers.text = phoneNumbers
            cell.selectionStyle = .none
            
            return cell

            
        }else if indexPath.section == 5{
            
            
            let cell:DoctorRequestDetailsTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "DoctorCell", for: indexPath) as! DoctorRequestDetailsTableViewCell
            
            cell.lblDoctorName.text = docName
            cell.lblDoctorSkill.text = docSpec
            cell.lblRoom.text = doctorRoom
            cell.lblClinicHours.text = doctorSched
            
            cell.separatorInset = UIEdgeInsetsMake(0, tblRequestResult.frame.size.width, 0, 0)
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.section == 6{
            
              let cell:EnterDoctorResultTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "enterDoctorResultCell", for: indexPath) as! EnterDoctorResultTableViewCell
            
            cell.lblDoctorName.text = docName
            cell.lblDoctorSpec.text = docSpec
            
            cell.separatorInset = UIEdgeInsetsMake(0, tblRequestResult.frame.size.width, 0, 0)
            
            return cell
            
            
        }else if indexPath.section == 7{
            
            let cell:WarningTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "cellWarning", for: indexPath) as! WarningTableViewCell
            
            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.section == 8{
            
            
            let cell:ProblemConditionTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "fourthCell", for: indexPath) as! ProblemConditionTableViewCell
            
            cell.lblProblemContent.text = defaults.value(forKey: "problemCondition") as! String?
            
            cell.lblProblemContent.sizeToFit()
            
            cell.selectionStyle = .none
            
            return cell
            
        }else{
            

            let cell:OkayButtonTableViewCell! = tblRequestResult.dequeueReusableCell(withIdentifier: "fifthCell", for: indexPath) as! OkayButtonTableViewCell
            
            cell.btnOkay.addTarget(self, action: #selector(self.okayAction(_:)), for: .touchUpInside)
            cell.btnDownloadLoaForm.addTarget(self, action: #selector(self.downloadForm(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none

            return cell
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    

    @IBAction func okayAction(_ sender: Any) {
        
        defaults.set("", forKey: "chosenProvinceCode")
        defaults.set("Hospital/Clinic Name", forKey: "filterSortBy")
        defaults.set("All Provinces", forKey: "filterProvince")
        defaults.set("All Cities", forKey: "filterCity")
        
        
        //SPECIALIZATION ---------------------------------------------->

        defaults.set(specialization, forKey: "chosenSpecializationName")
        defaults.set(specializationCode, forKey: "chosenSpecializationCode")
        defaults.set(specializationIndex, forKey: "chosenSpecializationIndexes")
        defaults.set("Doctor's Family Name", forKey: "sortByIndicator")
        defaults.set("", forKey: "searchDoctorsName")
        defaults.set("", forKey: "doctorsRoom")
        defaults.set("Doctor's Family Name", forKey: "sortByIndicator")
        defaults.set("", forKey: "searchDoctorsName")
        defaults.set("", forKey: "doctorsRoom")
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func downloadForm(_ sender: Any){
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            
            let alertController = UIAlertController(
                title: "",
                message: "Allow Medicard to access your Photos in able to download this form. Thank you.",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Settings", style: .default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }else{
            

            let delayInSeconds = 3.0
            let delaySavingImage = 1.5
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Creating Album..."
            loadingNotification.isUserInteractionEnabled = false;
            tblRequestResult.isUserInteractionEnabled = false

            if CustomPhotoAlbum.sharedInstance.fetchAssetCollectionForAlbum() == nil {
                
                CustomPhotoAlbum.sharedInstance.createAlbum()
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {

                    self.hideLastSection = 1
                    self.tblRequestResult.reloadData()
                    
                    loadingNotification.label.text = "Saving LoA..."
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delaySavingImage) {
                        
                        self.tableViewImage = self.tblRequestResult.screenshot
                        CustomPhotoAlbum.sharedInstance.save(image: self.tableViewImage)
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tblRequestResult.isUserInteractionEnabled = true
                        
                        self.hideLastSection = 0
                        self.tblRequestResult.reloadData()
                        
                        _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.successDownloadedForm, closeButtonTitle:"OK")
                        
                    }
                    
                }
                
                
            }else{
                
                self.hideLastSection = 1
                self.tblRequestResult.reloadData()
                
                loadingNotification.label.text = "Saving LoA..."
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delaySavingImage) {
                    
                    
                    self.tableViewImage = self.tblRequestResult.screenshot
                    CustomPhotoAlbum.sharedInstance.save(image: self.tableViewImage)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblRequestResult.isUserInteractionEnabled = true
                    
                    self.hideLastSection = 0
                    self.tblRequestResult.reloadData()
                    
                    _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.successDownloadedForm, closeButtonTitle:"OK")
                    
                }
                

            }
            
            
        }
        
    }
    
    func tableViewHeight() -> CGFloat {
        tblRequestResult.reloadData()
        tblRequestResult.layoutIfNeeded()
        
        return tblRequestResult.contentSize.height
    }
    

}
