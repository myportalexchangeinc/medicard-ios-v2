//
//  ChoosePlaceViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 1/25/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import CoreData

class ChoosePlaceViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate {
    
    @IBOutlet var collectionPlace: UICollectionView!
    
    var places = ["Pangasinan", "La Union", "Ilocos Sur", "Ilocos Norte", "Baguio", "Cordellera", "Abra", "Bataan", "Cagayan Valley"]
    
    var locationStatus: String!
    var subjectInformation = [NSManagedObject]()
    let defaults = UserDefaults.standard
    var chosenProvince: String!
    var chosenCity: String!
    var chosenCities: [String] = []
    var chosenIndex: [Int] = []
    var cellStatus:NSMutableDictionary = NSMutableDictionary();
    var nullChosenIndex: [String] = []
    var nullChosenCity: [String] = []
    var chosenProvinceNameArray: [String] = []
    var chosenProvinceCodeArray: [String] = []
    

    
    var searchActive : Bool = false

    @IBOutlet var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        chosenProvince = (defaults.value(forKey: "chosenProvince")) as! String!

        
        if locationStatus == "province"{
            
            self.title = "Provinces"
            
            fetchProvinces()
            
            
        }else{
            
            self.title = "City"
            
            
            fetchCities()
            
            
            let backButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(savePlaces(_:)))
            
            self.navigationItem.rightBarButtonItem = backButton
            
        }
        
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPlace(_:)))
        
      
        
        collectionPlace.allowsMultipleSelection = true
        
        addToolBarMedicarNum()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

          navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
         if locationStatus == "city"{
            
            chosenIndex = defaults.value(forKey: "chosenIndex") as? [Int] ?? [Int]()
            chosenCities = defaults.value(forKey: "chosenCity") as? [String] ?? [String]()
            
            print("Array City: \(chosenCities)")
            print("Array Indexes \(chosenIndex)")
            
            searchBar.placeholder = "Search City..."
            
            
         }else{
            
            searchBar.placeholder = "Search Province..."
            
        }
        
        
    }
    
    func cancelPlace(_ _sender: AnyObject){
        
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("SEARCH TEXT: \(searchBar.text)")
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        var fetchRequest = NSFetchRequest<NSManagedObject>()
        var fetchRequest2 = NSFetchRequest<NSManagedObject>()
        
         let managedContext = appDelegate.managedObjectContext
         let managedContext2 = appDelegate.managedObjectContext
        
        if locationStatus == "city"{
            
            fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
            fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
            
            let predicate1 = NSPredicate(format: "(cityName BEGINSWITH [c] %@)", searchBar.text!)
            let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
            fetchRequest.predicate = motherPredicate
            
            let predicate2 = NSPredicate(format: "(cityName CONTAINS [c] %@)", " " + searchBar.text!)
            let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
            fetchRequest2.predicate = motherPredicate2
            
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest2.returnsObjectsAsFaults = false //needed to make data accessible

            
        }else{

            fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ProvinceEntity")
            fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "ProvinceEntity")
            
            let predicate1 = NSPredicate(format: "(provinceName BEGINSWITH [c] %@)", searchBar.text!)
            let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
            fetchRequest.predicate = motherPredicate
            
            let predicate2 = NSPredicate(format: "(provinceName CONTAINS [c] %@)", " " + searchBar.text!)
            let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
            fetchRequest2.predicate = motherPredicate2
            
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest2.returnsObjectsAsFaults = false //needed to make data accessible

            
        }
        
        
        do {
            
            
            if locationStatus == "city"{
                
                let results = try managedContext.fetch(fetchRequest)
                let results2 = try managedContext2.fetch(fetchRequest2)
                
                
                if searchBar.text == ""{
                    
                    fetchCities()
                    
                    collectionPlace.isHidden = false
                    
                }else if results.count == 0{
                    
                    collectionPlace.isHidden = true
                    
                }else{
                    subjectInformation = results + results2
                    collectionPlace.isHidden = false
                }

                
                
            }else{
                
                
                let results = try managedContext.fetch(fetchRequest)
                let results2 = try managedContext2.fetch(fetchRequest2)
                
                
                if searchBar.text == ""{
                    
                    fetchProvinces()
                    
                    collectionPlace.isHidden = false
                    
                }else if results.count == 0{
                    
                    collectionPlace.isHidden = true
                    
                }else{
                    subjectInformation = results + results2
                    collectionPlace.isHidden = false
                }

                
            }
            
            
            
            
        } catch {
            
            print("uh oh")
            
        }
        
        collectionPlace.reloadData()
        
    }
    
    func savePlaces(_ _sender: AnyObject){
        
        defaults.set(chosenIndex, forKey: "chosenIndex")
        
        if chosenCities.count == 0{
            
            chosenCities.append("All Cities")
            defaults.set(chosenCities, forKey: "filterCity")
            defaults.set(chosenCities, forKey: "chosenCity")
            
        }else{
            
            
            defaults.set(chosenCities, forKey: "filterCity")
            defaults.set(chosenCities, forKey: "chosenCity")
            
            if let index = chosenCities.index(of: "All Cities") {
                chosenCities.remove(at: index)
                
                defaults.set(chosenCities, forKey: "filterCity")
                defaults.set(chosenCities, forKey: "chosenCity")
                
                
                print("REMAINING ITEMS: \(chosenCities)")
            }
            
        }
        
       
        defaults.set("fromSortFilter", forKey: "retainFromHospital")
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return subjectInformation.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "choosePlaceCell", for: indexPath as IndexPath) as! ChoosePlaceCollectionViewCell
        
        if locationStatus == "province"{
            
            let provinces = subjectInformation[(indexPath as NSIndexPath).row]
            let provinceName = provinces.value(forKey: "provinceName") as! String
            let trimProvinceName = provinceName.trimmingCharacters(in:.whitespaces)
            
            cell.lblPlaces.text = trimProvinceName
            
            
            if self.chosenProvinceNameArray.count != 0{
                
                for i in 0..<chosenProvinceNameArray.count{
                    
                    if trimProvinceName == chosenCities[i]{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                        cell.lblPlaces.textColor = UIColor.white
                        
                        break
                        
                    }else{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                        cell.lblPlaces.backgroundColor = UIColor.clear
                        cell.lblPlaces.textColor = UIColor.black
                        
                    }
                    
                }
                
            }
            
        }else{
         
            let cities = subjectInformation[(indexPath as NSIndexPath).row]
            let cityName = cities.value(forKey: "cityName") as! String
            let trimCityName = cityName.trimmingCharacters(in:.whitespaces)
        
            
             cell.lblPlaces.text = trimCityName
            
            cell.lblPlaces.layer.borderWidth = 1
            cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
            cell.lblPlaces.layer.cornerRadius = 2
            cell.lblPlaces.clipsToBounds = true
            cell.lblPlaces.textColor = UIColor.black
            
            
            if self.chosenCities.count != 0{
                
                for i in 0..<chosenCities.count{
                    
                    if trimCityName == chosenCities[i]{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                        cell.lblPlaces.textColor = UIColor.white
                        
                        break
                        
                    }else{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                         cell.lblPlaces.backgroundColor = UIColor.clear
                        cell.lblPlaces.textColor = UIColor.black

                    }
                    
                }
                
            }
            
        }
        
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         defaults.set("fromSortFilter", forKey: "retainFromHospital")
        
         if locationStatus == "province"{
            
            let cell = collectionView.cellForItem(at: indexPath) as! ChoosePlaceCollectionViewCell
            
            
            let provinces = subjectInformation[(indexPath as NSIndexPath).row]
            let provinceName = provinces.value(forKey: "provinceName") as! String
            let provinceCode = provinces.value(forKey: "provinceCode") as! String
            let trimProvince = provinceName.trimmingCharacters(in: .whitespaces)
            
            
            if cell.lblPlaces.textColor == UIColor.white {
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.black
                cell.lblPlaces.backgroundColor = UIColor.clear
                
                if let index = chosenProvinceNameArray.index(of: trimProvince){
                    
                    chosenProvinceNameArray.remove(at: index)
                    
                }
                
                if let index2 = chosenProvinceNameArray.index(of: provinceCode){
                    
                    chosenProvinceNameArray.remove(at: index2)
                    
                }
                
                
                
            }else{
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.white
                
                chosenProvinceNameArray.append(cell.lblPlaces.text!)
                chosenProvinceCodeArray.append(provinceCode)
            
                
            }
            
            defaults.set("fromProvince", forKey: "chosenIndicator")
            defaults.set(nullChosenCity, forKey: "chosenCity")
            defaults.set(nullChosenIndex, forKey: "chosenIndex")
            
            
         }else{
            
            let cell = collectionView.cellForItem(at: indexPath) as! ChoosePlaceCollectionViewCell

            if cell.lblPlaces.textColor == UIColor.white{
                
                let cities = subjectInformation[(indexPath as NSIndexPath).row]
                let cityName = cities.value(forKey: "cityName") as! String
                let trimCity = cityName.trimmingCharacters(in: .whitespaces)
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.black
                cell.lblPlaces.backgroundColor = UIColor.clear
                
                if let index = chosenCities.index(of: trimCity) {
                    chosenCities.remove(at: index)
                    
                    
                    print("REMAINING ITEMS: \(chosenCities)")
                }
                
                if let index2 = chosenIndex.index(of: indexPath.row) {
                    chosenIndex.remove(at: index2)
                    
                    
                    print("REMAINING ITEMS: \(chosenCities)")
                }

                
                
            }else{
                
                let cities = subjectInformation[(indexPath as NSIndexPath).row]
                let cityName = cities.value(forKey: "cityName") as! String
                let trimCity = cityName.trimmingCharacters(in: .whitespaces)
                
                defaults.set(cityName, forKey: "chosenCity")
                defaults.set("fromCity", forKey: "chosenIndicator")
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.white
                
                chosenCities.append(trimCity)
                chosenIndex.append(indexPath.row)
                

                
                
                
            }
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
         defaults.set("fromSortFilter", forKey: "retainFromHospital")
        
         if locationStatus == "province"{
            
         }else{
            
            let cell = collectionView.cellForItem(at: indexPath) as! ChoosePlaceCollectionViewCell
            cell.isSelected = false;
            self.cellStatus[indexPath.row] = false;
            
            let cities = subjectInformation[(indexPath as NSIndexPath).row]
            let cityName = cities.value(forKey: "cityName") as! String
            let trimCity = cityName.trimmingCharacters(in: .whitespaces)
            
            if let index = chosenCities.index(of: trimCity) {
                chosenCities.remove(at: index)
                
                
                print("REMAINING ITEMS: \(chosenCities)")
            }
            
            if let index2 = chosenIndex.index(of: indexPath.row) {
                chosenIndex.remove(at: index2)
                
                
                print("REMAINING ITEMS: \(chosenCities)")
            }

        }
        
    }


    
    
    func fetchCities(){
        
        if chosenProvince == "All Provinces"{
            
            
            let appDelegate =
                UIApplication.shared.delegate as! AppDelegate
            
            let managedContext = appDelegate.managedObjectContext
            
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
            let sortDescriptor = NSSortDescriptor(key: "cityName", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptor]
            
            do {
                let results = try managedContext.fetch(fetchRequest)
                subjectInformation = results
                
                
                collectionPlace.reloadData()
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
            

        }else{
            
           
           let appDelegate =
               UIApplication.shared.delegate as! AppDelegate
           
           let managedContext = appDelegate.managedObjectContext
           
           let chosenProvinceCode = defaults.value(forKey: "chosenProvinceCode") as! String!
           let predicate = NSPredicate(format: "provinceCode == %@", chosenProvinceCode!)
           let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
           
           fetchRequest.predicate = predicate
           
           do {
               let results = try managedContext.fetch(fetchRequest)
               subjectInformation = results
               
               
               collectionPlace.reloadData()
               
               MBProgressHUD.hide(for: self.view, animated: true)
               
           } catch let error as NSError {
               print("Could not fetch. \(error), \(error.userInfo)")
           }

            
        }
        

    }
    
    
    
    func fetchProvinces(){
        
        
       let appDelegate =
           UIApplication.shared.delegate as! AppDelegate
       
       let managedContext = appDelegate.managedObjectContext
       
       let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ProvinceEntity")
       let sortDescriptor = NSSortDescriptor(key: "provinceName", ascending: true)
       fetchRequest.sortDescriptors = [sortDescriptor]
       
       do {
           let results = try managedContext.fetch(fetchRequest)
           subjectInformation = results
           
           
           collectionPlace.reloadData()
           
           MBProgressHUD.hide(for: self.view, animated: true)
           
       } catch let error as NSError {
           print("Could not fetch. \(error), \(error.userInfo)")
       }


    }
    
    func addToolBarMedicarNum(){
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        searchBar.inputAccessoryView = toolBar
        
        
    }
    
    func pickerDoneClicked(){
        
        searchBar.resignFirstResponder()
        
    }
    
    
}
