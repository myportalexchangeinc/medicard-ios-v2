//
//  DoctorRequestDetailsTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 12/28/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit

class DoctorRequestDetailsTableViewCell: UITableViewCell {
    
    
    @IBOutlet var lblDoctorName: UILabel!
    @IBOutlet var lblDoctorSkill: UILabel!
    @IBOutlet var lblDoctosMobileNumber: UILabel!
    @IBOutlet var lblClinicHours: UILabel!
    @IBOutlet var lblRoom: UILabel!
    @IBOutlet var lblAlertSelectDoctor: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
