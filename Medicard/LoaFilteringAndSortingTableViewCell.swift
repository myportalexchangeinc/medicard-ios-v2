//
//  LoaFilteringAndSortingTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 2/20/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class LoaFilteringAndSortingTableViewCell: UITableViewCell {
    
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var edtInputData: UITextField!
    @IBOutlet var lblMultipleData: UILabel!
    @IBOutlet var btnResetFilter: UIButton!
    @IBOutlet var btnShowFilter: UIButton!
    @IBOutlet var lblDateTo: UITextField!
    @IBOutlet var lblDateFrom: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        
        
    }
    
    

}
