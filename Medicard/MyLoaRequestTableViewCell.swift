//
//  MyLoaRequestTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/18/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class MyLoaRequestTableViewCell: UITableViewCell {
    
    @IBOutlet var lblHospitalName: UILabel!
    @IBOutlet var lblRequestDate: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblDoctorName: UILabel!
    @IBOutlet var lblDoctorSpec: UILabel!
    @IBOutlet var lblRoom: UILabel!
    @IBOutlet var lblSchedule: UILabel!
    @IBOutlet var lblLoaType: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
