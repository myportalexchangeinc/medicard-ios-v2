//
//  ChangeOldPinTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/17/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class ChangeOldPinTableViewCell: UITableViewCell {
    
    @IBOutlet var edtOldPin: UITextField!
    @IBOutlet var edtNewPin: UITextField!
    @IBOutlet var edtReTypePin: UITextField!
    @IBOutlet var btnChangePin: UIButton!
    @IBOutlet var lblCurrentPin: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
