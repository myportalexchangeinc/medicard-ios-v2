//
//  NewChoosePlaceViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 3/22/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import CoreData

class NewChoosePlaceViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate  {
    
    @IBOutlet var collectionPlace: UICollectionView!
    
    var locationStatus: String!
    var subjectInformation = [NSManagedObject]()
    let defaults = UserDefaults.standard
    var chosenProvince: String!
    var chosenCity: String!
    var chosenCities: [String] = []
    var chosenIndex: [Int] = []
    var cellStatus:NSMutableDictionary = NSMutableDictionary();
    var nullChosenIndex: [String] = []
    var nullChosenCity: [String] = []
    
    var chosenProvinceNameArray: [String] = []
    var chosenProvinceCodeArray: [String] = []
    
    var arraysOfCityName: [[String]] = []
    var arraysOfCityCode: [[String]] = []
    
    var chosenArrayOfCity = NSMutableArray()
    
    var searchActive: Bool = true
    
    @IBOutlet var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        chosenCities = Getter_Setter.chosenCityName
        chosenProvinceNameArray = Getter_Setter.chosenProvinceNameArray
        chosenProvinceCodeArray = Getter_Setter.chosenProvinceCodeArray
        
        print(" \(chosenProvinceCodeArray)")
        
        if locationStatus == "province"{
            
            self.title = "Provinces"
            
            fetchProvinces()
            searchBar.placeholder = "Search Province..."
            
            let backButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(savePlaces(_:)))
            self.navigationItem.rightBarButtonItem = backButton
            
            
        }else{
            
            self.title = "City"
            
            searchBar.placeholder = "Search City..."
            fetchCities()
            
            
            let backButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(savePlaces(_:)))
            self.navigationItem.rightBarButtonItem = backButton
            
        }
        

        
        
    }
    
    func fetchCities(){
        
        if chosenProvinceNameArray.count == 0{
            
            let appDelegate =
                UIApplication.shared.delegate as! AppDelegate
            
            let managedContext = appDelegate.managedObjectContext
            
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
            let sortDescriptor = NSSortDescriptor(key: "cityName", ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptor]
            
            do {
                let results = try managedContext.fetch(fetchRequest)
                subjectInformation = results
                
                
                collectionPlace.reloadData()
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
            
            
        }else{
            
            
            let appDelegate =
                UIApplication.shared.delegate as! AppDelegate
            
            print("HERE: \(Getter_Setter.finalChosenProvinceCodeArray)")
            
            let managedContext = appDelegate.managedObjectContext
            let predicate = NSPredicate(format: "provinceCode IN [cd] %@", chosenProvinceCodeArray)
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
            
            fetchRequest.predicate = predicate
            
            do {
                let results = try managedContext.fetch(fetchRequest)
                subjectInformation = results
                collectionPlace.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
                
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
            
            
        }
        
        
    }
    
    
    
    func fetchProvinces(){
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ProvinceEntity")
        let sortDescriptor = NSSortDescriptor(key: "provinceName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            subjectInformation = results
            
            
            collectionPlace.reloadData()
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
    
    func savePlaces(_ _sender: AnyObject){
        
        
        if locationStatus == "province"{
            Getter_Setter.chosenProvinceNameArray = chosenProvinceNameArray
            Getter_Setter.chosenProvinceCodeArray = chosenProvinceCodeArray
        }else{
            Getter_Setter.chosenCityName = chosenCities
        }
        
        
        
        self.navigationController?.popViewController(animated: true)
        
    }

    
    func cancelPlace(_ _sender: AnyObject){
        
        
        if locationStatus == "province"{
            Getter_Setter.chosenProvinceNameArray = chosenProvinceNameArray
            Getter_Setter.chosenProvinceCodeArray = chosenProvinceCodeArray
        }else{
            Getter_Setter.chosenCityName = chosenCities
        }
        
        
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("SEARCH TEXT: \(searchBar.text)")
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        var fetchRequest = NSFetchRequest<NSManagedObject>()
        var fetchRequest2 = NSFetchRequest<NSManagedObject>()
        
        let managedContext = appDelegate.managedObjectContext
        let managedContext2 = appDelegate.managedObjectContext
        
        if locationStatus == "city"{
            
            fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
            fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "CityEntity")
            
            let predicate1 = NSPredicate(format: "(cityName BEGINSWITH [c] %@)", searchBar.text!)
            let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
            fetchRequest.predicate = motherPredicate
            
            let predicate2 = NSPredicate(format: "(cityName CONTAINS [c] %@)", " " + searchBar.text!)
            let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
            fetchRequest2.predicate = motherPredicate2
            
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest2.returnsObjectsAsFaults = false //needed to make data accessible
            
            
        }else{
            
            fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ProvinceEntity")
            fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "ProvinceEntity")
            
            let predicate1 = NSPredicate(format: "(provinceName BEGINSWITH [c] %@)", searchBar.text!)
            let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
            fetchRequest.predicate = motherPredicate
            
            let predicate2 = NSPredicate(format: "(provinceName CONTAINS [c] %@)", " " + searchBar.text!)
            let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
            fetchRequest2.predicate = motherPredicate2
            
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest2.returnsObjectsAsFaults = false //needed to make data accessible
            
            
        }
        
        
        do {
            
            
            if locationStatus == "city"{
                
                let results = try managedContext.fetch(fetchRequest)
                let results2 = try managedContext2.fetch(fetchRequest2)
                
                
                if searchBar.text == ""{
                    
                    fetchCities()
                    
                    collectionPlace.isHidden = false
                    
                }else if results.count == 0{
                    
                    collectionPlace.isHidden = true
                    
                }else{
                    subjectInformation = results + results2
                    collectionPlace.isHidden = false
                }
                
                
                
            }else{
                
                
                let results = try managedContext.fetch(fetchRequest)
                let results2 = try managedContext2.fetch(fetchRequest2)
                
                
                if searchBar.text == ""{
                    
                    fetchProvinces()
                    
                    collectionPlace.isHidden = false
                    
                }else if results.count == 0{
                    
                    collectionPlace.isHidden = true
                    
                }else{
                    subjectInformation = results + results2
                    collectionPlace.isHidden = false
                }
                
                
            }
            
            
        } catch {
            
            print("uh oh")
            
        }
        
        collectionPlace.reloadData()
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return subjectInformation.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "choosePlaceCell", for: indexPath as IndexPath) as! ChoosePlaceCollectionViewCell
        
        if locationStatus == "province"{
            
            let provinces = subjectInformation[(indexPath as NSIndexPath).row]
            let provinceName = provinces.value(forKey: "provinceName") as! String
            let trimProvinceName = provinceName.trimmingCharacters(in:.whitespaces)
            
            cell.lblPlaces.text = trimProvinceName
            
            cell.lblPlaces.layer.borderWidth = 1
            cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
            cell.lblPlaces.layer.cornerRadius = 2
            cell.lblPlaces.clipsToBounds = true
            cell.lblPlaces.textColor = UIColor.black
            
            if self.chosenProvinceNameArray.count != 0{
                
                for i in 0..<chosenProvinceNameArray.count{
                    
                    if trimProvinceName == chosenProvinceNameArray[i]{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                        cell.lblPlaces.textColor = UIColor.white
                        
                        break
                        
                    }else{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                        cell.lblPlaces.backgroundColor = UIColor.clear
                        cell.lblPlaces.textColor = UIColor.black
                        
                        
                    }
                    
                }
                
            }

            
        }else{
            
            let cities = subjectInformation[(indexPath as NSIndexPath).row]
            let cityName = cities.value(forKey: "cityName") as! String
            let trimCityName = cityName.trimmingCharacters(in:.whitespaces)
            
            
            cell.lblPlaces.text = trimCityName
            
            cell.lblPlaces.layer.borderWidth = 1
            cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
            cell.lblPlaces.layer.cornerRadius = 2
            cell.lblPlaces.clipsToBounds = true
            cell.lblPlaces.textColor = UIColor.black
            
            
            if self.chosenCities.count != 0{
                
                for i in 0..<chosenCities.count{
                    
                    if trimCityName == chosenCities[i]{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                        cell.lblPlaces.textColor = UIColor.white
                        
                        break
                        
                    }else{
                        
                        cell.lblPlaces.layer.borderWidth = 1
                        cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                        cell.lblPlaces.layer.cornerRadius = 2
                        cell.lblPlaces.clipsToBounds = true
                        cell.lblPlaces.backgroundColor = UIColor.clear
                        cell.lblPlaces.textColor = UIColor.black
                        
                    }
                    
                }
                
            }
            
        }
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        defaults.set("fromSortFilter", forKey: "retainFromHospital")
        
        if locationStatus == "province"{
            
            let cell = collectionView.cellForItem(at: indexPath) as! ChoosePlaceCollectionViewCell
            
            
            let provinces = subjectInformation[(indexPath as NSIndexPath).row]
            let provinceName = provinces.value(forKey: "provinceName") as! String
            let provinceCode = provinces.value(forKey: "provinceCode") as! String
            let trimProvince = provinceName.trimmingCharacters(in: .whitespaces)
            
            
            if cell.lblPlaces.textColor == UIColor.white {
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.black
                cell.lblPlaces.backgroundColor = UIColor.clear
                
                if let index = chosenProvinceNameArray.index(of: trimProvince){
                    
                    chosenProvinceNameArray.remove(at: index)
                    
                }
                
                if let index2 = chosenProvinceNameArray.index(of: provinceCode){
                    
                    chosenProvinceNameArray.remove(at: index2)
                    
                }
                
                
                
            }else{
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.white
                
                chosenProvinceNameArray.append(cell.lblPlaces.text!)
                chosenProvinceCodeArray.append(provinceCode)
                
                print("CHOSEN PROVINCE CODE: \(chosenProvinceCodeArray)")
                print("CHOSEN PROVINCE NAME: \(chosenProvinceNameArray)")
                
            }
            
            defaults.set("fromProvince", forKey: "chosenIndicator")
            defaults.set(nullChosenCity, forKey: "chosenCity")
            defaults.set(nullChosenIndex, forKey: "chosenIndex")
            
        }else{
            
            let cell = collectionView.cellForItem(at: indexPath) as! ChoosePlaceCollectionViewCell
            
            if cell.lblPlaces.textColor == UIColor.white{
                
                let cities = subjectInformation[(indexPath as NSIndexPath).row]
                let cityName = cities.value(forKey: "cityName") as! String
                let trimCity = cityName.trimmingCharacters(in: .whitespaces)
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.black
                cell.lblPlaces.backgroundColor = UIColor.clear
                
                if let index = chosenCities.index(of: trimCity) {
                    chosenCities.remove(at: index)

                    
                    
                    print("REMAINING ITEMS: \(chosenArrayOfCity)")
                }
                
                if let index2 = chosenIndex.index(of: indexPath.row) {
                    chosenIndex.remove(at: index2)
                    
                    
                    print("REMAINING ITEMS: \(chosenCities)")
                }
                
                Getter_Setter.chosenCityName = chosenCities

                
                
                
            }else{
                
                let cities = subjectInformation[(indexPath as NSIndexPath).row]
                let cityName = cities.value(forKey: "cityName") as! String
                let trimCity = cityName.trimmingCharacters(in: .whitespaces)
                
                defaults.set(cityName, forKey: "chosenCity")
                defaults.set("fromCity", forKey: "chosenIndicator")
                
                cell.lblPlaces.layer.borderWidth = 1
                cell.lblPlaces.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                cell.lblPlaces.layer.cornerRadius = 2
                cell.lblPlaces.clipsToBounds = true
                cell.lblPlaces.textColor = UIColor.white
                
                chosenCities.append(trimCity)
                chosenIndex.append(indexPath.row)
                
                Getter_Setter.chosenCityName = chosenCities
                
                
            }
            
        }
        
    }
    
    
   /* func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        defaults.set("fromSortFilter", forKey: "retainFromHospital")
        
        if locationStatus == "province"{
            
        }else{

            let cities = subjectInformation[(indexPath as NSIndexPath).row]
            let cityName = cities.value(forKey: "cityName") as! String
            let trimCity = cityName.trimmingCharacters(in: .whitespaces)
            
            if let index = chosenCities.index(of: trimCity) {
                chosenCities.remove(at: index)
                
                
                print("REMAINING ITEMS: \(chosenCities)")
            }
            
            if let index2 = chosenIndex.index(of: indexPath.row) {
                chosenIndex.remove(at: index2)
                
                
                print("REMAINING ITEMS: \(chosenCities)")
            }
            
            Getter_Setter.chosenCityName = chosenCities
            
        }
        
    } */




}
