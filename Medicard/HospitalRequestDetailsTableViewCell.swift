//
//  HospitalRequestDetailsTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 12/28/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit

class HospitalRequestDetailsTableViewCell: UITableViewCell {

    
    @IBOutlet var lblHospitalName: UILabel!
    @IBOutlet var lblHospitalAddress: UILabel!
    @IBOutlet var lblPhoneNumbers: UILabel!
    @IBOutlet var lblContactPerson: UILabel!
    @IBOutlet var lblClinicHours: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
