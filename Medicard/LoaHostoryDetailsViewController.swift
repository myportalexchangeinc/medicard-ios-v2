//
//  LoaHostoryDetailsViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 2/17/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView
import ReachabilitySwift
import Alamofire
import SwiftyJSON
import Photos

class LoaHostoryDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tblLoaHistoryDetails: UITableView!
    var strApprovalCode: String!
    var strMemberCode: String!
    var strMemberName: String!
    var strAge: String!
    var strGender: String!
    var strCompany: String!
    var strDateApproved: String!
    var strRequestingDoctor: String!
    var strProblemCondition: String!
    var strValidationDate: String!
    var strHospitalName: String!
    var strPageHeader: String!
    var strRequestStatus: String!
    var cancelledStatus: Int!
    var hideLastSection: Int!
    var tableViewImage: UIImage!
    
    let reachability = Reachability()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = strPageHeader
        
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.closeLoaHistoryDetails(_:)))
        
        tblLoaHistoryDetails.rowHeight = UITableViewAutomaticDimension
        tblLoaHistoryDetails.estimatedRowHeight = 228;
        
        if CustomPhotoAlbum.sharedInstance.fetchAssetCollectionForAlbum() == nil {
            
            CustomPhotoAlbum.sharedInstance.createAlbum()
            
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        print(strRequestStatus)
        
        if strRequestStatus == "APPROVED" || strRequestStatus == "APPROVED " || strRequestStatus == "OUTSTANDING"{
            cancelledStatus = 0
            hideLastSection = 0
            tblLoaHistoryDetails.reloadData()
        }else{
            cancelledStatus = 1
            hideLastSection = 0
            tblLoaHistoryDetails.reloadData()
        }
        
        print("CANCEL STATUS \(cancelledStatus)")
        
    }
    
    func closeLoaHistoryDetails(_ sender: AnyObject){
        
        Getter_Setter.sortingAndFilterIndicator = 0
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if hideLastSection == 0{
            if section == 5{
                if cancelledStatus == 0{
                    return 1
                }else{
                    return 0
                }
            }else if section == 6{
                if cancelledStatus == 0{
                    return 0
                }else{
                    return 1
                }
            }else{
                return 1
            }

        }else{
            if section == 5{
                return 0
            }else if section == 6{
                if cancelledStatus == 0{
                    return 0
                }else{
                    return 0
                }
            }else{
                return 1
            }

        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cellIdentifier = "Cell"
            let cell: UITableViewCell! = tblLoaHistoryDetails.dequeueReusableCell(withIdentifier: cellIdentifier)
            
            if strRequestStatus == "APPROVED " || strRequestStatus == "APPROVED" || strRequestStatus == "OUTSTANDING" {
                 cell.textLabel?.text = "REQUEST SUBMITTED"
            }else if strRequestStatus == "CANCELLED" {
                 cell.textLabel?.text = "REQUEST \(strRequestStatus!)"
            }else if strRequestStatus == "EXPIRED" {
                 cell.textLabel?.text = "REQUEST \(strRequestStatus!)"
            }else if strRequestStatus == "AVAILED"{
                 cell.textLabel?.text = "REQUEST \(strRequestStatus!)"
            }
            
            
            
           
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            cell.textLabel?.textAlignment = .center
            
            cell.separatorInset = UIEdgeInsetsMake(0, -1000, 0, 0)
            
            cell.selectionStyle = .none
            
            return cell

        }else if indexPath.section == 1{
            
             let cell:RequestLoaResultFirstTableViewCell! = tblLoaHistoryDetails.dequeueReusableCell(withIdentifier: "firstCell", for: indexPath) as! RequestLoaResultFirstTableViewCell
            
            cell.lblCompanyName.text = strCompany
            cell.lblMemberName.text = strMemberName
            cell.lblAge.text = strAge
            cell.lblApprovalCode.text = strApprovalCode
            cell.lblMemberCode.text = strMemberCode
            
            print("date: \(strDateApproved)")
            
            
            
            //2017-02-02 09:45
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            formatter.date(from: strDateApproved!)
            let dateFormatted = formatter.date(from: strDateApproved!)
            formatter.dateFormat = "MMMM dd, yyyy"
            
            cell.lblDateApproved.text = formatter.string(from: dateFormatted!)
            
            
            if strGender != nil{
                
                if strGender == "0" {
                    cell.lblGender.text = "FEMALE"
                }else{
                    cell.lblGender.text = "MALE"
                }
                
            }
            
            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.section == 2{
            
            let cell:DoctorRequestDetailsTableViewCell! = tblLoaHistoryDetails.dequeueReusableCell(withIdentifier: "doctorCell", for: indexPath) as! DoctorRequestDetailsTableViewCell
            
            cell.lblDoctorName.text = strRequestingDoctor
            
            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.section == 3{
            
            let cell:HospitalRequestDetailsTableViewCell! = tblLoaHistoryDetails.dequeueReusableCell(withIdentifier: "hospitalCell", for: indexPath) as! HospitalRequestDetailsTableViewCell
            
            cell.lblHospitalName.text = strHospitalName
            
            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.section == 4{
            
            let cell:ProblemConditionTableViewCell! = tblLoaHistoryDetails.dequeueReusableCell(withIdentifier: "problemCell", for: indexPath) as! ProblemConditionTableViewCell
            
            cell.lblProblemContent.text = strProblemCondition
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            formatter.date(from: strDateApproved!)
            let dateFormatted = formatter.date(from: strDateApproved!)
            formatter.dateFormat = "MM/dd/yyyy"
            let result = formatter.string(from: dateFormatted!)
            
            //ADDED 3DAYS FROM CURRENT DATE
            let tomorrow = Calendar.current.date(byAdding: .day, value: 3, to: dateFormatted!)
            formatter.dateFormat = "MM/dd/YYYY"
            let result2 = formatter.string(from: tomorrow!)
            
            
            cell.lblDueDate.text = "This form is valid from " + result + " to " + result2 + "."
            
            
            cell.selectionStyle = .none
            
            return cell
            
        }else if indexPath.section == 5{
            
            let cell:DoneRequestDetailsTableViewCell! = tblLoaHistoryDetails.dequeueReusableCell(withIdentifier: "doneCell", for: indexPath) as! DoneRequestDetailsTableViewCell
            
            cell.btnGoBack.addTarget(self, action: #selector(goBackAction(sender:)), for: .touchUpInside)
            cell.btnCancelRequest.addTarget(self, action: #selector(cancelRequestAction(sender:)), for: .touchUpInside)
            cell.btnDownloadLoa.addTarget(self, action: #selector(downloadLoaAction(sender:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            
            return cell
            
        }else{
            
            let cell:DoneRequestDetailsTableViewCell! = tblLoaHistoryDetails.dequeueReusableCell(withIdentifier: "goBackCell", for: indexPath) as! DoneRequestDetailsTableViewCell
            
            cell.btnGoBack.addTarget(self, action: #selector(goBackAction(sender:)), for: .touchUpInside)
            
            
            cell.selectionStyle = .none
            
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 6{
            return 60
        }else{
            return UITableViewAutomaticDimension
        }
        
        
    }
    
    
    
    @IBAction func goBackAction(sender: UIButton){
    
    
        self.dismiss(animated: true, completion: nil)
    
    }
    
    @IBAction func cancelRequestAction(sender: UIButton){
        
        let alertController = UIAlertController(title: "", message: AlertMessages.warningCancelLoa, preferredStyle: UIAlertControllerStyle.alert)
        let cancelButton = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
            
        }
        
        let okButton = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            
            if self.reachability?.isReachable == true{
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Cancelling Request..."
                loadingNotification.isUserInteractionEnabled = false;
                self.tblLoaHistoryDetails.isUserInteractionEnabled = false
                
                self.cancelLoaRequest(approvalNumber: self.strApprovalCode)
                
            }else{
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
                
            }
            
        }
        alertController.addAction(cancelButton)
        alertController.addAction(okButton)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func downloadLoaAction(sender: UIButton){
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            
            let alertController = UIAlertController(
                title: "",
                message: "Allow Medicard to access your Photos in able to download this form. Thank you.",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Settings", style: .default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }else{
            
            
            let delayInSeconds = 3.0
            let delaySavingImage = 1.5
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Creating Album..."
            loadingNotification.isUserInteractionEnabled = false;
            tblLoaHistoryDetails.isUserInteractionEnabled = false
            
            if CustomPhotoAlbum.sharedInstance.fetchAssetCollectionForAlbum() == nil {
                
                CustomPhotoAlbum.sharedInstance.createAlbum()
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    
                    self.hideLastSection = 1
                    self.tblLoaHistoryDetails.reloadData()
                    
                    loadingNotification.label.text = "Saving LoA..."
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delaySavingImage) {
                        
                        self.tableViewImage = self.tblLoaHistoryDetails.screenshot
                        CustomPhotoAlbum.sharedInstance.save(image: self.tableViewImage)
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.tblLoaHistoryDetails.isUserInteractionEnabled = true
                        
                        self.hideLastSection = 0
                        self.tblLoaHistoryDetails.reloadData()
                        
                        _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.successDownloadedForm, closeButtonTitle:"OK")
                        
                    }
                    
                }
                
                
            }else{
                
                self.hideLastSection = 1
                self.tblLoaHistoryDetails.reloadData()
                
                loadingNotification.label.text = "Saving LoA..."
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delaySavingImage) {
                    
                    
                    self.tableViewImage = self.tblLoaHistoryDetails.screenshot
                    CustomPhotoAlbum.sharedInstance.save(image: self.tableViewImage)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblLoaHistoryDetails.isUserInteractionEnabled = true
                    
                    self.hideLastSection = 0
                    self.tblLoaHistoryDetails.reloadData()
                    
                    _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.successDownloadedForm, closeButtonTitle:"OK")
                    
                }
                
                
            }
            
            
        }
        
    }
    
    
    func tableViewHeight() -> CGFloat {
        tblLoaHistoryDetails.reloadData()
        tblLoaHistoryDetails.layoutIfNeeded()
        
        return tblLoaHistoryDetails.contentSize.height
    }
    
    
    func cancelLoaRequest(approvalNumber: String){
        
        let params = ["approvalNo" : approvalNumber]  as [String : Any]
        
        let request = Alamofire.request(PostRouter.postCancelRequest(params as [String : AnyObject]))
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblLoaHistoryDetails.isUserInteractionEnabled = true
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblLoaHistoryDetails.isUserInteractionEnabled = true
                    
                    self.strRequestStatus = "CANCELLED"
                    self.cancelledStatus = 1
                    self.tblLoaHistoryDetails.reloadData()
                    
                    Getter_Setter.cancelRequestStatus = 1
                    Getter_Setter.sortingAndFilterIndicator = 0
                    
                    print(post.description)
                    
                }
        }
        debugPrint(request)

    }

}
