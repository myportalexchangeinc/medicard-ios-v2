//
//  ViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 9/26/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit
import Foundation
import NVActivityIndicatorView
import CRToastSwift
import SCLAlertView
import Alamofire
import SwiftyJSON
import ReachabilitySwift
import CoreData
import Photos


class ViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable{
    
    
    
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var txtUsername: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    let validateForm = FormValidation()
    let reachability = Reachability()
    
     var loadingNotification = MBProgressHUD()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.navigationBar.isHidden = true
        
        btnSignIn.layer.cornerRadius = 2
        btnSignUp.layer.cornerRadius = 2
        btnSignUp.layer.borderWidth = 0.5
        btnSignUp.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
        
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            
            CustomPhotoAlbum.sharedInstance.createAlbum()
            
        }
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        
        return true
    }
    
    
    @IBAction func signUpButton(_ sender: AnyObject) {
        
        
        self.txtUsername.text = ""
        self.txtPassword.text = ""
        
        let goToRegistration = self.storyboard!.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        let navController = UINavigationController(rootViewController: goToRegistration)
        self.present(navController, animated:true, completion: nil)


    }
    
    
    @IBAction func signInButton(_ sender: AnyObject) {
        
        
        if (validateForm.isEmpty(txtUsername) || validateForm.isEmpty(txtPassword)) {
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
            
            
        }else{
            
            if reachability?.isReachable == true{
                
                loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Logging in..."
                loadingNotification.isUserInteractionEnabled = false;
                self.view.isUserInteractionEnabled = false
                
                login()
                
            }else{
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
            }
            
        }
        
    }
    
    
    
    func login(){
        
        let username = txtUsername.text! as String
        let password = txtPassword.text! as String
        
        let loginPost = ["username": username, "password": password] as [String : Any]
        
        Alamofire.request(PostRouter.postLogin(loginPost as [String : AnyObject]))
            .responseJSON { response in
                if response.result.isFailure == true {
                    
                    print("ERROR", response.description)
                    
                     _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    let post = JSON(value)
                    
                    
                    
                    print("JSON DATA:\(post.description)")
                    
                    if post["responseCode"].stringValue == "210"{
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorNoUserAccount, closeButtonTitle:"OK")
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.isUserInteractionEnabled = true
                        
                    }else if post["responseCode"].stringValue == "220"{
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorAccountLocked, closeButtonTitle:"OK")
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.isUserInteractionEnabled = true
                        
                        print("JSON DATA 220")
                        
                    }else if post["responseCode"].stringValue == "230"{
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.isUserInteractionEnabled = true
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorCredentials, closeButtonTitle:"OK")
                        
                    }else{
                        
                        let userData = post["UserAccount"]
                        let pinNumber = userData["PIN"].stringValue 
                        
                        print("JSON DATA" + userData.description)
                        
                        let defaults = UserDefaults.standard
                        defaults.set(userData["MEM_CODE"].stringValue, forKey: "memberCode")
                        defaults.set(userData["FORCE_CHANGE_PASSWORD"].stringValue, forKey: "forceChangePassword")
                        defaults.set(pinNumber, forKey: "pinNumber")
                        
                        self.loadingNotification.label.text = "Updating Hospitals..."
                        
                        self.saveHospitals()
                        
                        
                    }
                    
                }
                
        }
        
    }
    
    
    
    func saveHospitals(){
        
        
        let request = Alamofire.request(PostRouter.getHospitalList as URLRequestConvertible)
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    let hospitalList = post["hospitalList"]
                    
                    print("JSON DATA" + hospitalList.description)
                    
                                    
                    for item in hospitalList.arrayValue {

                        
                        let appDelegate =
                            UIApplication.shared.delegate as! AppDelegate
                        
                        let managedContext = appDelegate.managedObjectContext
                        
                        let entity = NSEntityDescription.entity(forEntityName: "HospitalsEntity",
                                                                in: managedContext)!
                        
                        let hospital = NSManagedObject(entity: entity,
                                                       insertInto: managedContext)
                        
                        let cityName = item["city"].stringValue
                        let trimStrCity = cityName.trimmingCharacters(in:.whitespaces)

                        
                        // add our data
                        hospital.setValue(item["hospitalCode"].stringValue, forKey: "hospitalCode")
                        hospital.setValue(item["hospitalName"].stringValue, forKey: "hospitalName")
                        hospital.setValue(item["keyword"].stringValue, forKey: "keyword")
                        hospital.setValue(item["alias"].stringValue, forKey: "alias")
                        hospital.setValue(item["category"].stringValue, forKey: "category")
                        hospital.setValue(item["coordinator"].stringValue, forKey: "coordinator")
                        hospital.setValue(item["streetAddress"].stringValue, forKey: "streetAddress")
                        hospital.setValue(trimStrCity, forKey: "city")
                        hospital.setValue(item["province"].stringValue, forKey: "province")
                        hospital.setValue(item["region"].stringValue, forKey: "region")
                        hospital.setValue(item["phoneNo"].stringValue, forKey: "phoneNo")
                        hospital.setValue(item["faxno"].stringValue, forKey: "faxno")
                        hospital.setValue(item["contactPerson"].stringValue, forKey: "contactPerson")
                        
                        
                        do {
                            try managedContext.save()
                            
                            print("Saved!")
                            
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                        
                    }

                    self.loadingNotification.label.text = "Updating Cities..."
                    
                    self.saveCities()

                }
        }
        debugPrint(request)

    }
    
    
    
    func saveCities(){
        
        let request = Alamofire.request(PostRouter.getCityList as URLRequestConvertible)
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    let citiesList = post["cities"]
                    
                    print("JSON DATA" + citiesList.description)
                    
                    
                    
                    for item in post["cities"].arrayValue {
                        
                    
                        let appDelegate =
                            UIApplication.shared.delegate as! AppDelegate
                        
                        let managedContext = appDelegate.managedObjectContext
                        
                        let entity = NSEntityDescription.entity(forEntityName: "CityEntity",
                                                                in: managedContext)!
                        
                        let cities = NSManagedObject(entity: entity,
                                                       insertInto: managedContext)
                        
                        // add our data
                        cities.setValue(item["cityCode"].stringValue, forKey: "cityCode")
                        cities.setValue(item["cityName"].stringValue, forKey: "cityName")
                        cities.setValue(item["provinceCode"].stringValue, forKey: "provinceCode")
                        cities.setValue(item["regionCode"].stringValue, forKey: "regionCode")
                        cities.setValue(item["countryCode"].stringValue, forKey: "countryCode")
                        cities.setValue(item["countryName"].stringValue, forKey: "countryName")
                        cities.setValue("false", forKey: "isSelected")
                        
                        
                        do {
                            try managedContext.save()
                            
                            print("Saved!")
                            
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }

                    }
                    

                    self.loadingNotification.label.text = "Updating Province..."
                    
                    self.saveProvinces()
                    
                }
        }
        debugPrint(request)
        
    }

    
    
    func saveProvinces(){
        
        let request = Alamofire.request(PostRouter.getProvinceList as URLRequestConvertible)
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    let provinceList = post["provinces"]
                    
                    print("JSON DATA" + provinceList.description)
                    
                    
                    for item in provinceList.arrayValue {
                        
                       let appDelegate =
                           UIApplication.shared.delegate as! AppDelegate
                       
                       let managedContext = appDelegate.managedObjectContext
                       
                       let entity = NSEntityDescription.entity(forEntityName: "ProvinceEntity",
                                                               in: managedContext)!
                       
                       let provinces = NSManagedObject(entity: entity,
                                                    insertInto: managedContext)
                       
                       // add our data
                       provinces.setValue(item["provinceCode"].stringValue, forKey: "provinceCode")
                       provinces.setValue(item["provinceName"].stringValue, forKey: "provinceName")
                       provinces.setValue(item["regionCode"].stringValue, forKey: "regionCode")
                       
                       
                       do {
                           try managedContext.save()
                           
                           print("Saved!")
                           
                       } catch let error as NSError {
                           print("Could not save. \(error), \(error.userInfo)")
                       }
                        
                    }
                    
                    self.loadingNotification.label.text = "Updating Specialization..."
                    
                    self.saveSpecialization()
                    
                }
        }
        debugPrint(request)
        
    }
    
    func saveSpecialization(){
    
        let request = Alamofire.request(PostRouter.getSpecialization as URLRequestConvertible)
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    let provinceList = post["specializations"]
                    
                    print("JSON DATA" + provinceList.description)
                    
                    
                    for item in provinceList.arrayValue {
                        
                        let appDelegate =
                            UIApplication.shared.delegate as! AppDelegate
                        
                        let managedContext = appDelegate.managedObjectContext
                        
                        let entity = NSEntityDescription.entity(forEntityName: "SpecializationEntity",
                                                                in: managedContext)!
                        
                        let provinces = NSManagedObject(entity: entity,
                                                        insertInto: managedContext)
                        
                        // add our data
                        provinces.setValue(item["specializationCode"].stringValue, forKey: "specializationCode")
                        provinces.setValue(item["specializationDescription"].stringValue, forKey: "specializationDescription")
                        
                        
                        do {
                            try managedContext.save()
                            
                            print("Saved!")
                            
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                        
                        
                    }
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let defaults = UserDefaults.standard
                    defaults.set("1", forKey: "loginStatus")
                    defaults.set(self.txtUsername.text, forKey: "username")
                    defaults.set(self.txtPassword.text, forKey: "password")
                    defaults.set(self.txtUsername.text, forKey: "storedUsername")
                    defaults.set(self.txtPassword.text, forKey: "storedPassword")
                    
                    self.txtUsername.text = ""
                    self.txtPassword.text = ""
                    
                    
                    let medicardAccounts = self.storyboard!.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                    medicardAccounts.modalTransitionStyle = .crossDissolve
                    self.present(medicardAccounts, animated:true, completion: nil)
                    
                }
        }
        debugPrint(request)
        
    }
    
}

