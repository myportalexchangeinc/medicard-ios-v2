//
//  SortAndFilterHospitalViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 1/25/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView

class SortAndFilterHospitalViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    @IBOutlet var edtSortBy: UITextField!
    @IBOutlet var btnCheckBox: UIButton!
    @IBOutlet var edtChooseProvince: UITextField!
    @IBOutlet var edtChooseCity: UITextField!
    @IBOutlet var btnResetFilters: UIButton!
    @IBOutlet var btnShowResult: UIButton!
    @IBOutlet var edtFindCity: TextfieldModification!
    var sortByPickerView: UIPickerView = UIPickerView()
    
    var sortBy = ["Hospital/Clinic Name", "MediCard Clinic First", "City", "Province"]
    
    let defaults = UserDefaults.standard
    var cityName: String = ""
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Sort and Filter Hospital"
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelSortingAndFiltering(_:)))
        
        btnShowResult.layer.cornerRadius = 2
        btnResetFilters.layer.cornerRadius = 2
        
        edtSortBy.text = sortBy[0]
        
        sortByPickerView.delegate = self
        sortByPickerView.dataSource = self
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        

//      if defaults.value(forKey: "retainFromHospital") as! String == "fromHospital"{
//          
//          edtChooseCity.text = defaults.value(forKey: "filterCity") as? String
//          edtSortBy.text = defaults.value(forKey: "filterSortBy") as? String
//          edtChooseProvince.text = defaults.value(forKey: "filterProvince") as? String
//          defaults.set(edtChooseProvince.text, forKey: "chosenProvince")
//          
//      }else if defaults.value(forKey: "retainFromHospital") as! String == "fromSortFilter"{
//          
//          edtChooseProvince.text = defaults.value(forKey: "chosenProvince") as! String!
//          
//          let cityNames = defaults.value(forKey: "filterCity") as? [String] ?? [String]()
//          let str = String(describing: cityNames)
//        
//        print("STR: \(removeSpecialCharsFromString(str))")
//          
//          
//          if defaults.value(forKey: "chosenIndicator") as! String! == "fromProvince"{
//
//            edtChooseCity.text =  "All Cities"
//            defaults.set("All Cities", forKey: "filterCity")
//            
//          }else{
//            
//            edtChooseCity.text = removeSpecialCharsFromString(str)
//
//          }
//          
//          
//      }
//        
//        edtFindCity.text = defaults.value(forKey: "hospitalsName") as? String
//        
//        if Getter_Setter.medicardClinicFirst == true{
//            btnCheckBox.isSelected = true
//            btnCheckBox.setBackgroundImage(UIImage(named: "ic_selected.png"), for: UIControlState.normal)
//        }else{
//            btnCheckBox.isSelected = false
//            btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
//        }


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func removeSpecialCharsFromString(_ text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
     
        if textField == edtSortBy{
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            showAlertSortBy()
            
            return false
            
        }else if textField == edtChooseProvince{
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let goToChoosePlace = self.storyboard!.instantiateViewController(withIdentifier: "ChoosePlaceViewController") as! ChoosePlaceViewController
            goToChoosePlace.locationStatus = "province"
            self.navigationController?.pushViewController(goToChoosePlace, animated: true)
            
            return false
            
            
        }else if textField == edtChooseCity{
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let goToChoosePlace = self.storyboard!.instantiateViewController(withIdentifier: "ChoosePlaceViewController") as! ChoosePlaceViewController
            goToChoosePlace.locationStatus = "city"
            self.navigationController?.pushViewController(goToChoosePlace, animated: true)
            
            return false
            
            
        }else{
            
            
            return true
            
        }
        
      

    }


    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortBy.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        edtSortBy.text = sortBy[row]
        
        return sortBy[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("ROW: \(row)")
        
    }

    
    func addPicker(){
        
        edtSortBy.inputView = sortByPickerView
        sortByPickerView.backgroundColor = UIColor.white
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        edtSortBy.inputAccessoryView = toolBar
        
        
    }
    
    func pickerDoneClicked(){
        
        edtSortBy.resignFirstResponder()
        
    }
    
    
    func cancelSortingAndFiltering(_ sender: AnyObject){
        
//        defaults.set("", forKey: "chosenProvinceCode")
//        defaults.set("Hospital/Clinic Name", forKey: "filterSortBy")
//        defaults.set("All Provinces", forKey: "filterProvince")
//        defaults.set("All Cities", forKey: "filterCity")
//        defaults.set("", forKey: "hospitalsName")
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func resetFilterAction(_ sender: Any) {
        
//        defaults.set("", forKey: "chosenProvinceCode")
//        defaults.set("Hospital/Clinic Name", forKey: "filterSortBy")
//        defaults.set("All Provinces", forKey: "filterProvince")
//        defaults.set("All Cities", forKey: "filterCity")
//        defaults.set("", forKey: "hospitalsName")
//        
//        edtSortBy.text = "Hospital/Clinic Name"
//        edtChooseProvince.text = "All Provinces"
//        edtChooseCity.text = "All Cities"
//        edtFindCity.text = ""
//        
//        
//        viewDidLoad()
        
    }
    
    @IBAction func showResultFilter(_ sender: Any) {
        
//        defaults.set(edtSortBy.text, forKey: "filterSortBy")
//        defaults.set(edtChooseProvince.text, forKey: "filterProvince")
//        defaults.set(edtChooseCity.text, forKey: "filterCity")
//        
//        defaults.set("fromSortFilter", forKey: "retainFromHospital")
//        defaults.set(edtFindCity.text, forKey: "hospitalsName")
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func checkBoxAction(_ sender: UIButton) {
        
        if (btnCheckBox.isSelected == true)
        {
            btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
            btnCheckBox.isSelected = false
            Getter_Setter.medicardClinicFirst = false
            
        }else{
            btnCheckBox.setBackgroundImage(UIImage(named: "ic_selected.png"), for: UIControlState.normal)
            btnCheckBox.isSelected = true
            Getter_Setter.medicardClinicFirst = true
        }

        
    }
    
    
    func showAlertSortBy(){
        
        
        let alert = SCLAlertView()
        let icon = UIImage(named:"ic_sort_image.png")
        alert.addButton("Hospital/Clinic Name"){
            
            self.edtSortBy.text = "Hospital/Clinic Name"
        }
        
        alert.addButton("MediCard Clinic First") {
            
            self.edtSortBy.text = "MediCard Clinic First"
        }
        
        alert.addButton("City") {
            
            self.edtSortBy.text = "City"
        }
        
        alert.addButton("Province") {
            
            self.edtSortBy.text = "Province"
        }
        
        
        alert.showCustom("Sort By", subTitle: "", color: UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0), icon: icon!)

        
    }
}
