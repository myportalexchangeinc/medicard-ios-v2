//
//  SaveTableScreenshot.swift
//  Medicard
//
//  Created by Al John Albuera on 2/19/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView

class SaveTableScreenshot: NSObject {
    
    
    func savePhotoImage(tableView: UITableView, tableViewHeight: CGFloat){
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: tableView.contentSize.width, height: tableView.contentSize.height - 130),false, 0.0)
        
        let context = UIGraphicsGetCurrentContext()
        
        let previousFrame = tableView.frame
        
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.contentSize.width, height: tableViewHeight - 130);
        
        tableView.layer.render(in: context!)
        
        tableView.frame = previousFrame
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext();
        
        
        CustomPhotoAlbum.sharedInstance.save(image: image!)
        
        _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.successDownloadedForm, closeButtonTitle:"OK")

        
    }
    

    
    

}
