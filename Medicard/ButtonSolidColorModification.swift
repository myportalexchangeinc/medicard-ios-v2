//
//  ButtonSolidColorModification.swift
//  Medicard
//
//  Created by Al John Albuera on 1/30/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class ButtonSolidColorModification: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
        self.layer.cornerRadius = 3

    }

}
