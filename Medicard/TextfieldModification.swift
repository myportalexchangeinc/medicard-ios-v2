//
//  TextfieldModification.swift
//  Medicard
//
//  Created by Al John Albuera on 1/17/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class TextfieldModification: UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let spacerView = UIView(frame:CGRect(x: 0, y: 0, width: 10, height: 10))
        leftViewMode = .always
        leftView = spacerView
        
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.cornerRadius = 5
        
    }
    

}
