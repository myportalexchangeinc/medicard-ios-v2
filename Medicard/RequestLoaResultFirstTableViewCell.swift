//
//  RequestLoaResultFirstTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/3/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class RequestLoaResultFirstTableViewCell: UITableViewCell {
    
    @IBOutlet var lblMemberCode: UILabel!
    @IBOutlet var lblMemberName: UILabel!
    @IBOutlet var lblAge: UILabel!
    @IBOutlet var lblGender: UILabel!
    @IBOutlet var lblCompanyName: UILabel!
    @IBOutlet var lblDateApproved: UILabel!
    @IBOutlet var lblRemarks: UILabel!
    @IBOutlet var lblValidityDate: UILabel!
    @IBOutlet var lblEffectivityDate: UILabel!
    @IBOutlet var lblApprovalCode: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
