//
//  SelectDoctorViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 12/28/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData
import SCLAlertView
import ReachabilitySwift

class SelectDoctorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tblSelectDoctor: UITableView!
    @IBOutlet var btnEnterDoctor: UIButton!
    
    @IBOutlet var searchDoctor: UISearchBar!
    @IBOutlet var lblNotFound: UILabel!
    
    
    let defaults = UserDefaults.standard
    
    var doctorName = [String] ()
    var doctorSpec = [String] ()
    var doctorCode = [String] ()
    var doctorMobileNum = [String] ()
    var subjectInformation = [NSManagedObject]()
    
    var sortBySorting: String!
    var filterByRoom: String!
    
    let reachability = Reachability()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.\
        
        
        let header = defaults.string(forKey: "consultationHeader")! as String;
        
        if header == "consultation" {
            self.title = "Consultation"
        }else{
            self.title = "Maternity Consultation"
        }
        
        tblSelectDoctor.rowHeight = UITableViewAutomaticDimension
        tblSelectDoctor.estimatedRowHeight = 52;

        
        addToolBarMedicarNum()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading..."
        loadingNotification.isUserInteractionEnabled = false;
        tblSelectDoctor.isUserInteractionEnabled = false
        
        print("CHOSEN SPECIALIZATION NAME: \(defaults.value(forKey: "chosenSpecializationName")!)")
        print("CHOSEN SPECIALIZATION CODE: \(defaults.value(forKey: "chosenSpecializationCode")!)")
        print("CHOSEN SPECIALIZATION INDEXES: \(defaults.value(forKey: "chosenSpecializationIndexes")!)")
        print("ROOM NUM: \(defaults.value(forKey: "room")!)")
        print("SORTING DATA: \(defaults.value(forKey: "sortingData")!)")
        print("FILTER SPECIALIZATION: \(defaults.value(forKey: "filterSpecialization")!)")
        
        sortBySorting = defaults.value(forKey: "sortingData") as! String
        filterByRoom = defaults.value(forKey: "room") as! String
        
        searchDoctor.text = defaults.value(forKey: "doctorsName") as? String
        
        print("DOCTORS NAME: \(searchDoctor.text)")
        
        let specializationCodeCount = defaults.value(forKey: "chosenSpecializationName") as? [String] ?? [String]()
        
        
        if reachability?.isReachable == true{
            
            if defaults.value(forKey: "doctorsName") as? String == ""  && specializationCodeCount.count == 0 && filterByRoom == "Room not specified." || filterByRoom == "N/A"{
                deleteDataEntity()
            }else if defaults.value(forKey: "doctorsName") as? String == ""  && specializationCodeCount.count == 0 && filterByRoom == ""{
                 deleteDataEntity()
            }else{
                fetchFilteredDoctors()
            }
            
        }else{
            
              _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
            
        }
        
       

    }
    
    fileprivate func searchBarDidBeginEditing(_ searchBar: UISearchBar) {
        
        if searchBar == searchDoctor {
            
            ShowKeyboard.animateViewMoving(up: true, moveValue: -100, view: self.view)
            addToolBarMedicarNum()
            
        }
        
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectInformation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:SelecDoctorTableViewCell! = tblSelectDoctor.dequeueReusableCell(withIdentifier: "SelectDoctorCell", for: indexPath) as! SelecDoctorTableViewCell
        
        let docDetails = subjectInformation[(indexPath as NSIndexPath).row]
        
        let LName = docDetails.value(forKey: "docLname") as! String
        let FName = docDetails.value(forKey: "docFname") as! String
        let Room = docDetails.value(forKey: "room") as! String!
        let Schedule = docDetails.value(forKey: "schedule") as? String
        
        cell.lblDoctorName.text = LName + ", " + FName
        cell.lblDoctorSkill.text = docDetails.value(forKey: "specDesc") as? String
        
        if Room == "" {
            
             cell.lblDoctorRoom.text = "Room not specified."
            
        }else{
            
             cell.lblDoctorRoom.text = "Room: \(Room!)"
            
        }
        
        if Schedule == "" {
            
            cell.lblDocSchedule.text = "Schedule not specified."
            
        }else{
            
            cell.lblDocSchedule.text = "Schedule: \(docDetails.value(forKey: "schedule")!)"
        }
        
        
        

        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let docDetails = subjectInformation[(indexPath as NSIndexPath).row]
       
            let goToRequestDetails = self.storyboard!.instantiateViewController(withIdentifier: "NewRequestDetailsViewController") as! NewRequestDetailsViewController
            defaults.set("choose", forKey: "tableIndicator")
            
            //backend
            defaults.set(docDetails.value(forKey: "doctorCode"), forKey: "doctorCode")
            
            //frontend
        
            let LName = docDetails.value(forKey: "docLname") as! String
            let FName = docDetails.value(forKey: "docFname") as! String
        
            defaults.set(LName + ", " + FName, forKey: "doctorName")
            defaults.set(docDetails.value(forKey: "specDesc"), forKey: "doctorSpec")
            defaults.set(docDetails.value(forKey: "room"), forKey: "room")
            defaults.set(docDetails.value(forKey: "schedule"), forKey: "schedule")
    
            
        
            if docDetails.value(forKey: "room") as! String == "" {
            
                defaults.set("Room not specified.", forKey: "room")
            
            }else{
            
                defaults.set(docDetails.value(forKey: "room") as! String!, forKey: "room")
            
            }

        
            defaults.set(docDetails.value(forKey: "schedule"), forKey: "schedule")
        
            navigationController?.pushViewController(goToRequestDetails, animated: true)
            

    }
    
    
    @IBAction func enterDoctorAction(_ sender: Any) {
        
        defaults.set("", forKey: "doctorName")
        defaults.set("", forKey: "doctorSpec")
        defaults.set("", forKey: "room")
        defaults.set("", forKey: "schedule")
        defaults.set("", forKey: "room")
        
        let goToRequestDetails = self.storyboard!.instantiateViewController(withIdentifier: "NewRequestDetailsViewController") as! NewRequestDetailsViewController
        defaults.set("input", forKey: "tableIndicator")
        navigationController?.pushViewController(goToRequestDetails, animated: true)
        
        
    }
    
    func getDoctorsList(){
        
        print("HOSPITAL CODE: \(Getter_Setter.selectedHospitalCode)")
        
        let hospitalCode = Getter_Setter.selectedHospitalCode
        
        let verifyParams = ["hospitalCode": hospitalCode]
        
        let request = Alamofire.request(PostRouter.getDoctorsToHospital(verifyParams as [String: AnyObject]))
            .responseJSON { response in
                guard response.result.error == nil else {
                    print(response.result.error!)
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                     _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                    return
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    let post = JSON(value)
                    print("JSON DATA" + post.description)
                    
                    if (post["getDoctorsToHospital"].count == 0){
                        self.tblSelectDoctor.isHidden = true;
                    }else{
                        self.tblSelectDoctor.isHidden = false;
                    }
                    
                    for item in post["getDoctorsToHospital"].arrayValue{

                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let managedContext = appDelegate.managedObjectContext
                        
                        let entity = NSEntityDescription.entity(forEntityName: "DoctorsEntity",
                                                                in: managedContext)!
                        
                        let doctors = NSManagedObject(entity: entity,
                                                      insertInto: managedContext)
                        
                        
                        doctors.setValue(item["doctorCode"].stringValue, forKey: "doctorCode")
                        doctors.setValue(item["docLname"].stringValue, forKey: "docLname")
                        doctors.setValue(item["docFname"].stringValue, forKey: "docFname")
                        doctors.setValue(item["docMname"].stringValue, forKey: "docMname")
                        doctors.setValue(item["hospitalCode"].stringValue, forKey: "hospitalCode")
                        doctors.setValue(item["hospitalName"].stringValue, forKey: "hospitalName")
                        doctors.setValue(item["specDesc"].stringValue, forKey: "specDesc")
                        doctors.setValue(item["specCode"].stringValue, forKey: "specCode")
                        doctors.setValue(item["schedule"].stringValue, forKey: "schedule")
                        doctors.setValue(item["room"].stringValue, forKey: "room")
                        doctors.setValue(item["wtax"].stringValue, forKey: "wtax")
                        doctors.setValue(item["gracePeriod"].stringValue, forKey: "gracePeriod")
                        doctors.setValue(item["vat"].stringValue, forKey: "vat")
                        doctors.setValue(item["specialRem"].stringValue, forKey: "specialRem")
                        doctors.setValue(item["hospRemarks"].stringValue, forKey: "hospRemarks")
                        doctors.setValue(item["roomBoard"].stringValue, forKey: "roomBoard")
                        doctors.setValue(item["remarks"].stringValue, forKey: "remarks")
                        doctors.setValue(item["remarks2"].stringValue, forKey: "remarks2")
                        
                        
                        do {
                            try managedContext.save()
                            
                            print("Saved!")

                            
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                    }
                    
                        self.fetchDoctorFromHospital()

                        self.tblSelectDoctor.reloadData()
                        
                        
                    }
                
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblSelectDoctor.isUserInteractionEnabled = true
                    
                }
        
        debugPrint(request)
        
    }
    
    
    func deleteDataEntity(){
       
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
       let managedContext = appDelegate.managedObjectContext
       
       let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DoctorsEntity")
       let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
       
       do {
           try managedContext.execute(batchDeleteRequest)
           
           print("DELTEED!")
           
           self.getDoctorsList()
           
           
       } catch {
           // Error Handling
       }

    }
    
    
    
    func fetchDoctorFromHospital(){
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DoctorsEntity")

        
        //SORTING
        let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.returnsObjectsAsFaults = false //needed to make data accessible
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            subjectInformation = results
            
            print("SUBJECT INFORMATION: \(subjectInformation)")
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tblSelectDoctor.isUserInteractionEnabled = true
            
            if subjectInformation.count == 0{
                
                tblSelectDoctor.isHidden = true
                
            }else{
                tblSelectDoctor.isHidden = false
                tblSelectDoctor.reloadData()
            }
            
            
            
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
    
    
    func fetchFilteredDoctors(){
        
        let chosenDoctorsName = defaults.value(forKey: "doctorsName") as? String
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
         let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DoctorsEntity")
        
        let specializationName = defaults.value(forKey: "chosenSpecializationName") as? [String] ?? [String]()
        
        
        
        if defaults.value(forKey: "doctorsName") as? String == "" {
            
            if specializationName.count == 0{
                
                if filterByRoom == "" {
                    
                    print("HERE")
                    
                    fetchDoctorFromHospital()
                    
                }else{
                    
                    print("OR HERE")
                    
                    //FILTERING
                    let predicate1 = NSPredicate(format: "room CONTAINS [cd] %@", filterByRoom)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                    
                }
                
                
            }else{
                
                if filterByRoom == "" || filterByRoom == "N/A" {
                    
                    let predicate1 = NSPredicate(format: "specDesc IN [cd] %@", specializationName)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                }else{
                    
                    //FILTERING
                    let predicate1 = NSPredicate(format: "room CONTAINS [cd] %@", filterByRoom)
                    let predicate2 = NSPredicate(format: "specDesc IN [cd] %@", specializationName)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                    
                }

                
                
            }
            
        }else{
            
            
            if specializationName.count == 0{
                
                if filterByRoom == "" {
                    
                    //SORTING
                    let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR docFname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ ", chosenDoctorsName!, chosenDoctorsName!, chosenDoctorsName!)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors

                    
                }else{
                    
                    //FILTERING
                    let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR docFname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ ", chosenDoctorsName!, chosenDoctorsName!, chosenDoctorsName!)
                    let predicate2 = NSPredicate(format: "room CONTAINS [cd] %@", filterByRoom)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                    
                }
                
                
            }else{
                
                if filterByRoom == "" {
                    
                    
                    
                    let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR docFname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ ", chosenDoctorsName!, chosenDoctorsName!, chosenDoctorsName!)
                    let predicate2 = NSPredicate(format: "specDesc IN [cd] %@", specializationName)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                }else{
                    
                    //FILTERING
                    let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR docFname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ ", chosenDoctorsName!, chosenDoctorsName!, chosenDoctorsName!)
                    let predicate2 = NSPredicate(format: "specDesc IN [cd] %@", specializationName)
                    let predicate3 = NSPredicate(format: "room CONTAINS [cd] %@", filterByRoom)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                    
                }
                
                
            }
            
            
        }
        

        fetchRequest.returnsObjectsAsFaults = false //needed to make data accessible
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            subjectInformation = results
            
            print("SUBJECT INFORMATION: \(subjectInformation)")
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tblSelectDoctor.isUserInteractionEnabled = true
            
            if subjectInformation.count == 0{
                
                tblSelectDoctor.isHidden = true
                
            }else{
                tblSelectDoctor.isHidden = false
                tblSelectDoctor.reloadData()
            }
            
            
            
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        
        
    }
    
    
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.text = defaults.value(forKey: "doctorsName") as? String
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        let specializationName = defaults.value(forKey: "chosenSpecializationName") as? [String] ?? [String]()
        let specializationFiter = defaults.value(forKey: "filterSpecialization") as! String
        let finalSpecializationArray = specializationFiter.components(separatedBy: ", ")
        
            // Fallback on earlier versions
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DoctorsEntity")
       
        
        if specializationName.count == 0{
            
            
            if filterByRoom == ""{
                
                if (searchBar.text?.characters.count)! > 0{
                    
                    //SORTING
                    let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR docFname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ ", searchBar.text!, searchBar.text!, searchBar.text!)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                    fetchRequest.predicate = motherPredicate
                    
                    
                }
                
                
            }else{
                
                if (searchBar.text?.characters.count)! > 0{
                    
                    //FILTERING
                    let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR docFname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ AND room CONTAINS [cd] %@",searchBar.text!,searchBar.text!,searchBar.text!, filterByRoom)
                    let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                }
                
                
            }
            
            
            
        }else{
            
            if filterByRoom == ""{
                
                if (searchBar.text?.characters.count)! > 0{
                    
                    //FILTERING
                    let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR docFname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ AND specDesc IN [cd] %@",searchBar.text!,searchBar.text!, specializationName)
                    let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                }
                
                
                
                
            }else{
                
                if (searchBar.text?.characters.count)! > 0{
                    
                    //FILTERING
                    let predicate1 = NSPredicate(format: "docLname  CONTAINS [cd] %@ OR docFname OR CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ AND room CONTAINS [cd] %@ AND specDesc IN [cd] %@",searchBar.text!,searchBar.text!, filterByRoom, specializationName)
                    let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
                    fetchRequest.predicate = motherPredicate
                    
                    //SORTING
                    let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: true)
                    let sortDescriptors = [sortDescriptor]
                    fetchRequest.sortDescriptors = sortDescriptors
                    
                    
                }

            }
            
        }
        
        fetchRequest.returnsObjectsAsFaults = false //needed to make data accessible
        
        do {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tblSelectDoctor.isUserInteractionEnabled = true
            
            let results = try managedContext.fetch(fetchRequest)
            
            if searchBar.text == ""{
                self.fetchDoctorFromHospital()
                tblSelectDoctor.isHidden = false
                btnEnterDoctor.isHidden = false
                lblNotFound.text = "Doctor is not on the list."
                
            }else if results.count == 0{
                
                tblSelectDoctor.isHidden = true
                
            }else{
                subjectInformation = results
                tblSelectDoctor.isHidden = false
                btnEnterDoctor.isHidden = false
                lblNotFound.text = "Doctor is not on the list."
                print("RESUTLS: \(results)")
            }
            
            
        } catch {
            
            print("uh oh")
            
        }
        
        tblSelectDoctor.reloadData()
        
    }
    
    
    

    
    /* func loadDoctorsSearc(_ searchBar: UISearchBar){
        
         MBProgressHUD.hide(for: self.view, animated: true)
        
        let specializationName = defaults.value(forKey: "chosenSpecializationName") as? [String] ?? [String]()
        let specializationFiter = defaults.value(forKey: "filterSpecialization") as! String
        let finalSpecializationArray = specializationFiter.components(separatedBy: ", ")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
            
         let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DoctorsEntity")
         
         
         if specializationName.count == 0{
             
             
             if filterByRoom == ""{
                 
                 print("TRIGGER 1")
                 
                 if (searchBar.text?.characters.count)! > 0{
                     
                     //FILTERING
                     let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@ ", searchBar.text!, searchBar.text!)
                     let motherPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
                     fetchRequest.predicate = motherPredicate
                 }
                 
                 
                 
             }else{
                 
                 print("TRIGGER 2")
                 
                 if (searchBar.text?.characters.count)! > 0{
                     
                     //FILTERING
                     let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@",searchBar.text!,searchBar.text!)
                     let predicate2 = NSPredicate(format: "room MATCHES [cd] %@", filterByRoom)
                     let predicate3 = NSPredicate(format: "specDesc IN [cd] %@", specializationName)
                     let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                     fetchRequest.predicate = motherPredicate
                     
                     //SORTING
                     let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: false)
                     let sortDescriptors = [sortDescriptor]
                     fetchRequest.sortDescriptors = sortDescriptors
                     
                 }
                 
                 
                 
             }
             
             
         }else{
             
             if filterByRoom == ""{
                 
                 print("TRIGGER 3")
                 
                 if (searchBar.text?.characters.count)! > 0{
                     
                     //FILTERING
                     let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@",searchBar.text!,searchBar.text!)
                     let predicate2 = NSPredicate(format: "room MATCHES [cd] %@", filterByRoom)
                     let predicate3 = NSPredicate(format: "specDesc IN [cd] %@", specializationName)
                     let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                     fetchRequest.predicate = motherPredicate
                     
                     //SORTING
                     let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: false)
                     let sortDescriptors = [sortDescriptor]
                     fetchRequest.sortDescriptors = sortDescriptors
                     
                 }
                 
                 
             }else{
                 
                 print("TRIGGER 4")
                 
                 if (searchBar.text?.characters.count)! > 0{
                     
                     //FILTERING
                     let predicate1 = NSPredicate(format: "docLname CONTAINS [cd] %@ OR specDesc CONTAINS [cd] %@",searchBar.text!,searchBar.text!)
                     let predicate2 = NSPredicate(format: "room MATCHES [cd] %@", filterByRoom)
                     let predicate3 = NSPredicate(format: "specDesc IN [cd] %@", specializationName)
                     let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1, predicate2, predicate3])
                     fetchRequest.predicate = motherPredicate
                     
                     //SORTING
                     let sortDescriptor = NSSortDescriptor(key: sortBySorting, ascending: false)
                     let sortDescriptors = [sortDescriptor]
                     fetchRequest.sortDescriptors = sortDescriptors
                     
                 }
                 
                 
             }
             
         }

         fetchRequest.returnsObjectsAsFaults = false //needed to make data accessible
         
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tblSelectDoctor.isUserInteractionEnabled = true
            
            if searchBar.text == ""{
                self.fetchDoctorsList(soryBy: self.sortBySorting, roomNumber: self.filterByRoom)
                tblSelectDoctor.isHidden = false
                btnEnterDoctor.isHidden = false
                lblNotFound.text = "Doctor is not on the list."
                
            }else if results.count == 0{
                
                tblSelectDoctor.isHidden = true
                
            }else{
                subjectInformation = results
                tblSelectDoctor.isHidden = false
                btnEnterDoctor.isHidden = false
                lblNotFound.text = "Doctor is not on the list."
                print("RESUTLS: \(results)")
            }
            
        } catch {
            
            print("uh oh")
            
        }
        
        tblSelectDoctor.reloadData()
        
    } */
    
    
    func addToolBarMedicarNum(){
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        searchDoctor.inputAccessoryView = toolBar
        
        
    }

    
    func pickerDoneClicked(){
        
        searchDoctor.resignFirstResponder()
        
    }
    
    

    @IBAction func sortFilterDoctor(_ sender: Any) {
        
        let goToSortingAndFiltering = self.storyboard!.instantiateViewController(withIdentifier: "SortAndFilterDoctorsViewController") as! SortAndFilterDoctorsViewController
        let navController = UINavigationController(rootViewController: goToSortingAndFiltering)
        self.present(navController, animated:true, completion: nil)

        
    }
    
}
