//
//  ChangePasswordTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/17/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class ChangePasswordTableViewCell: UITableViewCell {
    
    @IBOutlet var edtUserName: UITextField!
    @IBOutlet var edtOldPassword: UITextField!
    @IBOutlet var edtNewPassword: UITextField!
    @IBOutlet var edtRetypePassword: UITextField!
    @IBOutlet var btnChangePassword: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
