//
//  EnterDoctorResultTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/25/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class EnterDoctorResultTableViewCell: UITableViewCell {
    
    @IBOutlet var lblDoctorName: UILabel!
    @IBOutlet var lblDoctorSpec: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
