//
//  WarningTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/17/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class WarningTableViewCell: UITableViewCell {
    
    @IBOutlet var lblWarningMessage: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
