//
//  ProblemConditionTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/3/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class ProblemConditionTableViewCell: UITableViewCell {
    
    @IBOutlet var lblProblemHeader: UILabel!
    @IBOutlet var lblProblemContent: UILabel!
    @IBOutlet var lblDueDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
