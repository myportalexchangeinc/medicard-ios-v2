//
//  SelecDoctorTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 12/28/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit

class SelecDoctorTableViewCell: UITableViewCell {
    
    @IBOutlet var lblDoctorName: UILabel!
    @IBOutlet var lblDoctorSkill: UILabel!
    @IBOutlet var lblDoctorRoom: UILabel!
    @IBOutlet var lblDocSchedule: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
