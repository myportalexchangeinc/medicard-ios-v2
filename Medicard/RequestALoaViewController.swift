//
//  RequestALoaViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 12/27/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView

class RequestALoaViewController: UIViewController {
    
    @IBOutlet var btnConsultation: UIButton!
    @IBOutlet var btnMaternity: UIButton!
    @IBOutlet var btnBasicTests: UIButton!
    @IBOutlet var btnOtherProcedure: UIButton!
    
    var specialization: [String] = []
    var specializationCode: [String] = []
    var specializationIndex: [String] = []
    
    var placeNullArray: [Int] = []
    var nullArray: [String] = []
    
    
    
    var gender: String!
    
    let defaults = UserDefaults.standard
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Request LOA"
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelRequestLOA(_:)))
        
        defaults.set("", forKey: "filterSortBy")
        defaults.set("", forKey: "filterProvince")
        defaults.set("", forKey: "filterCity")
        

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Getter_Setter.medicardClinicFirst = false
        Getter_Setter.finalMedicardClinicFirst = false
        Getter_Setter.finalChosenCityName = nullArray
        Getter_Setter.finalChosenCityCode = specialization
        Getter_Setter.finalChosenProvinceName = "All Province"
        Getter_Setter.finalChosenProvinceCode = ""
        Getter_Setter.chosenProvinceName = "All Province"
        Getter_Setter.chosenProvinceCode = ""
        Getter_Setter.chosenCity = nullArray
        Getter_Setter.finalChosenSearch = ""
        Getter_Setter.finalChosenSortBy = "category"
        
        Getter_Setter.selectedDoctorName = ""
        Getter_Setter.selectedDoctorSpec = ""
        Getter_Setter.selectedDoctorCode = ""
        Getter_Setter.selectedDoctorSched = ""
        Getter_Setter.selectedRoom = ""
        
        Getter_Setter.selectedHospitalCode = ""
        Getter_Setter.selectedHospitalName = ""
        Getter_Setter.selectedDeviceID = ""
        Getter_Setter.selectedAddress = ""
        Getter_Setter.selectedphoneNo = ""
        Getter_Setter.selectedCity = ""
        Getter_Setter.selectedProvince = ""
        Getter_Setter.selectedRegion = ""
        Getter_Setter.seletedContactPerson = ""

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func cancelRequestLOA(_ sender: AnyObject){
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func goToConsultation(_ sender: Any) {
        
        let goToSelectHospital = self.storyboard!.instantiateViewController(withIdentifier: "SelectHospitalViewController") as! SelectHospitalViewController
        defaults.set("consultation", forKey: "consultationHeader")
        defaults.set("fromHome", forKey: "selectIndicator")
        defaults.setValue("consultationLoA", forKey: "loaIndicator")
        defaults.set("", forKey: "chosenProvinceCode")
        defaults.set("Hospital/Clinic Name", forKey: "filterSortBy")
        defaults.set("All Provinces", forKey: "filterProvince")
        defaults.set("All Cities", forKey: "filterCity")
        
        //SPECIALIZATION
        defaults.set(specialization, forKey: "chosenSpecializationName")
        defaults.set(specializationCode, forKey: "chosenSpecializationCode")
        defaults.set(specializationIndex, forKey: "chosenSpecializationIndexes")
        defaults.set("Doctor's Family Name", forKey: "sortsData")
        defaults.set("docLnam", forKey: "sortingData")
        defaults.set("", forKey: "searchDoctorsName")
        defaults.set("", forKey: "doctorsRoom")
        defaults.set("", forKey: "roomNumber")
        defaults.set("docLname", forKey: "sortingData")
        defaults.set("", forKey: "filterSpecialization")
        defaults.set("", forKey: "doctorsName")
        defaults.set("", forKey: "hospitalsName")
        defaults.set(specialization, forKey: "chosenCity")
        defaults.set(placeNullArray, forKey: "chosenIndex")
        defaults.set("", forKey: "chosenProvince")
        defaults.set("", forKey: "chosenProvinceCode")
        
        navigationController?.pushViewController(goToSelectHospital, animated: true)
        
    }
    
    
    
    @IBAction func goToMaternityConsultation(_ sender: Any) {
        
        if(gender == "MALE"){
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningMaleUser, closeButtonTitle:"OK")
            
        }else{
            
            let goToSelectHospital = self.storyboard!.instantiateViewController(withIdentifier: "SelectHospitalViewController") as! SelectHospitalViewController
            defaults.set("maternity", forKey: "consultationHeader")
            defaults.set("fromHome", forKey: "selectIndicator")
            defaults.setValue("maternityLoA", forKey: "loaIndicator")
            defaults.set("", forKey: "chosenProvinceCode")
            defaults.set("Hospital/Clinic Name", forKey: "filterSortBy")
            defaults.set("All Provinces", forKey: "filterProvince")
            defaults.set("All Cities", forKey: "filterCity")
            
            //SPECIALIZATION
            defaults.set(specialization, forKey: "chosenSpecializationName")
            defaults.set(specializationCode, forKey: "chosenSpecializationCode")
            defaults.set(specializationIndex, forKey: "chosenSpecializationIndexes")
            defaults.set("Doctor's Family Name", forKey: "sortByIndicator")
            defaults.set("docLname", forKey: "sortingData")
            defaults.set("", forKey: "searchDoctorsName")
            defaults.set("", forKey: "doctorsRoom")
            defaults.set("", forKey: "roomNumber")
            defaults.set("docLname", forKey: "sortingData")
            defaults.set("", forKey: "filterSpecialization")
            defaults.set("", forKey: "doctorsName")
            defaults.set("", forKey: "hospitalsName")
            
            navigationController?.pushViewController(goToSelectHospital, animated: true)
            
        }
        
        
    }
    
    
    @IBAction func goToBasicTest(_ sender: Any) {
        
        
        
    }
    
    

    @IBAction func goToOtherProcedures(_ sender: Any) {
        
        
        
    }
    
    
    
    
    
    

}
