//
//  SortAndFilterDoctorsViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 1/30/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView

class SortAndFilterDoctorsViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var scheduleView: UIView!
    @IBOutlet var edtDoctorName: UITextField!
    @IBOutlet var edtSortBy: UITextField!
    @IBOutlet var edtSpecialization: UITextField!
    @IBOutlet var edtRoom: UITextField!
    @IBOutlet var btnResetFilter: UIButton!
    @IBOutlet var edtShowResult: UIButton!
    var specialization: [String] = []
    var specializationCode: [String] = []
    var specializationIndex: [String] = []
    
    let defaults = UserDefaults.standard
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Sort and Filter Doctor"
        
        
        scheduleView.layer.borderWidth = 1
        scheduleView.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor

        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelSortingAndFiltering(_:)))

        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }

    
    
    func cancelSortingAndFiltering(_ sender: AnyObject){
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("CHOSEN SPECIALIZATION NAME: \(defaults.value(forKey: "chosenSpecializationName"))")
        print("CHOSEN SPECIALIZATION CODE: \(defaults.value(forKey: "chosenSpecializationCode"))")
        print("CHOSEN SPECIALIZATION INDEXES: \(defaults.value(forKey: "chosenSpecializationIndexes"))")
        
        
        let specializationName = defaults.value(forKey: "chosenSpecializationName") as? [String] ?? [String]()
        
        if specializationName.count == 0{
            
           
            edtSpecialization.text = "All Specializations"
            
        }else{
            
            let str = String(describing: specializationName)
            edtSpecialization.text = removeSpecialCharsFromString(str)
            
        }
        
        let dataSort = defaults.value(forKey: "sortsData") as? String
        
        if dataSort == ""{
            
            edtSortBy.text = "Doctor's Family Name"
            self.defaults.set("docLname", forKey: "sortingData")
            
        }else{
            
            edtSortBy.text = defaults.value(forKey: "sortsData") as? String
            
        }
        
        edtRoom.text = defaults.value(forKey: "room") as? String
        edtDoctorName.text = defaults.value(forKey: "doctorsName") as? String
        
       
        

    }
    
    
    func removeSpecialCharsFromString(_ text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == edtSortBy{
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            showAlertSortBy()
            
            return false
            
        }else if textField == edtDoctorName{
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            
            return true
            
        }else if textField == edtSpecialization{
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            defaults.set(edtRoom.text, forKey: "room")
            defaults.set(edtSpecialization.text, forKey: "filterSpecialization")
            defaults.set(edtDoctorName.text, forKey: "doctorsName")
            
            let goToChooseSpecialization = self.storyboard!.instantiateViewController(withIdentifier: "ChooseSpecializationViewController") as! ChooseSpecializationViewController
            self.navigationController?.pushViewController(goToChooseSpecialization, animated: true)
            
            return false
            
        }else{
            
            self.view.endEditing(true)
            
            return true
        }

        
    }
    
    
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        edtSortBy.resignFirstResponder()
        edtRoom.resignFirstResponder()
        edtDoctorName.resignFirstResponder()
        edtSpecialization.resignFirstResponder()
        
        return true
        
    }
    
    @IBAction func resetFilterAction(_ sender: Any) {
        
        //SPECIALIZATION
        defaults.set(specialization, forKey: "chosenSpecializationName")
        defaults.set(specializationCode, forKey: "chosenSpecializationCode")
        defaults.set(specializationIndex, forKey: "chosenSpecializationIndexes")
        defaults.set("Doctor's Family Name", forKey: "sortByIndicator")
        defaults.set("docLname", forKey: "sortingData")
        defaults.set("", forKey: "searchDoctorsName")
        defaults.set("", forKey: "doctorsRoom")
        defaults.set("", forKey: "roomNumber")
        defaults.set("docLname", forKey: "sortingData")
        defaults.set("", forKey: "filterSpecialization")
        defaults.set("", forKey: "doctorsName")
        
        
        edtSortBy.text = "Doctor's Family Name"
        edtSpecialization.text = "All Specializations"
        edtRoom.text = ""
        edtDoctorName.text = ""
        
    }

    
    @IBAction func showResultFilter(_ sender: Any) {
        
        defaults.set(edtRoom.text, forKey: "room")
        defaults.set(edtSpecialization.text, forKey: "filterSpecialization")
        defaults.set(edtDoctorName.text, forKey: "doctorsName")
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func showAlertSortBy(){
        
        let alert = SCLAlertView()
        let icon = UIImage(named:"ic_sort_image.png")
        alert.addButton("Doctor's Family Name"){
            
            self.edtSortBy.text = "Doctor's Family Name"
            self.defaults.set("docLname", forKey: "sortingData")
            self.defaults.set("Doctor's Family Name", forKey: "sortsData")
        }
        
        alert.addButton("Room") {
            
            self.edtSortBy.text = "Room Number"
            self.defaults.set("room", forKey: "sortingData")
            self.defaults.set("Room Number", forKey: "sortsData")
        }
        
        alert.addButton("Specialization") {
            
            self.edtSortBy.text = "Specialization"
            self.defaults.set("specDesc", forKey: "sortingData")
            self.defaults.set("Specialization", forKey: "sortsData")
        }
        
        
        alert.showCustom("Sort By", subTitle: "", color: UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0), icon: icon!)
        
    }

    
    

    
    

}
