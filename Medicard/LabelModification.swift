//
//  LabelModification.swift
//  Medicard
//
//  Created by Al John Albuera on 2/21/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class LabelModification: UILabel {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
        
    }
    
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 3, bottom: 0, right: 3)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }

}
