//
//  OkayButtonTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/3/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class OkayButtonTableViewCell: UITableViewCell {
    
    @IBOutlet var lblDueDate: UILabel!
    @IBOutlet var btnOkay: UIButton!
    @IBOutlet var btnDownloadLoaForm: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
