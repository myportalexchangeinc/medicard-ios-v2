//
//  RequestLoaDisapprovedViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 2/1/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class RequestLoaDisapprovedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var tblDisapproved: UITableView!
    
    var hospName: String!
    var hospAddress: String!
    var phoneNumbers: String!
    var contactPerson: String!
    var clinicHours: String!
    var province: String!
    var city: String!
    var region: String!
    var docName: String!
    var docSpec: String!
    var doctorRoom: String!
    var doctorSched: String!
    
    let defaults = UserDefaults.standard
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let header = defaults.string(forKey: "consultationHeader")! as String;
        
        if header == "consultation" {
            self.title = "Consultation"
        }else{
            self.title = "Maternity Consultation"
        }
        
        tblDisapproved.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        tblDisapproved.rowHeight = UITableViewAutomaticDimension
        tblDisapproved.estimatedRowHeight = 246; //Set this to any value that works for you.

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        hospName = defaults.value(forKey: "hospitalName") as! String!
        hospAddress = defaults.value(forKey: "streetAddress") as! String!
        phoneNumbers = defaults.value(forKey: "phoneNo") as! String!
        contactPerson = defaults.value(forKey: "contactPerson") as! String!
        clinicHours = defaults.value(forKey: "clinicHours") as! String!
        city = defaults.value(forKey: "city") as! String!
        province = defaults.value(forKey: "province") as! String!
        region = defaults.value(forKey: "region") as! String!
        doctorRoom = defaults.value(forKey: "room") as! String!
        doctorSched = defaults.value(forKey: "schedule") as! String!
        docName = defaults.value(forKey: "doctorName") as! String!
        docSpec = defaults.value(forKey: "doctorSpec") as! String!
        doctorRoom = defaults.value(forKey: "room") as! String!
        doctorSched = defaults.value(forKey: "schedule") as! String!
        
        tblDisapproved.reloadData()
        
        self.navigationItem.hidesBackButton = true;

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            
            return 1
            
        }else if section == 1{
            
            return 1
            
        }else if section == 2{
            
            return 1
            
        }else if section == 3{
            
            return 1
            
        }else{
            
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cell:RequestLoaResultFirstTableViewCell! = tblDisapproved.dequeueReusableCell(withIdentifier: "firstCell", for: indexPath) as! RequestLoaResultFirstTableViewCell
            
            cell.lblMemberName.text = defaults.value(forKey: "memberName") as! String?
            cell.lblMemberCode.text = defaults.value(forKey: "memberCode") as! String?
            cell.lblAge.text = defaults.value(forKey: "memberAge") as! String?
            
            if defaults.value(forKey: "memberGender") as! String? == "0" {
                
                cell.lblGender.text = "FEMALE"
                
            }else{
                
                cell.lblGender.text = "FEMALE"
                
            }
            
            cell.lblCompanyName.text = defaults.value(forKey: "companyName") as! String?
            cell.lblRemarks.text = defaults.value(forKey: "remarks") as! String?
            
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.section == 1{
            
            let cell:HospitalRequestDetailsTableViewCell! = tblDisapproved.dequeueReusableCell(withIdentifier: "HospitalCell", for: indexPath) as! HospitalRequestDetailsTableViewCell
            
            
            cell.lblHospitalName.text = hospName
            cell.lblHospitalAddress.text = hospAddress + ", " + city + ", " + province + ", " + region
            cell.lblContactPerson.text = contactPerson
            cell.lblPhoneNumbers.text = phoneNumbers
            cell.lblClinicHours.text = clinicHours
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.section == 2{
            
            
            let cell:DoctorRequestDetailsTableViewCell! = tblDisapproved.dequeueReusableCell(withIdentifier: "DoctorCell", for: indexPath) as! DoctorRequestDetailsTableViewCell
            
            cell.lblDoctorName.text = docName
            cell.lblDoctorSkill.text = docSpec
            cell.lblRoom.text = doctorRoom
            cell.lblClinicHours.text = doctorSched
            
            
            cell.selectionStyle = .none
            
            return cell
            
            
        }else if indexPath.section == 3{
            
            
            let cell:ProblemConditionTableViewCell! = tblDisapproved.dequeueReusableCell(withIdentifier: "fourthCell", for: indexPath) as! ProblemConditionTableViewCell
            
            cell.lblProblemContent.text = defaults.value(forKey: "problemCondition") as! String?
            
            cell.lblProblemContent.sizeToFit()
            
            cell.selectionStyle = .none
            
            return cell
            
        }else{
            
            
            let cell:OkayButtonTableViewCell! = tblDisapproved.dequeueReusableCell(withIdentifier: "fifthCell", for: indexPath) as! OkayButtonTableViewCell
            
            cell.btnOkay.addTarget(self, action: #selector(self.okayAction(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            
            return cell
            
        }
        
    }
    
    
    @IBAction func okayAction(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    

}
