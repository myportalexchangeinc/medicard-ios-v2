//
//  Getter_Setter.swift
//  Medicard
//
//  Created by Al John Albuera on 2/2/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class Getter_Setter: NSObject {
    //Before Final Data
    open static var chosenHospitalName: [String] = []
    open static var chosenHospitalIndex: [Int] = []
    open static var chosenDoctorName: [String] = []
    open static var chosenDoctorIndex: [Int] = []
    open static var chosenSortBy: String!
    open static var chosenSearch: String!
    open static var chosenStatus: String!
    open static var chosenServiceType: String!
    open static var chosenRequestDateFrom: String!
    open static var chosenRequestDateTo: String!
    
    open static var chooseHospitalClininc: String!
    open static var chooseDoctor: String!
    
    //Final Data
    open static var finalChosenHospitalName: [String] = []
    open static var finalChosenHospitalIndexes: [Int] = []
    open static var finalChosenDoctorName: [String] = []
    open static var finalChosenDoctorIndexes: [Int] = []
    open static var finalChosenSortBy: String!
    open static var finalChosenSearch: String!
    open static var finalChosenStatus: String!
    open static var finalChosenServiceType: String!
    open static var finalChosenRequestDateFrom: String!
    open static var finalChosenRequestDateTo: String!
    
    open static var sortingAndFilterIndicator: Int!
    open static var cancelRequestStatus: Int!
    
    
    open static var chosenProvinceName: String!
    open static var chosenProvinceCode: String!
    open static var chosenCityName: [String] = []
    open static var chosenCityCode: [String] = []
    
    
    open static var finalChosenProvinceName: String!
    open static var finalChosenProvinceCode: String!
    open static var finalChosenCityName: [String] = []
    open static var finalChosenCityCode: [String] = []
    open static var finalChosenProvinceNameArray: [String] = []
    open static var finalChosenProvinceCodeArray: [String] = []
    open static var chosenProvinceNameArray: [String] = []
    open static var chosenProvinceCodeArray: [String] = []
    
    
    
    open static var dateFrom: NSDate!
    open static var finalDateFrom: NSDate!
    open static var dateTo: NSDate!
    open static var finalDateTo: NSDate!
    open static var finalMinusDateFrom: NSDate!
    open static var finalMinusDateTo: NSDate!
    
    open static var medicardClinicFirst: Bool!
    open static var finalMedicardClinicFirst: Bool!
    
    open static var chosenProvince: String!
    open static var chosenCity: [String] = []
    
    
    open static var selectedHospitalCode: String!
    open static var selectedDeviceID: String!
    open static var selectedHospitalName: String!
    open static var selectedAddress: String!
    open static var selectedphoneNo: String!
    open static var selectedCity: String!
    open static var selectedProvince: String!
    open static var selectedRegion: String!
    open static var seletedContactPerson: String!
    
    open static var selectedDoctorName: String!
    open static var selectedDoctorCode: String!
    open static var selectedDoctorSpec: String!
    open static var selectedRoom: String!
    open static var selectedDoctorSched: String!
    
    open static var arraysOfCity: NSMutableArray!
    open static var finalArraysOfCity: NSMutableArray!
    


}
