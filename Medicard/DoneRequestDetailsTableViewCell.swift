//
//  DoneRequestDetailsTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/25/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class DoneRequestDetailsTableViewCell: UITableViewCell {
    
    
    @IBOutlet var tvConditions: UITextView!
    @IBOutlet var lblDueDate: UILabel!
    @IBOutlet var btnCheckBox: UIButton!
    @IBOutlet var btnTermsAndCondition: UIButton!
    @IBOutlet var btnCancelTransaction: UIButton!
    @IBOutlet var btnSubmitTransaction: UIButton!
    
    
    @IBOutlet var btnCancelRequest: UIButton!
    @IBOutlet var btnGoBack: UIButton!
    @IBOutlet var btnDownloadLoa: UIButton!
    @IBOutlet var lblReqValidationDate: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
