//
//  SortFilterLoaHistoryViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 2/21/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import CoreData

class SortFilterLoaHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet var tblMultiSelectFilter: UITableView!
    var filterTypes: String!
    var subjectInformation = [NSManagedObject]()
    var sortingNames: [String] = []
    var filteredSortingNames: [String] = []
    //Doctors
    var filterChosenDoctorsName: [String] = []
    var filterChosenDoctorsIndexes: [Int] = []
    //Hospitals
    var filterChosenHospitalName: [String] = []
    var filterChosenHospitalIndexes: [Int] = []
    
    var cellStatus:NSMutableDictionary = NSMutableDictionary();

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Sort and Filter"
        
        tblMultiSelectFilter.separatorStyle = .none
        
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelSortingAndFiltering(_:)))
        
        let backButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(saveChosenItem(_:)))
        
        self.navigationItem.rightBarButtonItem = backButton

        
        
        if filterTypes == "hospital"{
            
            fetchHospitalFromLoaList()
            
        }else if filterTypes == "doctor"{
            
            fetchDoctorFromLoaList()
            
        }else if filterTypes == "tests"{
            
           fetchTestsFromLoaList()
            
        }else{
            
            fetchDiagnosisFromLoaList()
            
        }
        
        tblMultiSelectFilter.rowHeight = UITableViewAutomaticDimension
        tblMultiSelectFilter.estimatedRowHeight = 50;
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if filterTypes == "hospital"{
            
            filterChosenHospitalName = Getter_Setter.chosenHospitalName
            filterChosenHospitalIndexes = Getter_Setter.chosenHospitalIndex
            
            if Getter_Setter.chosenHospitalIndex.count == 0{
                
            }else{
                
                for item in Getter_Setter.chosenHospitalIndex {
                    print("Found \(item)")
                    
                    let indexPathForFirstRow = IndexPath(row: item, section: 0)
                    tblMultiSelectFilter.selectRow(at: indexPathForFirstRow, animated: true, scrollPosition: .none)
                    
                }
                
            }

            
        }else if filterTypes == "doctor"{
            
            filterChosenDoctorsName = Getter_Setter.chosenDoctorName
            filterChosenDoctorsIndexes = Getter_Setter.chosenDoctorIndex
            
            if Getter_Setter.chosenDoctorIndex.count == 0{
                
            }else{
                
                for item in Getter_Setter.chosenDoctorIndex {
                    print("Found \(item)")
                    
                    let indexPathForFirstRow = IndexPath(row: item, section: 0)
                    tblMultiSelectFilter.selectRow(at: indexPathForFirstRow, animated: true, scrollPosition: .none)
                    
                }
                
            }
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSortingNames.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MultipleSelectionLoaTableViewCell! = tblMultiSelectFilter.dequeueReusableCell(withIdentifier: "multipleSelectionCell", for: indexPath) as! MultipleSelectionLoaTableViewCell
        
        
        if filterTypes == "hospital"{
            
            cell.lblMultipleSelection.text = filteredSortingNames[indexPath.row]
            
        }else if filterTypes == "doctor"{
            
            cell.lblMultipleSelection.text = filteredSortingNames[indexPath.row]
            
        }else if filterTypes == "tests"{
            
            cell.lblMultipleSelection.text = "Tests"
            
        }else{
            
            cell.lblMultipleSelection.text = "Diagnosis"
            
        }
        
        
        
        cell.selectionStyle = .none
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tblMultiSelectFilter.cellForRow(at: indexPath) as! MultipleSelectionLoaTableViewCell
        
        if filterTypes == "hospital"{
            
            cell.setSelected(true, animated: true)
            filterChosenHospitalName.append(cell.lblMultipleSelection.text!)
            filterChosenHospitalIndexes.append(indexPath.row)
            self.cellStatus[indexPath.row] = true;
            
            
        }else if filterTypes == "doctor"{
            
            cell.setSelected(true, animated: true)
            filterChosenDoctorsName.append(cell.lblMultipleSelection.text!)
            filterChosenDoctorsIndexes.append(indexPath.row)
            self.cellStatus[indexPath.row] = true;
            
        }else if filterTypes == "tests"{
            
            cell.setSelected(true, animated: true)
            
        }else{
            
            cell.setSelected(true, animated: true)
            
        }
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tblMultiSelectFilter.cellForRow(at: indexPath) as! MultipleSelectionLoaTableViewCell
        
        
        if filterTypes == "hospital"{
            
            
            if let index = filterChosenHospitalName.index(of: cell.lblMultipleSelection.text!) {
                filterChosenHospitalName.remove(at: index)
                
            }
            
            if let index = filterChosenHospitalIndexes.index(of: indexPath.row) {
                filterChosenHospitalIndexes.remove(at: index)
                
            }

             cell.setSelected(false, animated: true)
            self.cellStatus[indexPath.row] = false;
            
        }else if filterTypes == "doctor"{
            
            if let index = filterChosenDoctorsName.index(of: cell.lblMultipleSelection.text!) {
                filterChosenDoctorsName.remove(at: index)
                
            }
            
            if let index = filterChosenDoctorsIndexes.index(of: indexPath.row) {
                filterChosenDoctorsIndexes.remove(at: index)
                
            }
            
             cell.setSelected(false, animated: true)
            self.cellStatus[indexPath.row] = false;
            
        }else if filterTypes == "tests"{
            
             cell.setSelected(false, animated: true)
            
        }else{
            
             cell.setSelected(false, animated: true)
            
        }

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    func cancelSortingAndFiltering(_ sender: AnyObject){
        
        
        self.navigationController?.popViewController(animated: true)
        
        Getter_Setter.sortingAndFilterIndicator = 0
        
    }
    
    
    func saveChosenItem(_ sender: AnyObject){
        
        if filterTypes == "hospital"{
            
            Getter_Setter.chosenHospitalName = filterChosenHospitalName
            Getter_Setter.chosenHospitalIndex = filterChosenHospitalIndexes
            
        }else if filterTypes == "doctor"{
            
            Getter_Setter.chosenDoctorName = filterChosenDoctorsName
            Getter_Setter.chosenDoctorIndex = filterChosenDoctorsIndexes
           
        }
        
        Getter_Setter.sortingAndFilterIndicator = 1
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func fetchDoctorFromLoaList(){
    
            
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "LoaHistoryEntity")
        let sortDescriptor = NSSortDescriptor(key: "doctorName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            subjectInformation = results
            
            for itemDoctorName in subjectInformation{
                
                sortingNames.append(itemDoctorName.value(forKey: "doctorName") as! String)
                
            }
            
            let unique = Array(Set(sortingNames))
            filteredSortingNames = unique.sorted(){ $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        
    }
    
    
    func fetchHospitalFromLoaList(){
        
       
       let appDelegate =
           UIApplication.shared.delegate as! AppDelegate
       
       let managedContext = appDelegate.managedObjectContext
       
       let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "LoaHistoryEntity")
       let sortDescriptor = NSSortDescriptor(key: "hospitalName", ascending: true)
       fetchRequest.sortDescriptors = [sortDescriptor]
       
       
       do {
           let results = try managedContext.fetch(fetchRequest)
           subjectInformation = results
           
           for itemDoctorName in subjectInformation{
               
               sortingNames.append(itemDoctorName.value(forKey: "hospitalName") as! String)
               
               
           }
           
           let unique = Array(Set(sortingNames))
           filteredSortingNames = unique.sorted(){ $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
           
           
       } catch let error as NSError {
           print("Could not fetch. \(error), \(error.userInfo)")
       }

    }
    
    
    func fetchDiagnosisFromLoaList(){
        
        
    }
    
    func fetchTestsFromLoaList(){
        
        
        
    }



}
