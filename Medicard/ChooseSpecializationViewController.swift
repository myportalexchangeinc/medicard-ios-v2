//
//  ChooseSpecializationViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 1/30/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import CoreData

class ChooseSpecializationViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate {
    
    
    @IBOutlet var collectionSpecialization: UICollectionView!
    var subjectInformation = [NSManagedObject]()
    var chosenSpecializationName: [String] = []
    var chosenSpecializationCode: [String] = []
    var chosenSpecializationIndexes: [Int] = []
    var cellStatus:NSMutableDictionary = NSMutableDictionary();
    var searchActive : Bool = false
    
    let defaults = UserDefaults.standard
    
    @IBOutlet var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Specialization"
        
        let backButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(saveSpecialization(_:)))
        
        self.navigationItem.rightBarButtonItem = backButton
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelSpecicalization(_:)))


        collectionSpecialization.allowsMultipleSelection = true
        
        addToolBarMedicarNum()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        chosenSpecializationIndexes = defaults.value(forKey: "chosenSpecializationIndexes") as? [Int] ?? [Int]()
        chosenSpecializationName = defaults.value(forKey: "chosenSpecializationName") as? [String] ?? [String]()
        chosenSpecializationCode = defaults.value(forKey: "chosenSpecializationCode") as? [String] ?? [String]()
        
        fetchSpecialization()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return subjectInformation.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "choosePlaceCell", for: indexPath as IndexPath) as! ChoosePlaceCollectionViewCell

        let specialization = subjectInformation[(indexPath as NSIndexPath).row]
        let specializationName = specialization.value(forKey: "specializationDescription") as! String
        
        
        
        cell.lblPlaces.text = specializationName
        
        cell.lblPlaces.layer.borderWidth = 1
        cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
        cell.lblPlaces.layer.cornerRadius = 2
        cell.lblPlaces.backgroundColor = UIColor.clear
        cell.lblPlaces.textColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)

        
        
        if chosenSpecializationName.count != 0{
            
            for i in 0..<chosenSpecializationName.count{
                
                if specializationName == chosenSpecializationName[i]{
                    
                    cell.lblPlaces.backgroundColor =  UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                    cell.lblPlaces.textColor = UIColor.white
                    cell.lblPlaces.layer.borderWidth = 1
                    cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                    cell.lblPlaces.layer.cornerRadius = 2
                    
                    break
                    
                }else{
                    
                    cell.lblPlaces.layer.borderWidth = 1
                    cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
                    cell.lblPlaces.layer.cornerRadius = 2
                    cell.lblPlaces.backgroundColor = UIColor.clear
                    cell.lblPlaces.textColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
                    
                }
                
            }
            
        }
        
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let cell = collectionView.cellForItem(at: indexPath) as! ChoosePlaceCollectionViewCell
        
        if cell.lblPlaces.textColor == UIColor.white{
            
            let specialization = subjectInformation[(indexPath as NSIndexPath).row]
            let specializationName = specialization.value(forKey: "specializationDescription") as! String
            let specializationCode = specialization.value(forKey: "specializationCode") as! String
            
            cell.lblPlaces.layer.borderWidth = 1
            cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
            cell.lblPlaces.layer.cornerRadius = 2
            cell.lblPlaces.backgroundColor = UIColor.clear
            cell.lblPlaces.textColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
            
            if let index = chosenSpecializationName.index(of: specializationName) {
                chosenSpecializationName.remove(at: index)
                
                print("REMAINING ITEMS: \(chosenSpecializationName)")
            }
            
            if let index = chosenSpecializationCode.index(of: specializationCode) {
                chosenSpecializationCode.remove(at: index)
                
                print("REMAINING ITEMS: \(chosenSpecializationCode)")
            }
            
            if let index = chosenSpecializationIndexes.index(of: indexPath.row) {
                chosenSpecializationIndexes.remove(at: index)
                
                print("REMAINING ITEMS: \(chosenSpecializationIndexes)")
            }

            
        }else{
            
            let specialization = subjectInformation[(indexPath as NSIndexPath).row]
            let specializationName = specialization.value(forKey: "specializationDescription") as! String
            let specializationCode = specialization.value(forKey: "specializationCode") as! String
            
            cell.lblPlaces.backgroundColor =  UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0)
            cell.lblPlaces.textColor = UIColor.white
            cell.lblPlaces.layer.borderWidth = 1
            cell.lblPlaces.layer.borderColor = UIColor(red:57/255.0, green:67/255.0, blue:172/255.0, alpha: 1.0).cgColor
            cell.lblPlaces.layer.cornerRadius = 2
            
            
            chosenSpecializationName.append(specializationName)
            chosenSpecializationIndexes.append(indexPath.row)
            chosenSpecializationCode.append(specializationCode)
            
        }
   
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! ChoosePlaceCollectionViewCell
        cell.isSelected = false;
        self.cellStatus[indexPath.row] = false;
        
        let specialization = subjectInformation[(indexPath as NSIndexPath).row]
        let specializationName = specialization.value(forKey: "specializationDescription") as! String
        let specializationCode = specialization.value(forKey: "specializationCode") as! String
        
        if let index = chosenSpecializationName.index(of: specializationName) {
            chosenSpecializationName.remove(at: index)
            
            print("REMAINING ITEMS: \(chosenSpecializationName)")
        }
        
        if let index = chosenSpecializationCode.index(of: specializationCode) {
            chosenSpecializationCode.remove(at: index)
            
            print("REMAINING ITEMS: \(chosenSpecializationCode)")
        }
        
        if let index = chosenSpecializationIndexes.index(of: indexPath.row) {
            chosenSpecializationIndexes.remove(at: index)
            
            print("REMAINING ITEMS: \(chosenSpecializationIndexes)")
        }

    }
    
    
    
    func fetchSpecialization(){

       let appDelegate =
           UIApplication.shared.delegate as! AppDelegate
       
       let managedContext = appDelegate.managedObjectContext
       
       let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SpecializationEntity")
       let sortDescriptor = NSSortDescriptor(key: "specializationDescription", ascending: true)
       fetchRequest.sortDescriptors = [sortDescriptor]
       
       do {
           let results = try managedContext.fetch(fetchRequest)
           subjectInformation = results
           
           
           collectionSpecialization.reloadData()
           
           MBProgressHUD.hide(for: self.view, animated: true)
           
       } catch let error as NSError {
           print("Could not fetch. \(error), \(error.userInfo)")
       }

        
    }
    
    
     func saveSpecialization(_ sender: AnyObject){
        
        defaults.set(chosenSpecializationName, forKey: "chosenSpecializationName")
        defaults.set(chosenSpecializationCode, forKey: "chosenSpecializationCode")
        defaults.set(chosenSpecializationIndexes, forKey: "chosenSpecializationIndexes")

        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func cancelSpecicalization(_ sender: AnyObject){
        
        self.navigationController?.popViewController(animated: true)
        
    }
 
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SpecializationEntity")
        let managedContext2 = appDelegate.managedObjectContext
        let fetchRequest2 = NSFetchRequest<NSManagedObject>(entityName: "SpecializationEntity")
        
        let predicate1 = NSPredicate(format: "(specializationDescription BEGINSWITH [c] %@)", searchBar.text!)
        let motherPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate1])
        fetchRequest.predicate = motherPredicate
        
        let predicate2 = NSPredicate(format: "(specializationDescription CONTAINS [c] %@)", " " + searchBar.text!)
        let motherPredicate2 = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate2])
        fetchRequest2.predicate = motherPredicate2

        
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest2.returnsObjectsAsFaults = false //needed to make data accessible
        
        do {
            
            let results = try managedContext.fetch(fetchRequest)
            let results2 = try managedContext2.fetch(fetchRequest2)
            
            if searchBar.text == ""{
                
                fetchSpecialization()
                collectionSpecialization.isHidden = false
                
            }else if results.count == 0{
                
                collectionSpecialization.isHidden = true
                
            }else{
                subjectInformation = results + results2
                collectionSpecialization.isHidden = false
            }
            
            
        } catch {
            
            print("uh oh")
            
        }
        
        collectionSpecialization.reloadData()
        
    }
    
    func addToolBarMedicarNum(){
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        searchBar.inputAccessoryView = toolBar
        
        
    }
    
    func pickerDoneClicked(){
        
        searchBar.resignFirstResponder()
        
    }

}
