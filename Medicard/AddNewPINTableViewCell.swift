//
//  AddNewPINTableViewCell.swift
//  Medicard
//
//  Created by Al John Albuera on 1/17/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit

class AddNewPINTableViewCell: UITableViewCell {
    
    @IBOutlet var edtPin: UITextField!
    @IBOutlet var edtReTypePin: UITextField!
    @IBOutlet var btnAddPin: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
