//
//  ChangePassViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 11/23/16.
//  Copyright © 2016 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView
import Alamofire
import SwiftyJSON
import ReachabilitySwift

class ChangePassViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var revealButton: UIBarButtonItem!
    
    var forceChangePassword: String!
    var memberCode: String!
    
    var forceChangePass: String!
    let defaults = UserDefaults.standard
    
    var showIndicator: Int!
    var showPinIndicator: Int!
    var showSavePinOrRegisterPin: Int!
    let reachability = Reachability()
    
    
    @IBOutlet var tblAccountSettings: UITableView!
    
    
    
    let validateForm = FormValidation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        forceChangePass = defaults.value(forKey: "forceChangePassword") as! String!
        print("FORCE CHANGE PASS: " + forceChangePass!)
        
        memberCode = defaults.string(forKey: "storedUsername")! as String
        
        
        if revealViewController() != nil {
            
            //revealViewController().rightViewRevealWidth = 150
            revealButton.target = revealViewController()
            revealButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
            let tapPressGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            revealButton.customView?.addGestureRecognizer(tapPressGesture)
            
        }
        
        tblAccountSettings.separatorStyle = .none
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let buttonPosition = (textField as AnyObject).convert(CGPoint.zero, to: tblAccountSettings)
        var indexPath = tblAccountSettings.indexPathForRow(at: buttonPosition)!
        
        if indexPath.section == 0{
            
            tblAccountSettings.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
            
        }else if indexPath.section == 1{
            
            
            tblAccountSettings.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(tableViewHeight() - 300))
            
            
        }else if indexPath.section == 2{
            
            tblAccountSettings.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(tableViewHeight() - 300))
            
        }
    
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("TAGS: \(textField.tag)")
        
        if self.showSavePinOrRegisterPin == 0{
             let indexPath = IndexPath(row: 0, section: 1)
             let cell = (self.tblAccountSettings.cellForRow(at: indexPath)! as! AddNewPINTableViewCell)
            
            
            if textField == cell.edtPin || textField == cell.edtReTypePin{
                
                let maxLength = 4
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                
                return newString.length <= maxLength

            }
            
        }else{
             let indexPath = IndexPath(row: 0, section: 2)
             let cell = (self.tblAccountSettings.cellForRow(at: indexPath)! as! ChangeOldPinTableViewCell)
            
            if textField == cell.edtOldPin || textField == cell.edtNewPin || textField == cell.edtReTypePin{
                
                let maxLength = 4
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                
                return newString.length <= maxLength
                
            }
            
        }
        
        
        return true
        
        
    }
    
    func tableViewHeight() -> CGFloat {
        tblAccountSettings.layoutIfNeeded()
        
        return tblAccountSettings.contentSize.height
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("PIN NUMBER: \(defaults.value(forKey: "pinNumber")!)")
        
        if forceChangePass != nil{
            
            if forceChangePass == "1" {
                
                self.title = "Change Password"
                self.navigationItem.rightBarButtonItem?.image = nil;
                
                _ = SCLAlertView().showWarning("", subTitle:"You need to change your password.", closeButtonTitle:"OK")
                
                showIndicator = 1
                
                
            }else{
                
                self.title = "Account Settings"
                self.navigationItem.leftBarButtonItem?.isEnabled = false;
                self.navigationItem.leftBarButtonItem?.image = nil;
                
                showIndicator = 0
                
                
                if defaults.value(forKey: "pinNumber") as! String == ""{
                    showSavePinOrRegisterPin = 0
                }else{
                    showSavePinOrRegisterPin = 1
                }
                
                tblAccountSettings.reloadData()
                
                
            }
            
        }
        
    }
    
    
    func dismissKeyboard(){
        
        
        print("OPEN!")
        
    }
    
    func closeChangePassword(){
        
        self.dismiss(animated: true, completion: nil);
        
    }
    
    func changeYourPassword(_ username: String, newPassword: String, oldPassword: String){
        
        let changePassword = ["username": username, "newPassword": newPassword, "oldPassword": oldPassword] as [String : Any]
        
        print("CHANGE PASSWORD DATA:\(changePassword)")
        
        Alamofire.request(PostRouter.postChangePassword(changePassword as [String : AnyObject]))
            .responseJSON { response in
                if response.result.isFailure == true {
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblAccountSettings.isUserInteractionEnabled = true
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    let post = JSON(value)
                    
                    print("JSON DATA ADD ACCOUNT: " + post.description)
                    
                    
                    if post["responseCode"].stringValue == "200"{
                        
                        if self.forceChangePass != nil{
                            
                            let indexPath = IndexPath(row: 0, section: 0)
                            let cell = (self.tblAccountSettings.cellForRow(at: indexPath)! as! ChangePasswordTableViewCell)
                            
                            if self.forceChangePass == "1" {
                                
                                cell.edtOldPassword.text = ""
                                cell.edtNewPassword.text = ""
                                cell.edtRetypePassword.text = ""
                                cell.edtUserName.becomeFirstResponder()
                                
                                self.defaults.set("0", forKey: "forceChangePassword")
                                
                                _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.sucessChangePassword, closeButtonTitle:"OK")
                                
                                self.dismiss(animated: true, completion: nil)
                                
                            }else{
                                
                                cell.edtOldPassword.text = ""
                                cell.edtNewPassword.text = ""
                                cell.edtRetypePassword.text = ""
                                cell.edtUserName.becomeFirstResponder()
                                
                                self.defaults.set("0", forKey: "forceChangePassword")
                                self.defaults.set(cell.edtUserName.text, forKey: "storedUsername")
                                self.defaults.set(cell.edtNewPassword.text, forKey: "storedPassword")
                                
                                _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.sucessChangePassword, closeButtonTitle:"OK")
                                
                            }
                            
                        }
                        
                        
                    }else if post["responseCode"].stringValue == "230"{
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningInvalidUsernamePassword, closeButtonTitle:"OK")
                        
                    }
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblAccountSettings.isUserInteractionEnabled = true
                    
                }else{
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorChangePassword, closeButtonTitle:"OK")
                    
                }
                
            }
        
    }
    
    
    func goBackToLogin(){
        
        let defaults = UserDefaults.standard
        defaults.set("0", forKey: "loginStatus")
        defaults.set("", forKey: "username")
        defaults.set("", forKey: "password")
        defaults.set("", forKey: "storedUsername")
        defaults.set("", forKey: "storedPassword")
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        viewController.modalTransitionStyle = .crossDissolve
        self.present(viewController, animated:true, completion: nil)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
     
       if showIndicator == 1{
     
           if section == 0{
     
     
               return 1
     
           }else if section == 1{
     
     
               return 0
     
     
           }else{
     
     
               return 0
           }
     
     
       }else{
        
        
            if showSavePinOrRegisterPin == 0{
            
                if section == 0{
                    
                    
                    return 1
                    
                }else if section == 1{
                    
                    
                    return 1
                    
                    
                }else{
                    
                    
                    return 0
                }
            
            }else{
            
                if section == 0{
                    
                    
                    return 1
                    
                }else if section == 1{
                    
                    
                    return 0
                    
                    
                }else{
                    
                    
                    return 1
                }
            
            }
       
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0{
            
            
            let cell:ChangePasswordTableViewCell! = tblAccountSettings.dequeueReusableCell(withIdentifier: "changePasswordCell", for: indexPath) as! ChangePasswordTableViewCell
            
            cell.btnChangePassword.addTarget(self, action: #selector(changePassworAction(_:)), for: .touchUpInside)
            
            cell.btnChangePassword.layer.cornerRadius = 2
            cell.edtUserName.text = memberCode
            
            cell.selectionStyle = .none
            

            
            

            
            return cell
            
        }else if indexPath.section == 1{
            
            
            let cell:AddNewPINTableViewCell! = tblAccountSettings.dequeueReusableCell(withIdentifier: "addNewPinCell", for: indexPath) as! AddNewPINTableViewCell
            
            cell.btnAddPin.addTarget(self, action: #selector(addNewPin(_:)), for: .touchUpInside)
            
             cell.btnAddPin.layer.cornerRadius = 2
            
            cell.selectionStyle = .none
            
            addToolBarContactNum(textField: cell.edtPin)
            addToolBarContactNum(textField: cell.edtReTypePin)
            
            return cell
            
            
        }else{
            
            let cell:ChangeOldPinTableViewCell! = tblAccountSettings.dequeueReusableCell(withIdentifier: "changePinCell", for: indexPath) as! ChangeOldPinTableViewCell
            
            
             cell.btnChangePin.addTarget(self, action: #selector(changeOldPin(_:)), for: .touchUpInside)
             cell.btnChangePin.layer.cornerRadius = 2
             cell.lblCurrentPin.text = "Your current PIN is: \(defaults.value(forKey: "pinNumber")!)"
            
            
            cell.selectionStyle = .none
            
            addToolBarContactNum(textField: cell.edtReTypePin)
            addToolBarContactNum(textField: cell.edtNewPin)
            addToolBarContactNum(textField: cell.edtOldPin)
            
            
            return cell
            
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        if indexPath.section == 0{
            
            return 435
            
        }else if indexPath.section == 1{
            
            return 350
            
        }else{
            
            return 350
        }
        
    }
    
    
    
    func changePassworAction(_ sender: UIButton){
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = (tblAccountSettings.cellForRow(at: indexPath)! as! ChangePasswordTableViewCell)
        
        
        if cell.edtUserName.text == "" || cell.edtOldPassword.text == "" || cell.edtNewPassword.text == ""{
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
            
        }else if(validateForm.isContainUpperCase(cell.edtNewPassword) == false && validateForm.isContainNumber(cell.edtNewPassword) == false){
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPasswordValidation, closeButtonTitle:"OK")
            
        }else if validateForm.isContainUpperCase(cell.edtNewPassword) == true && validateForm.isContainNumber(cell.edtNewPassword) == false{
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPasswordValidation, closeButtonTitle:"OK")
            
            
        }else if validateForm.isContainUpperCase(cell.edtNewPassword) == false && validateForm.isContainNumber(cell.edtNewPassword) == true{
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPasswordValidation, closeButtonTitle:"OK")
            
            
        }else if cell.edtNewPassword.text != cell.edtRetypePassword.text{
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPasswordRetypePassDidntMatch, closeButtonTitle:"OK")
            
        }else{
            
            if(cell.edtNewPassword.text != cell.edtRetypePassword.text){
                
                _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPasswordDidntMatch, closeButtonTitle:"OK")
                
            }else{
                
                if(cell.edtNewPassword.text?.contains(" "))!{
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPasswordValidation, closeButtonTitle:"OK")
                    
                }else{
                    
                    if reachability?.isReachable == true{
                        
                        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                        loadingNotification.mode = MBProgressHUDMode.indeterminate
                        loadingNotification.label.text = "Requesting..."
                        loadingNotification.isUserInteractionEnabled = false;
                        self.tblAccountSettings.isUserInteractionEnabled = false
                        
                        changeYourPassword(cell.edtUserName.text!, newPassword: cell.edtNewPassword.text!, oldPassword: cell.edtOldPassword.text!)

                        
                    }else{
                        
                         _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
                        
                    }

                }
                
            }
            
        }

        
        
    }
    
    
    func addNewPin(_ sender: UIButton){
        
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = (tblAccountSettings.cellForRow(at: indexPath)! as! AddNewPINTableViewCell)
        
        if cell.edtPin.text == "" || cell.edtReTypePin.text == "" {
            
              _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
            
        }else if cell.edtPin.text != cell.edtReTypePin.text{
            
             _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPinRetypePinDidntMatch, closeButtonTitle:"OK")
            
        }else if (cell.edtPin.text?.characters.count)! < 4 || (cell.edtPin.text?.characters.count)! > 4 {
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPinCount, closeButtonTitle:"OK")
            
        }else{
            
            
            let storedUsername = defaults.value(forKey: "storedUsername") as! String
            let storedPassword = defaults.value(forKey: "storedPassword") as! String
            
            if reachability?.isReachable == true{
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Registering PIN..."
                loadingNotification.isUserInteractionEnabled = false;
                self.tblAccountSettings.isUserInteractionEnabled = false
                
                registerPin(username: storedUsername, password: storedPassword, pinNumber: cell.edtPin.text!)
                
            }else{
                
                 _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
                
            }
            
        }
        
        
    }
    
    
    func changeOldPin(_ sender: UIButton){
        
        let indexPath = IndexPath(row: 0, section: 2)
        let cell = (tblAccountSettings.cellForRow(at: indexPath)! as! ChangeOldPinTableViewCell)
        
        if cell.edtNewPin.text == "" || cell.edtOldPin.text == ""{
            
             _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.errorEmptyFields, closeButtonTitle:"OK")
        }else if cell.edtNewPin.text != cell.edtReTypePin.text {
            
             _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPinRetypePinDidntMatch, closeButtonTitle:"OK")
            
        }else if (cell.edtNewPin.text?.characters.count)! < 4 || (cell.edtNewPin.text?.characters.count)! > 4 {
            
            _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPinCount, closeButtonTitle:"OK")
            
        }else{
            
             if reachability?.isReachable == true{
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Updating PIN..."
                loadingNotification.isUserInteractionEnabled = false;
                self.tblAccountSettings.isUserInteractionEnabled = false
                
                updatePin(oldPin: cell.edtOldPin.text!, newPin: cell.edtNewPin.text!, username: defaults.value(forKey: "storedUsername")! as! String)
                
             }else{
                
                 _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.noInternetConnectionMessage, closeButtonTitle:"OK")
                
            }
          
        }
        
    }
    
    
    func registerPin(username: String, password: String, pinNumber: String){
        
        let indexPath = IndexPath(row: 0, section: 1)
        let cell = (tblAccountSettings.cellForRow(at: indexPath)! as! AddNewPINTableViewCell)
        
        let addPin = ["username": username, "pin": pinNumber, "password": password] as [String : Any]
        
        Alamofire.request(PostRouter.postAddPin(addPin as [String : AnyObject]))
            .responseJSON { response in
                if response.result.isFailure == true {
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblAccountSettings.isUserInteractionEnabled = true
                    
                      _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    let post = JSON(value)
                    
                    if post["responseCode"].stringValue == "200"{
                        
                        cell.edtPin.text = ""
                        cell.edtReTypePin.text = ""
                        
                        self.defaults.set(pinNumber, forKey: "pinNumber")
                        self.showSavePinOrRegisterPin = 1
                        self.tblAccountSettings.reloadData()
                        
                        
                        _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.successAddedPin, closeButtonTitle:"OK")
                        
                    }else if post["responseCode"].stringValue == "230"{
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPinIncorrect, closeButtonTitle:"OK")
                        
                    }
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblAccountSettings.isUserInteractionEnabled = true
                    
                }
                
            }
        
        
    }
    
    
    
    func updatePin(oldPin: String, newPin: String, username: String){
        
        let indexPath = IndexPath(row: 0, section: 2)
        let cell = (tblAccountSettings.cellForRow(at: indexPath)! as! ChangeOldPinTableViewCell)
        
        let updatePin = ["newPin": newPin, "oldPin": oldPin, "username": username] as [String : Any]
        
        Alamofire.request(PostRouter.postChangePin(updatePin as [String : AnyObject]))
            .responseJSON { response in
                if response.result.isFailure == true {
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblAccountSettings.isUserInteractionEnabled = true
                    
                    _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningSomethingWrong, closeButtonTitle:"OK")
                    
                }
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    let post = JSON(value)
                    
                    if post["responseCode"].stringValue == "200"{
                        
                        cell.edtNewPin.text = ""
                        cell.edtReTypePin.text = ""
                        cell.edtOldPin.text = ""
                        
                        self.defaults.set(newPin, forKey: "pinNumber")
                        
                        self.tblAccountSettings.reloadData()
                        
                        _ = SCLAlertView().showSuccess(AlertMessages.successTitle, subTitle:AlertMessages.successChangedPin, closeButtonTitle:"OK")
                        
                    }else if post["responseCode"].stringValue == "230"{
                        
                        _ = SCLAlertView().showWarning(AlertMessages.errorTitle, subTitle:AlertMessages.warningPinIncorrect, closeButtonTitle:"OK")
                        
                    }
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tblAccountSettings.isUserInteractionEnabled = true
                    
                }
                
            }
        
    }
    
    
    func addToolBarContactNum(textField: UITextField){
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        
    }
    
    func pickerDoneClicked(){
        
         tblAccountSettings.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(tableViewHeight() - 500))
        
        if showSavePinOrRegisterPin == 0{
            let indexPathAddPin = IndexPath(row: 0, section: 1)
            let cellAddPin = (tblAccountSettings.cellForRow(at: indexPathAddPin)! as! AddNewPINTableViewCell)
            
            cellAddPin.edtPin.resignFirstResponder()
            cellAddPin.edtReTypePin.resignFirstResponder()
            
            
        }else{
            let indexPathChangePin = IndexPath(row: 0, section: 2)
            let cellChangePin = (tblAccountSettings.cellForRow(at: indexPathChangePin)! as! ChangeOldPinTableViewCell)
            
            cellChangePin.edtReTypePin.resignFirstResponder()
            cellChangePin.edtOldPin.resignFirstResponder()
            cellChangePin.edtNewPin.resignFirstResponder()
            
        }

  
    }
    

}
