//
//  NewSortAndFilterHospitalViewController.swift
//  Medicard
//
//  Created by Al John Albuera on 3/22/17.
//  Copyright © 2017 Al John Albuera. All rights reserved.
//

import UIKit
import SCLAlertView

class NewSortAndFilterHospitalViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    
    @IBOutlet var edtSortBy: UITextField!
    @IBOutlet var btnCheckBox: UIButton!
    @IBOutlet var edtChooseProvince: UITextField!
    @IBOutlet var edtChooseCity: UITextField!
    @IBOutlet var btnResetFilters: UIButton!
    @IBOutlet var btnShowResult: UIButton!
    @IBOutlet var edtFindCity: TextfieldModification!
    var sortByPickerView: UIPickerView = UIPickerView()
    
    var sortBy = ["Hospital/Clinic Name", "MediCard Clinic First", "City", "Province"]
    
    var nullArray: [String] = []
    
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = "Sort and Filter Hospital"
        
        var image = UIImage(named: "ic_close_white@2x.png")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelSortingAndFiltering(_:)))
        
        btnShowResult.layer.cornerRadius = 2
        btnResetFilters.layer.cornerRadius = 2
        

        
        
        sortByPickerView.delegate = self
        sortByPickerView.dataSource = self

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        print("PROVINCE NAME: \(Getter_Setter.chosenProvinceNameArray)")
        print("PROVINCE NAME: \(Getter_Setter.chosenProvinceCodeArray)")
        print("CHOSEN CITY: \(Getter_Setter.chosenCityName)")
        
        
        edtFindCity.text = Getter_Setter.finalChosenSearch
        
        if Getter_Setter.finalChosenSortBy == "hospitalName"{
            edtSortBy.text = sortBy[0]
            Getter_Setter.chosenSortBy = "hospitalName"
        }else if Getter_Setter.finalChosenSortBy == ""{
            edtSortBy.text = sortBy[1]
            Getter_Setter.chosenSortBy = "category"
        }else if Getter_Setter.finalChosenSortBy == "category"{
            edtSortBy.text = sortBy[1]
            Getter_Setter.chosenSortBy = "category"
        }else if Getter_Setter.finalChosenSortBy == "city"{
            edtSortBy.text = sortBy[2]
            Getter_Setter.chosenSortBy = "city"
        }else if Getter_Setter.finalChosenSortBy == "province"{
            edtSortBy.text = sortBy[3]
            Getter_Setter.chosenSortBy = "province"
        }
        
        Getter_Setter.medicardClinicFirst = Getter_Setter.finalMedicardClinicFirst
        
        if (Getter_Setter.medicardClinicFirst == true)
        {
            btnCheckBox.setBackgroundImage(UIImage(named: "ic_selected.png"), for: UIControlState.normal)
            btnCheckBox.isSelected = true
            Getter_Setter.medicardClinicFirst = true
            
        }else{
            btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
            btnCheckBox.isSelected = false
            Getter_Setter.medicardClinicFirst = false
        }

        
        if Getter_Setter.chosenProvinceNameArray.count == 0{
            edtChooseProvince.text = "All Province"
            Getter_Setter.chosenProvinceNameArray = []
        }else{
            
            let province = String(describing: Getter_Setter.chosenProvinceNameArray)
            edtChooseProvince.text = removeSpecialCharsFromString(province)
            
        }
        
        if Getter_Setter.chosenCityName.count == 0{
            edtChooseCity.text = "All Cities"
            Getter_Setter.chosenCityName = []
        }else{
            
            
            let cities = String(describing: Getter_Setter.chosenCityName)
            edtChooseCity.text = removeSpecialCharsFromString(cities)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func removeSpecialCharsFromString(_ text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == edtSortBy{
            
             Getter_Setter.chosenSearch = edtFindCity.text
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            showAlertSortBy()
            
            return false
            
        }else if textField == edtChooseProvince{
            
            Getter_Setter.chosenSearch = edtFindCity.text
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let goToChoosePlace = self.storyboard!.instantiateViewController(withIdentifier: "NewChoosePlaceViewController") as! NewChoosePlaceViewController
            goToChoosePlace.locationStatus = "province"
            self.navigationController?.pushViewController(goToChoosePlace, animated: true)
            
            return false
            
            
        }else if textField == edtChooseCity{
            
             Getter_Setter.chosenSearch = edtFindCity.text
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let goToChoosePlace = self.storyboard!.instantiateViewController(withIdentifier: "NewChoosePlaceViewController") as! NewChoosePlaceViewController
            goToChoosePlace.locationStatus = "city"
            self.navigationController?.pushViewController(goToChoosePlace, animated: true)
            
            return false
            
            
        }else{
            
            
            return true
            
        }
        
        
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == edtFindCity{
            Getter_Setter.chosenSearch = textField.text
        }
        
        textField.resignFirstResponder()
        
        
        return true
    }

    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortBy.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        edtSortBy.text = sortBy[row]
        
        return sortBy[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("ROW: \(row)")
        
    }
    
    
    func addPicker(){
        
        edtSortBy.inputView = sortByPickerView
        sortByPickerView.backgroundColor = UIColor.white
        
        let toolBar = UIToolbar()
        toolBar.backgroundColor = UIColor.darkGray
        toolBar.sizeToFit()
        
        
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(35))
        btn.setBackgroundImage(UIImage(named: "bar_button_bg@2x.png")!, for: .normal)
        btn.addTarget(self, action: #selector(self.pickerDoneClicked), for: .touchUpInside)
        let Item = UIBarButtonItem(customView: btn)
        self.toolbarItems = [Item]
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, Item], animated: false)
        toolBar.isUserInteractionEnabled = true
        edtSortBy.inputAccessoryView = toolBar
        
        
    }
    
    func pickerDoneClicked(){
        
        edtSortBy.resignFirstResponder()
        
    }
    
    func cancelSortingAndFiltering(_ sender: AnyObject){
        
        Getter_Setter.finalChosenProvinceName = Getter_Setter.finalChosenProvinceName
        Getter_Setter.finalChosenCityName = Getter_Setter.finalChosenCityName
        Getter_Setter.finalChosenSearch = Getter_Setter.finalChosenSearch
        Getter_Setter.finalChosenSortBy = Getter_Setter.finalChosenSortBy
        Getter_Setter.finalMedicardClinicFirst = Getter_Setter.finalMedicardClinicFirst
        Getter_Setter.finalChosenProvinceCodeArray = Getter_Setter.finalChosenProvinceCodeArray
        Getter_Setter.finalChosenProvinceNameArray = Getter_Setter.finalChosenProvinceNameArray
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func resetFilterAction(_ sender: UIButton) {
        
        btnCheckBox.isSelected = false
        Getter_Setter.medicardClinicFirst = false
        Getter_Setter.finalMedicardClinicFirst = false
        Getter_Setter.chosenProvinceName = "All Province"
        Getter_Setter.chosenCityName = nullArray
        Getter_Setter.finalChosenProvinceCode = ""
        Getter_Setter.finalChosenProvinceName = ""
        Getter_Setter.finalChosenCityName = nullArray
        Getter_Setter.chosenSearch = ""
        Getter_Setter.finalChosenSearch = ""
        Getter_Setter.finalChosenSortBy = ""
        Getter_Setter.chosenProvinceNameArray = nullArray
        Getter_Setter.chosenProvinceCodeArray = nullArray
        
        viewWillAppear(true)
        
    }
    
    
    @IBAction func showResultFilter(_ sender: UIButton) {
    
        Getter_Setter.finalChosenProvinceName = Getter_Setter.chosenProvinceName
        Getter_Setter.finalChosenCityName = Getter_Setter.chosenCityName
        if edtFindCity.text == ""{
            Getter_Setter.finalChosenSearch = ""
        }else{
            Getter_Setter.finalChosenSearch = Getter_Setter.chosenSearch
        }
        
        Getter_Setter.finalMedicardClinicFirst = Getter_Setter.medicardClinicFirst
        Getter_Setter.finalChosenSortBy = Getter_Setter.chosenSortBy
        Getter_Setter.finalChosenProvinceCodeArray = Getter_Setter.chosenProvinceCodeArray
        Getter_Setter.finalChosenProvinceNameArray = Getter_Setter.chosenProvinceNameArray
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func checkBoxAction(_ sender: UIButton) {
        
        if (btnCheckBox.isSelected == true)
        {
            btnCheckBox.setBackgroundImage(UIImage(named: "ic_unselected.png"), for: UIControlState.normal)
            btnCheckBox.isSelected = false
            Getter_Setter.medicardClinicFirst = false
            
        }else{
            btnCheckBox.setBackgroundImage(UIImage(named: "ic_selected.png"), for: UIControlState.normal)
            btnCheckBox.isSelected = true
            Getter_Setter.medicardClinicFirst = true
        }
        
        
    }
    
    
    func showAlertSortBy(){
        
        
        let alert = SCLAlertView()
        let icon = UIImage(named:"ic_sort_image.png")
        alert.addButton("Hospital/Clinic Name"){
            
            self.edtSortBy.text = "Hospital/Clinic Name"
            Getter_Setter.chosenSortBy = "hospitalName"
        }
        
        alert.addButton("MediCard Clinic First") {
            
            self.edtSortBy.text = "MediCard Clinic First"
            Getter_Setter.chosenSortBy = "category"
        }
        
        alert.addButton("Province") {
            
            self.edtSortBy.text = "Province"
            Getter_Setter.chosenSortBy = "province"
        }
        
        alert.addButton("City") {
            
            self.edtSortBy.text = "City"
            Getter_Setter.chosenSortBy = "city"
        }
        

        
        
        alert.showCustom("Sort By", subTitle: "", color: UIColor(red:163/255.0, green:83/255.0, blue:153/255.0, alpha: 1.0), icon: icon!)
        
        
    }


}
